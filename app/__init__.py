from flask import Flask, render_template, redirect, url_for # Import flask and template operators
from flask_sqlalchemy import SQLAlchemy # Import SQLAlchemy
from flask_login import LoginManager
#from flask_principal import Principal, Identity, AnonymousIdentity, identity_changed
from flask_mail import Mail
from flask_bcrypt import Bcrypt

# Define the WSGI application object
app = Flask(__name__)

# bcrypt
bcrypt = Bcrypt(app)

# login 
login_manager = LoginManager()
login_manager.init_app(app)

# Configurations
app.config.from_object('config')

# Define the database object which is imported
# by modules and controllers
db = SQLAlchemy(app)




# Import a module / component using its blueprint handler variable
from app.auth.controllers import auth as auth_module
from app.general.controllers import general as general_module
from app.measurement.controllers import measurement as measuring_module
from app.equipment.controllers import equipment as equipment_module
from app.experiments.controllers import experiments as experiment_module
from app.admin.controllers import admin as admin_module
from app.mobile_services.controllers import mobile_services as mobile_services_module
from app.statistics.controllers import statistics as statistics_module
from app.qrgenerator.controllers import qrgenerator as qrgenerator_module
from app.fitness.controllers import fitness as fitness_module
from app.fitness.food.controllers import fitness_food as fitness_food_module
from app.fitness.exercise.controllers import fitness_exercise as fitness_exercise_module
#from app.storage.controllers import storage as storage_module

# Register blueprint(s)
app.register_blueprint(auth_module)
app.register_blueprint(general_module)
app.register_blueprint(measuring_module)
app.register_blueprint(equipment_module)
app.register_blueprint(experiment_module)
app.register_blueprint(admin_module)
app.register_blueprint(mobile_services_module)
app.register_blueprint(statistics_module)
app.register_blueprint(qrgenerator_module)
app.register_blueprint(fitness_module)
app.register_blueprint(fitness_food_module)
app.register_blueprint(fitness_exercise_module)
#app.register_blueprint(storage_module)

# create the database using SQLAlchemy
db.create_all()


# mail init
mail = Mail(app)


from app.general.controllers import index

# Sample HTTP error handling
@app.errorhandler(404)
def not_found(error):
	return render_template('404.html'), 404
    
@app.errorhandler(401)
def not_authorized(error):
    return redirect(url_for('general.index'))
    #return render_template('401.html'), 401

