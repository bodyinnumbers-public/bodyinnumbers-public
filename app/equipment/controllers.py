import jinja2
from flask import Blueprint, request, render_template, redirect
from app import db
from app.equipment.models import MeasuringEquipment, EquipmentCategories
from app.equipment.forms import NewEquipmentForm, EditEquipmentForm, AddCategoryForm
from flask_login import login_required

equipment = Blueprint('equipment', __name__, template_folder='templates', url_prefix='/app')


@equipment.route('/equipment/remove/<int:id_remove>')
@login_required
def remove_equipment(id_remove):
    """Remove selected equipment"""

    MeasuringEquipment.query.filter_by(id=id_remove).delete()
    db.session.commit()
    return redirect('app/equipment')


@equipment.route('/equipment/remove_cat/<int:id>')
@login_required
def remove_category(id):
    """Remove selected category"""

    EquipmentCategories.query.filter_by(id=id).delete()
    db.session.commit()
    return redirect('app/equipment')


@equipment.route('/equipment', methods=['GET', 'POST'])
@login_required
def get_equipment():
    """Adding equipments"""

    data = db.session.query(MeasuringEquipment).order_by(MeasuringEquipment.date_created.desc())
    cat = db.session.query(EquipmentCategories).order_by(EquipmentCategories.id)

    form_add = NewEquipmentForm(request.form)
    form_add.deviceCategory.choices = [(g.id, g.categoryName) for g in EquipmentCategories.query.order_by('id')]

    form_edit = EditEquipmentForm(request.form)
    form_edit.deviceCategory_e.choices = [(g.id, g.categoryName) for g in EquipmentCategories.query]

    form_cat = AddCategoryForm(request.form)
    
    if form_add.add.data and form_add.validate_on_submit():
        meas_eq_inst = MeasuringEquipment(
            deviceCategory=form_add.deviceCategory.data,
            deviceName=form_add.deviceName.data,
            description=form_add.description.data

            # created_by_user_id = 1
            # db.session.query(User).filter_by(id = currnet_user.get_id())
        )
        db.session.add(meas_eq_inst)
        db.session.commit()

    elif form_edit.edit.data and form_edit.validate_on_submit():
        equipment_inst = db.session.query(MeasuringEquipment).filter_by(id=form_edit.device_id.data).first()
        if equipment_inst:
            equipment_inst.deviceCategory = form_edit.deviceCategory_e.data
            equipment_inst.deviceName = form_edit.deviceName_e.data
            equipment_inst.description = form_edit.description_e.data
            db.session.flush()
            db.session.commit()

    elif form_cat.add.data and form_cat.validate_on_submit():
        meas_cat_inst = EquipmentCategories(
            categoryName=form_cat.categoryName.data
        )
        db.session.add(meas_cat_inst)
        db.session.commit()


    return render_template("equipment.html", title='Přístroj', data=data, cat=cat, form_add=form_add, form_edit=form_edit,
                           form_cat=form_cat)



#=====================================================================
@equipment.route('/equipment/add-category', methods=['GET', 'POST'])
@login_required
def add_category():
    """add new equipment category"""
    return render_template("add_category.html")
    
@equipment.route('/equipment/add-equipment', methods=['GET', 'POST'])
@login_required
def add_equipment():
    """add new equipment"""
    return render_template("add_equipment.html")
