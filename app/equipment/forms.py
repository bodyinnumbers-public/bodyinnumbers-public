from flask_wtf import Form
from wtforms import SubmitField, StringField, \
    TextAreaField, SelectField  # Import Form elements such as TextField and BooleanField (optional)
from wtforms.validators import DataRequired, length  # Import Form validators


class NewEquipmentForm(Form):
    """Form for adding new equipment"""
    deviceCategory = SelectField(u'Kategorie', coerce=int)
    deviceName = StringField('Název přístroje', [DataRequired(), length(max=64)])
    description = TextAreaField('Popis', [length(max=256)])
    add = SubmitField('Potvrdit')


class EditEquipmentForm(Form):
    """Form for edit equipment"""
    device_id = StringField('', [DataRequired()])
    deviceCategory_e = SelectField(u'Kategorie', coerce=int)
    deviceName_e = StringField('Název přístroje', [DataRequired(), length(max=64)])
    description_e = TextAreaField('Popis', [length(max=256)])
    edit = SubmitField('Uložit změny')


class AddCategoryForm(Form):
    """Form for adding categories"""
    categoryName = StringField('Název kategorie', [DataRequired(), length(max=64)])
    add = SubmitField('Vytvořit kategorii')
