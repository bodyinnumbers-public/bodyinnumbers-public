from app import db


class Base(db.Model):
    """Base table"""
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    date_modified = db.Column(db.DateTime, default=db.func.current_timestamp(), onupdate=db.func.current_timestamp())


class EquipmentCategories(Base, db.Model):
    """Table with information about equipment categories"""
    __tablename__ = 'equipment_categories'

    categoryName = db.Column(db.String(64), nullable=False)


class MeasuringEquipment(Base, db.Model):
    """Table with information about measuring equipment and devices"""
    __tablename__ = 'measuring_equipment'

    deviceCategory = db.Column(db.Integer, db.ForeignKey(EquipmentCategories.id, onupdate="CASCADE", ondelete="CASCADE"))
    deviceName = db.Column(db.String(64), nullable=False)
    description = db.Column(db.String(256), nullable=True)

