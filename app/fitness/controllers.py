from flask import Blueprint, request, render_template, redirect, jsonify, abort, url_for
from app import app, db
from app.auth.models import create_data
from app.admin.controllers import show_all_users
from flask_login import login_required
from app.fitness.forms import FoodForm
from app.fitness.food.models import DaySettings, FoodDefinition, FoodTracking, Food
from app.fitness.exercise.models import ExerciseTracking, Exercise, ExerciseType
from flask_login import current_user
from werkzeug.routing import Rule
import csv
import io

from app.fitness.forms import FoodForm
import json

fitness = Blueprint('fitness', __name__, template_folder='templates', url_prefix='/app')

@fitness.route('/fitness/food', methods=['GET', 'POST'])
def showFood():
    """Sample blank page"""
    # wtforms
    foodForm = FoodForm()
    if foodForm.data['foodInput']:
        parseFile(foodForm.data['foodInput'])
    if foodForm.data['exerciseInput']:
        parseExerciseFile(foodForm.data['exerciseInput'])
    return render_template("food.html", form=foodForm)

@fitness.route('/fitness/food/generate', methods=['GET', 'POST'])
def genData():
    fillByTestData()
    print("Vygenerovano")
    return redirect('/app/fitness/food')

def parseFile(csv_file):
    """Parse CSV file"""
    buffer = io.StringIO(csv_file.stream.read().decode("UTF8"), newline=None)
    csv_data = csv.reader(buffer, delimiter=',')

    for index, row in enumerate(csv_data):
        if (index == 0):
            continue
        print(row[0])

        Food.add_food(row[0], row[1], row[2], row[3], row[4], row[5])

def parseExerciseFile(csv_file):
    """Parse CSV file"""
    buffer = io.StringIO(csv_file.stream.read().decode("UTF8"), newline=None)
    csv_data = csv.reader(buffer, delimiter=',')
    type = ExerciseType.add_exercise_type("cvik", 0)

    for index, row in enumerate(csv_data):
        if (index == 0):
            continue
        print(row[0])

        Exercise.add_exercise(row[0], type, 0)

def fillByTestData():

    user_id = current_user.get_id()
    DaySettings.add_settings(DaySettings, user_id, 90, 2500, 2)

    f_1 = FoodTracking.add_foodtracking(user_id, 0)
    f_2 = FoodTracking.add_foodtracking(user_id, 0)
    f_3 = FoodTracking.add_foodtracking(user_id, 0)
    f_4 = FoodTracking.add_foodtracking(user_id, 0)

    FoodDefinition.add_definition(f_1, 'Jidlo_test', 100, 300, 1, 20, 20, 100)
    FoodDefinition.add_definition(f_2, 'Jidlo_test1', 130, 100, 0.2, 10, 5, 100)
    FoodDefinition.add_definition(f_3, 'Jidlo_test2', 0.30, 200, 0.1, 5, 11, 100)

    FoodDefinition.add_definition(f_4, 'Jidlo_test_a', 100, 300, 1, 20, 20, 100)
    FoodDefinition.add_definition(f_4, 'Jidlo_test_b', 130, 100, 0.2, 10, 5, 100)
    FoodDefinition.add_definition(f_4, 'Jidlo_test_c', 30, 200, 0.1, 5, 11, 100)

    f_a = ExerciseType.add_exercise_type("typ1", 0)
    f_b = ExerciseType.add_exercise_type("typ2", 0)

    f_1 = Exercise.add_exercise("klik", f_a, 0)
    f_2 = Exercise.add_exercise("klik1", f_b, 0)
    f_3 = Exercise.add_exercise("klik2", f_a, 0)
    f_4 = Exercise.add_exercise("klik3", f_b, 0)
    f_5 = Exercise.add_exercise("klik4", f_b, 0)
    f_6 = Exercise.add_exercise("klik5", f_b, 0)
    f_7 = Exercise.add_exercise("drep1", f_b, 0)
    f_8 = Exercise.add_exercise("drep2", f_b, 0)
    f_9 = Exercise.add_exercise("drep3", f_b, 0)
    f_10 = Exercise.add_exercise("drep4", f_b, 0)
    f_11 = Exercise.add_exercise("drep5", f_b, 0)

    ExerciseTracking.add_tracking(user_id, f_1, 600)
    ExerciseTracking.add_tracking(user_id, f_1, 600)
    ExerciseTracking.add_tracking(user_id, f_1, 600)

    ExerciseTracking.add_tracking(user_id, f_1, 300)
    ExerciseTracking.add_tracking(user_id, f_2, 200)
    ExerciseTracking.add_tracking(user_id, f_2, 100)
