from flask import Blueprint,jsonify,send_file
from flask_restful import reqparse, Resource, url_for, Api
from app.mobile_services.controllers import authorize
import math
from app.fitness.food.models import FoodTracking, DaySettings, Food
import werkzeug.datastructures
import json
from app.fitness.food.models import FoodDefinition
import datetime
from app.storage.models import Storage
import base64
import PIL
from PIL import Image
from io import BytesIO

fitness_food = Blueprint('fitness_food', __name__, template_folder='templates', url_prefix='/app')
fitness_mobile = Blueprint('mobile-fitness', __name__, template_folder='templates', url_prefix='/app')

@fitness_food.route('/fitness/food/test')
def sample():
    """Sample blank page"""
    test = 'test'
    return test

api = Api(fitness_food)

class Dashboard(Resource):
    """Today intake of user for graphs"""
    parser = reqparse.RequestParser()
    parser.add_argument('client_username', type=str, required=True)
    parser.add_argument('token', type=str, required=True)

    def post(self):
        data = self.parser.parse_args()
        client_username = data.get('client_username', '')
        token = data.get('token', '')

        userID = authorize(client_username, token)
        if userID:
            today_intake = FoodTracking.today_intake(FoodTracking, userID)
            return today_intake
        else:
            return {'message': 'authorization failed'}, 401

class Settings(Resource):
    """Settings of user"""
    parser = reqparse.RequestParser()
    parser.add_argument('client_username', type=str, required=True)
    parser.add_argument('token', type=str, required=True)

    def post(self):
        data = self.parser.parse_args()
        client_username = data.get('client_username', '')
        token = data.get('token', '')

        userID = authorize(client_username, token)
        if userID:
            last_settings = DaySettings.get_daysetting(DaySettings, userID)
            return last_settings
        else:
            return {'message': 'authorization failed'}, 401

class SettingsChange(Resource):
    """Settings of user"""
    parser = reqparse.RequestParser()
    parser.add_argument('client_username', type=str, required=True)
    parser.add_argument('token', type=str, required=True)
    parser.add_argument('calorie', type=int, required=True)
    parser.add_argument('water', type=float, required=True)
    parser.add_argument('weight', type=int, required=True)

    def post(self):
        data = self.parser.parse_args()
        client_username = data.get('client_username', '')

        calories_intake = int(data.get('calorie'))
        water_intake = float(data.get('water'))
        weight_intake = int(data.get('weight'))
        token = data.get('token', '')

        userID = authorize(client_username, token)
        if userID:
            DaySettings.add_settings(DaySettings, userID, weight_intake, calories_intake, water_intake)
            return "ok"
        else:
            return {'message': 'authorization failed'}, 401

class AddFood(Resource):
    """Adding user's eaten food"""
    parser = reqparse.RequestParser()
    parser.add_argument('food')
    parser.add_argument('token', type=str, required=True)
    parser.add_argument('client_username', type=str, required=True)
    parser.add_argument('file', type=werkzeug.datastructures.FileStorage, location='files', required=False)

    def post(self):
        data = self.parser.parse_args()
        client_username = data.get('client_username', '')
        token = data.get('token', '')

        userID = authorize(client_username, token)
        if userID:
            #json of eaten food like this ("food":[{"name":"jmeno","calorie":"1000"..."weight":"200"},...])
            id = None
            if data.file is not None:
                id = Storage.saveFile(data.file, "fitness")
            food_json = data.get('food', '')
            food = json.loads(food_json)
            food_track_id = FoodTracking.add_foodtracking(userID, id)
            #Adding eaten food to database
            for item in food:
                weight = float(item['weight'])
                #Conversion food's components to eaten weight
                const = weight/100

                name = item['name']
                calorie = math.floor(float(item['calorie'])*const * 10) / 10
                carbohydrate = math.floor(float(item['carbohydrate'])*const * 10) / 10
                fat = math.floor(float(item['fat'])*const * 10) / 10
                protein = math.floor(float(item['protein'])*const * 10) / 10
                water = math.floor(float(item['water'])*const * 10) / 10

                FoodDefinition.add_definition(
                    food_track_id, name,
                    water, calorie,
                    protein, fat,
                    carbohydrate, weight
                )
            return "ok"
        else:
            return {'message': 'authorization failed'}, 401

class GetFood(Resource):
    """Gets last fifty user's eaten food"""
    parser = reqparse.RequestParser()
    parser.add_argument('client_username', type=str, required=True)
    parser.add_argument('token', type=str, required=True)

    def serialize_data(self, fd):
        return{

            'weight': fd.FoodDefinition.weight,
            'calorie': fd.FoodDefinition.calorie,
            'water': fd.FoodDefinition.water,
            'carbohydrate': fd.FoodDefinition.carbohydrate,
            'protein': fd.FoodDefinition.protein,
            'fat': fd.FoodDefinition.fat,
            'name': fd.FoodDefinition.name,
        }


    def serialize_result(self, id, photo_id, created, calorie_total, weight_total, data):
        return {
                    'id': id,
                    'created': str(created.day) + "/" + str(created.month) + "/" + str(created.year),
                    'calorieTotal': calorie_total,
                    'weightTotal': weight_total,
                    'data': data,
                    'photo_id': photo_id
                }

    def serialize(self, food):
        """Transfom record to json
           Args:
               food: record from database
            Return:
                result: json of eaten food
                food{"calorieTotal": 4000,"created": "20/04/2017","data": [{...},{...}],"id": 24,"weightTotal": 400}
        """
        result = []
        data = []
        if len(food) == 0:
            return result

        id = food[0].food_tracking_id
        calorie_total = 0
        weight_total = 0
        created = 0
        photo_id = 0

        for fd in food:
            #Detect if fd is not next food_tracking
            if fd.food_tracking_id == id:
                created = fd.date
                photo_id = fd.photo_id
                if fd.FoodDefinition is not None:
                    calorie_total += int(fd.FoodDefinition.calorie)
                    weight_total += int(fd.FoodDefinition.weight)
                    data.append(self.serialize_data(fd))
                else:
                    weight_total = 0
                    calorie_total = 0
            else:
                # fd is next food_tracking add
                result.append((self.serialize_result(id, photo_id, created, calorie_total, weight_total, data)))
                id = fd.food_tracking_id
                photo_id = fd.photo_id
                created = fd.date
                data = []
                if fd.FoodDefinition is not None:
                    weight_total = int(fd.FoodDefinition.weight)
                    calorie_total = int(fd.FoodDefinition.calorie)
                    data.append(self.serialize_data(fd))
                else:
                    weight_total = 0
                    calorie_total = 0

        result.append((self.serialize_result(id, photo_id, created, calorie_total, weight_total, data)))
        return result

    def post(self):
        data = self.parser.parse_args()
        client_username = data.get('client_username', '')
        token = data.get('token', '')

        userID = authorize(client_username, token)
        if userID:
            #Gets last 50 food eaten by user
            food = FoodTracking.get_food(FoodTracking, userID, 50)
            serialize = self.serialize(food)

            return serialize
        else:
            return {'message': 'authorization failed'}, 401

class FindFood(Resource):
    """Finds 10 most similar food by name"""
    parser = reqparse.RequestParser()
    parser.add_argument('client_username', type=str, required=True)
    parser.add_argument('token', type=str, required=True)
    parser.add_argument('key_words', type=str, required=True)

    def post(self):
        data = self.parser.parse_args()
        client_username = data.get('client_username', '')
        token = data.get('token', '')

        userID = authorize(client_username, token)
        if userID:
            key_word = data.get('key_words', '')
            key_words = key_word.split(" ")
            max = len(key_words)
            for i in range(0, max):
                key_words[i] = "%"+key_words[i]+"%"
            food = Food.find_food(Food, key_words, 10)

            return jsonify(json_list=[self.serialize(i) for i in food])
        else:
            return {'message': 'authorization failed'}, 401

    def serialize(self, item):
        return {
            "name": item.name,
            "calorie": item.calorie,
            "water": item.water,
            "fat": item.fat,
            "carbohydrate": item.carbohydrate,
            "protein": item.protein
        }

class DeleteFood(Resource):
    """Delete food_tracking info and fooddef conected to it"""
    parser = reqparse.RequestParser()
    parser.add_argument('client_username', type=str, required=True)
    parser.add_argument('token', type=str, required=True)
    parser.add_argument('id', type=str, required=True)

    def post(self):
        data = self.parser.parse_args()
        client_username = data.get('client_username', '')
        token = data.get('token', '')
        userID = authorize(client_username, token)

        if userID:
            id = data.get('id', '')
            id = int(id)
            FoodTracking.delete_food_tracking(FoodTracking, id, userID)
            return "ok"
        else:
            return {'message': 'authorization failed'}, 401

class Statistics(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('client_username', type=str, required=True)
    parser.add_argument('token', type=str, required=True)

    def serialize(self, date, water, calorie):
        return {
            "date": str(date.day) + "/" + str(date.month) + "/" + str(date.year),
            "water": water,
            "calorie": calorie
        }

    def post(self):
        data = self.parser.parse_args()
        client_username = data.get('client_username', '')
        token = data.get('token', '')
        userID = authorize(client_username, token)

        if userID:
            records = FoodTracking.info_perweeks(FoodTracking, 4, userID)

            result = []
            if len(records) == 0:
                result.append({
            "date": datetime.datetime.now().strftime("%d/%m/%Y"),
            "water": 0,
            "calorie": 0
            })
                return result
            date = records[0].date
            water_sum = 0
            calorie_sum = 0
            for record in records:
                if record.date.day == date.day:
                    water_sum += record.FoodDefinition.water
                    calorie_sum += record.FoodDefinition.calorie
                else:
                    result.append(self.serialize(date, water_sum, calorie_sum))
                    water_sum = record.FoodDefinition.water
                    calorie_sum = record.FoodDefinition.calorie
                    date = record.date

            result.append(self.serialize(date, water_sum, calorie_sum))
            return result
        else:
            return {'message': 'authorization failed'}, 401

class GetFoto(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('client_username', type=str, required=True)
    parser.add_argument('token', type=str, required=True)
    parser.add_argument('id', type=str, required=True)

    def post(self):
        data = self.parser.parse_args()
        client_username = data.get('client_username', '')
        token = data.get('token', '')
        userID = authorize(client_username, token)

        if userID:
            path = Storage.getFilePath(data.get('id'))[0]
            basewidth = 300
            img = Image.open(path)
            wpercent = (basewidth / float(img.size[0]))
            hsize = int((float(img.size[1]) * float(wpercent)))
            img = img.resize((basewidth, hsize), PIL.Image.ANTIALIAS)
            buffer = BytesIO()
            img.save(buffer, format="JPEG", quality=70)
            file = base64.b64encode(buffer.getvalue()).decode('ascii')

            return {'file': file}

        else:
            return {'message': 'authorization failed'}, 401


api.add_resource(Statistics, '/mobile-fitness/statistic')
api.add_resource(GetFood, '/mobile-fitness/food')
api.add_resource(FindFood, '/mobile-fitness/food/find')
api.add_resource(AddFood, '/mobile-fitness/food/add')
api.add_resource(GetFoto, '/mobile-fitness/foto/get')
api.add_resource(DeleteFood, '/mobile-fitness/food/delete')
api.add_resource(Dashboard, '/mobile-fitness/dashboard')
api.add_resource(Settings, '/mobile-fitness/settings/get')
api.add_resource(SettingsChange, '/mobile-fitness/settings/change')
