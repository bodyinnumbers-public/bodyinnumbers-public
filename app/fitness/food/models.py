
from app import db
from flask import json
from app.auth.models import User
from sqlalchemy import func, Date
import time
import datetime
from app.storage.models import Storage

import math
class Base(db.Model):
    """Base table"""
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    date_modified = db.Column(db.DateTime, default=db.func.current_timestamp(), onupdate=db.func.current_timestamp())

class DaySettings(Base, db.Model):
    """Table for storing user's day-settings"""
    __tablename__ = 'day_settings'

    user_id = db.Column(db.Integer, db.ForeignKey(User.id))
    calorie_intake = db.Column(db.Integer)
    water_intake = db.Column(db.Integer)
    weight = db.Column(db.Integer)

    # New instance instantiation procedure
    def __init__(self, user_id, weight, calorie_intake, water_intake):
        """New food"""
        self.user_id = user_id
        self.weight = weight
        self.calorie_intake = calorie_intake
        self.water_intake = water_intake

    def get_daysetting(self, user_id):
        """Returns user's day settings.
           Args:
               self: Instance
               user_id: Id of user
           Return:
               day_settings: Day settings of user, dictionary.
        """
        day_settings = DaySettings.query.filter_by(user_id=user_id).order_by(DaySettings.id.desc()).first()
        if day_settings is None:
            #default values
            weight = 80
            calorie = 2000
            water = 2500
            DaySettings.add_settings(DaySettings, user_id, weight, calorie, water)
            day_settings = DaySettings.query.filter_by(user_id=user_id).order_by(DaySettings.id.desc()).first()
        day_set = {}
        day_set['weight'] = day_settings.weight
        day_set['calorie'] = day_settings.calorie_intake
        day_set['water'] = day_settings.water_intake

        return day_set

    def add_settings(self, user_id, weight, calorie_intake, water_intake):
        """Updates settings of user, if not exists creates new.
           Args:
               self: Instance.
               user_id: Id fo user.
               weight: User's weight.
               calorie_intake: Set calories intake per day.[g]
               water_intake: Set water intake per day.[ml]
        """
        if user_id is not None:
            day_settings = DaySettings(
                user_id=user_id,
                weight=weight,
                calorie_intake=calorie_intake,
                water_intake=water_intake
            )
            db.session.add(day_settings)
            db.session.commit()
            return day_settings.user_id

class Food(Base, db.Model):
    """Table for storing info about food"""
    __tablename__ = 'food'

    name = db.Column(db.String(256))
    water = db.Column(db.Float)
    calorie = db.Column(db.Float)
    protein = db.Column(db.Float)
    fat = db.Column(db.Float)
    carbohydrate = db.Column(db.Float)


    # New instance instantiation procedure
    def __init__(self, name, carbohydrate, protein, fat, water, calorie):
        """New food"""
        self.name = name
        self.carbohydrate = carbohydrate
        self.protein = protein
        self.fat = fat
        self.water = water
        self.calorie = calorie

    def add_food(name, water, calorie, protein, fat, carbohydrate):
        """Creates new food and add it to a table.
           Args:
               name: Name of food.
               water: Water in food.[ml]
               calorie: Calories in food.[kcal]
               protein: Protein in food.[g]
               fat: Fat in food.[g]
               carbohydrate: Carbohydrates in food.[g]
        """
        food = Food(
            name=name,
            carbohydrate=carbohydrate,
            protein=protein,
            fat=fat,
            water=water,
            calorie=calorie
        )
        db.session.add(food)
        db.session.commit()

    def find_food(self, keys, limit):
        finded = db.session.query(
            (Food.name.label('name')),
            (Food.water.label('water')),
            (Food.calorie.label('calorie')),
            (Food.protein.label('protein')),
            (Food.fat.label('fat')),
            (Food.carbohydrate.label('carbohydrate'))
        ).filter(*[Food.name.ilike(n) for n in keys]).limit(limit=limit).all()

        return finded

class FoodTracking(Base, db.Model):
    """Table for tracking eaten food """
    __tablename__ = 'food_tracking'

    user_id = db.Column(db.Integer, db.ForeignKey(User.id))
    photo_id = db.Column(db.Integer)

    def __init__(self, user_id, photo_id):
        """New nutrition info about food"""
        self.user_id = user_id
        self.photo_id = photo_id

    def add_foodtracking(user_id, photo_id):
        """Creates new food tracking and add it to a table
           Args:
               user_id: Id of logged user
               photo_id: Id of food's foto
            Return:
                food_tracking.id: Id of current added food track
        """
        food_tracking = FoodTracking(
            user_id=user_id,
            photo_id=photo_id
        )
        db.session.add(food_tracking)
        db.session.commit()
        return food_tracking.id

    def delete_food_tracking(self, food_tracking_id, user_id):
        """Deletes data from table food tracking, specified by food_trakcing_id and
            deletes data from food_definition connected to food_tarcking_id
           Args:
               food_tracking_id: id of data from food_trackin table
        """
        food_trakcing = FoodTracking.query.filter(FoodTracking.id==food_tracking_id, FoodTracking.user_id == user_id).first()
        db.session.delete(food_trakcing)
        db.session.commit()

    def comp_day(food, day_setting):
        """Computes and create dictionary of daily eaten food's components
           Args:
               food: Contains info about today's eaten food(calories, protein etc.).
               day_setting: Settings of food's components per day.
            Return:
                today_sum: Dictionary of daily eaten food's components.
        """

        # Sets of water and calories intake
        set_water = day_setting['water']
        set_calorie = day_setting['calorie']

        # Constants of recommendations
        rec_carbohydrate = math.floor((set_calorie / 100) * 45 * (1/4))
        rec_protein = math.floor((set_calorie / 100) * 30 * (1/4))
        rec_fat = math.floor((set_calorie / 100) * 25 * (1/9))

        # Create dictionary of food intake(can be jsonfied json.dumps(today_sum))
        today_sum = {}
        today_sum['calorie'] = [food[0].calorie, set_calorie]
        today_sum['carbohydrate'] = [food[0].carbohydrate, rec_carbohydrate]
        today_sum['protein'] = [food[0].protein, rec_protein]
        today_sum['fat'] = [food[0].fat, rec_fat]
        today_sum['water'] = [food[0].water, set_water]

        return today_sum

    def today_intake(self, user_id):
        """Gets today's intake of user
           Args:
               self:instance
            Return:
                intake:today's intake
        """
        today = time.strftime("%Y-%m-%d")
        #Gets data from table FoodDefinition
        food = db.session.query(
            func.count(FoodTracking.id),
            func.sum(FoodDefinition.calorie).label('calorie'),
            func.sum(FoodDefinition.carbohydrate).label('carbohydrate'),
            func.sum(FoodDefinition.protein).label('protein'),
            func.sum(FoodDefinition.fat).label('fat'),
            func.sum(FoodDefinition.water).label('water'),
        ).join(FoodDefinition, FoodDefinition.food_tracking_id == FoodTracking.id).filter(
                func.cast(FoodTracking.date_created, Date) == today,
                FoodTracking.user_id == user_id).group_by().all()

        day_setting = DaySettings.get_daysetting(DaySettings, user_id)
        intake = self.comp_day(food, day_setting)
        return intake

    def info_perweeks(self, weeks, user_id):
        """Gets intake of user by set weeks
           Args:
               self:instance
               weeks: weeks for water_intake record
               user_id: for getting only specific user water_intake

            Return:
                intake:today's intake
        """
        current_time = datetime.datetime.utcnow()
        month_ago = current_time - datetime.timedelta(weeks=weeks)

        info = db.session.query(
            FoodTracking,
            FoodDefinition,
            func.cast(FoodTracking.date_created, Date).label('date')
        ).join(FoodDefinition, FoodDefinition.food_tracking_id == FoodTracking.id).filter(
            FoodTracking.date_created > month_ago,
            FoodTracking.user_id == user_id
        ).all()

        return info

    def get_food(self, user_id, limit):

        food = db.session.query(
            FoodTracking.id.label('food_tracking_id'),
            func.cast(FoodTracking.date_created, Date).label('date'),
            FoodTracking.photo_id,
            FoodDefinition
        ).filter(FoodTracking.user_id == user_id
                 ).join(
            FoodDefinition, FoodDefinition.food_tracking_id == FoodTracking.id, isouter=True
        ).order_by(FoodTracking.id.desc()).limit(limit).all()

        return food

class FoodDefinition(Base, db.Model):
    """Table for nutrition info about food."""
    __tablename__ = 'food_definition'

    food_tracking_id = db.Column(db.Integer, db.ForeignKey(FoodTracking.id, onupdate="CASCADE", ondelete="CASCADE"))
    name = db.Column(db.String(256))
    carbohydrate = db.Column(db.Float)
    protein = db.Column(db.Float)
    fat = db.Column(db.Float)
    water = db.Column(db.Integer)
    calorie = db.Column(db.Float)
    weight = db.Column(db.Float)

    def __init__(self, food_tracking_id, name, carbohydrate, protein, fat, water, calorie, weight):
        self.food_tracking_id = food_tracking_id
        self.name = name
        self.carbohydrate = carbohydrate
        self.protein = protein
        self.fat = fat
        self.water = water
        self.calorie = calorie
        self.weight = weight

    def add_definition(food_tracking_id, name, water, calorie, protein, fat, carbohydrate, weight):
        """Creates new food definition and add it to a table.
             Args:
               food_tracking_id: Id of added food(food may contain other food).
               name: Name of food.
               water: Water in food.[ml]
               calorie: Calories in food.[kcal]
               protein: Protein in food.[g]
               fat: Fat in food.[g]
               carbohydrate: Carbohydrates in food.[g]
               weight: Weight of food.[g]
            Return:
                food_def.id: Id of current added food.
        """
        food_def = FoodDefinition(
            food_tracking_id=food_tracking_id,
            name=name,
            carbohydrate=carbohydrate,
            protein=protein,
            fat=fat,
            water=water,
            calorie=calorie,
            weight=weight
        )
        db.session.add(food_def)
        db.session.commit()
        return food_def.id
