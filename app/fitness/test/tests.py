import os
import unittest
import tempfile
from app.fitness.food.models import DaySettings, Food, FoodTracking, FoodDefinition
from app.fitness.exercise.models import Exercise, ExerciseType, ExerciseTracking
import json

class TestDaySettings(unittest.TestCase):
    def test_get_daysetting(self):
         day_settings = DaySettings.get_daysetting(DaySettings, user_id=1)
         self.assertNotEqual(len(day_settings), 0)

    def test_get_daysetting_1(self):
        DaySettings.add_settings(DaySettings, user_id=1, weight=1, calorie_intake=1, water_intake=1)
        day_settings = DaySettings.get_daysetting(DaySettings, user_id=1)
        self.assertEqual(day_settings, {'weight': 1, 'calorie': 1, 'water': 1})

class TestFood(unittest.TestCase):
    def test_find_food(self):
        array = []
        array.append("%test%")
        Food.add_food("test", 0, 0, 0, 0, 0)
        food = Food.find_food(Food, array, 10)
        self.assertNotEqual(len(food), 0)

class TestFoodTracking(unittest.TestCase):
    def test_add_foodtracking(self):
        i = FoodTracking.add_foodtracking(1, 0)
        self.assertNotEqual(i, None)

    def test_delete_food_tracking(self):
        id = FoodTracking.add_foodtracking(1, 0)
        FoodTracking.delete_food_tracking(FoodTracking, id, 1)

    def test_today_intake(self):
        day_setting = {
            'water': 1,
            'calorie': 0
        }
        id = FoodTracking.add_foodtracking(1, 0)
        FoodDefinition.add_definition(id, 'Jidlo_test', 100, 300, 1, 20, 20, 100)
        intake = FoodTracking.today_intake(FoodTracking, 1)
        self.assertNotEqual(len(intake), 0)

    def test_get_food(self):
        id = FoodTracking.add_foodtracking(1, 0)
        FoodDefinition.add_definition(id, 'Jidlo_test', 100, 300, 1, 20, 20, 100)
        food = FoodTracking.get_food(FoodTracking, 1, 1)
        self.assertNotEqual(len(food), 0)

class TestFoodDefinition(unittest.TestCase):
    def test_add_definition(self):
        id = FoodTracking.add_foodtracking(1, 0)
        id_d = FoodDefinition.add_definition(id, 'Jidlo_test', 100, 300, 1, 20, 20, 100)
        self.assertNotEqual(id_d, 0)

class TestExerciseType(unittest.TestCase):

    def test_add_exercise_type(self):
        id = ExerciseType.add_exercise_type('typ', 0)
        self.assertNotEqual(id, 0)

class TestExercise(unittest.TestCase):

    def test_add_exercise(self):
        id = Exercise.add_exercise("typ", 1, 0)
        self.assertNotEqual(id, None)

    def test_find_exercise(self):
        array = []
        array.append("%typ123456%")
        Exercise.add_exercise("typ123456", 1, 0)
        exercise = Exercise.find_exercise(Exercise,array, 1)
        self.assertNotEqual(len(exercise), 0)

    def test_get_exercise(self):
        exercise = Exercise.get_exercise(Exercise)
        self.assertNotEqual(len(exercise), 0)

class TestExerciseTracking(unittest.TestCase):

    def test_add_tracking(self):
       id = ExerciseTracking.add_tracking(4, 1, 20)
       self.assertNotEqual(id, 0)

    def test_get_exercise(self):
        ExerciseTracking.add_tracking(4, 1, 20)
        exer = ExerciseTracking.get_exercise(ExerciseTracking, 4, 1)
        self.assertNotEqual(exer, None)

if __name__ == '__main__':
    unittest.main()