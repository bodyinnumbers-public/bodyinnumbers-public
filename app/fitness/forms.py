from flask_wtf import Form
from wtforms import StringField, SubmitField, SelectField, TextAreaField, HiddenField, IntegerField, BooleanField, FileField  # Import Form elements such as StringField (optional)
from wtforms.validators import DataRequired, length  # Import Form validators


class FoodForm(Form):
    """Form for upload food"""
    foodInput = FileField('soubor')
    exerciseInput = FileField('soubor')
    save = SubmitField('Generate')

