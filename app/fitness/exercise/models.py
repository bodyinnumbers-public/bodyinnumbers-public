
from app import db
from sqlalchemy.dialects.postgresql import JSON
from flask import json
from app.auth.models import User
from sqlalchemy import func, Date
import time as time_py

class Base(db.Model):
    """Base table"""
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    date_modified = db.Column(db.DateTime, default=db.func.current_timestamp(), onupdate=db.func.current_timestamp())

def to_json(inst, cls):
    """
    Jsonify the sql alchemy query result.
    """
    convert = dict()
    # add your coversions for things like datetime's
    # and what-not that aren't serializable.
    d = dict()
    for c in cls.__table__.columns:
        v = getattr(inst, c.name)
        if c.type in convert.keys() and v is not None:
            try:
                d[c.name] = convert[c.type](v)
            except:
                d[c.name] = "Error:  Failed to covert using ", str(convert[c.type])
        elif v is None:
            d[c.name] = str()
        else:
            d[c.name] = v
    return json.dumps(d)

class ExerciseType(Base, db.Model):
    """Table for storing type of exercises"""
    __tablename__ = 'exercise_type'

    name = db.Column(db.String(64))
    foto_id = db.Column(db.Integer)

    def __init__(self, name, foto_id):
        """New nutrition info about food"""
        self.name = name
        self.foto_id = foto_id

    def add_exercise_type(name, foto_id):
        exercise_type = ExerciseType(
            name=name,
            foto_id=foto_id
        )
        db.session.add(exercise_type)
        db.session.commit()
        return exercise_type.id

class Exercise(Base, db.Model):
    """Table for storing info about exercises"""
    __tablename__ = 'exercise'

    name = db.Column(db.String(64))
    photo_id = db.Column(db.Integer)
    exercise_type_id = db.Column(db.Integer, db.ForeignKey(ExerciseType.id))

    def __init__(self, name, exercise_type_id, photo_id):
        """New nutrition info about food"""
        self.name = name
        self.exercise_type_id = exercise_type_id
        self.photo_id = photo_id

    def add_exercise(name, exercise_type_id, photo_id):
        exercise = Exercise(
            name=name,
            exercise_type_id=exercise_type_id,
            photo_id=photo_id
        )
        db.session.add(exercise)
        db.session.commit()
        return exercise.id

    def find_exercise(self, keys, limit):
        finded = db.session.query(
            (Exercise.id),
            (Exercise.name),
            (Exercise.exercise_type_id.label('type')),
        ).filter(*[Exercise.name.ilike(n) for n in keys]).limit(limit).all()
        return finded

    def get_exercise(self):
        exercises = Exercise.query.all()
        return exercises

class ExerciseTracking(Base, db.Model):
    """Copy of exercise table"""
    __tablename__ = 'exercise_tracking'

    user_id = db.Column(db.Integer, db.ForeignKey(User.id))
    exercise_id = db.Column(db.Integer, db.ForeignKey(Exercise.id))
    time = db.Column(db.Integer)

    def __init__(self, user_id, exercise_id, time):
        self.user_id = user_id
        self.time = time
        self.exercise_id = exercise_id

    def add_tracking(user_id, exercise_id, time):
        """Creates new food definition and add it to a table.
           Args:
               user_id: Id of user
               time: Time spent on exercise.
            Return:
                exer_track.id: Id of current added exercise.
        """
        exer_track = ExerciseTracking(
            user_id=user_id,
            exercise_id=exercise_id,
            time=time
        )
        db.session.add(exer_track)
        db.session.commit()
        return exer_track.id

    def delete_tracking(self, user_id, exercise_tracking_id):
        exercise_tracking = ExerciseTracking.query.filter(ExerciseTracking.id == exercise_tracking_id, ExerciseTracking.user_id == user_id).first()
        db.session.delete(exercise_tracking)
        db.session.commit()

    def get_exercise(self, user_id, limit):
        exercise_track = db.session.query(
            ExerciseTracking.id,
            func.cast(ExerciseTracking.date_created, Date).label('date'),
            ExerciseTracking.time,
            Exercise.name,
            Exercise.photo_id,
            ExerciseType.name.label('type_name')
        ).filter(
            ExerciseTracking.user_id == user_id
        ).join(
            Exercise, Exercise.id == ExerciseTracking.exercise_id
        ).join(
            ExerciseType, ExerciseType.id == Exercise.exercise_type_id
        ).order_by(
            ExerciseTracking.id.desc()
        ).limit(limit).all()

        return exercise_track