from flask import Blueprint, request, render_template, redirect, jsonify, abort, url_for
from app import app, db
from app.auth.models import User
from flask_login import login_required
from app.fitness.forms import FoodForm
import csv
import io
import json
from flask_restful import reqparse, Resource, url_for, Api
from app.mobile_services.controllers import authorize
import time as time_py
from app.fitness.exercise.models import Exercise, ExerciseTracking

fitness_exercise = Blueprint('fitness_exercise', __name__, template_folder='templates', url_prefix='/app')


@fitness_exercise.route('/fitness/exercise/test')
def sample():
    """Sample blank page"""
    return 'ok'

api = Api(fitness_exercise)

class GetExercise(Resource):
    """Gets last 50 made exercise of user"""
    parser = reqparse.RequestParser()
    parser.add_argument('client_username', type=str, required=True)
    parser.add_argument('token', type=str, required=True)

    def serialize(self, item):
        return {
            "id": item.id,
            "exercise_name": item.name,
            "exercise_type_name": item.type_name,
            "photo_id": item.photo_id,
            "time": item.time,
            "date": str(item.date.day) + "/" + str(item.date.month) + "/" + str(item.date.year)
        }

    def post(self):
        data = self.parser.parse_args()
        client_username = data.get('client_username', '')
        token = data.get('token', '')

        userID = authorize(client_username, token)
        if userID:
            #Gets last 50 food eaten by user
            exercise_tracks = ExerciseTracking.get_exercise(ExerciseTracking, userID, 50)
            return jsonify(json_list=[self.serialize(i) for i in exercise_tracks])
        else:
            return {'message': 'authorization failed'}, 401

class AddExercise(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('client_username', type=str, required=True)
    parser.add_argument('token', type=str, required=True)
    parser.add_argument('exercise', type=str, required=True)

    def post(self):
        data = self.parser.parse_args()
        client_username = data.get('client_username', '')
        token = data.get('token', '')

        userID = authorize(client_username, token)
        if userID:
            # json of done exercise like this ("exercise":
            # {"id":"1","time":"400"})
            exercise_json = data.get('exercise', '')
            exercise = json.loads(exercise_json)
            # Adding done exercise to database
            ExerciseTracking.add_tracking(
                userID,
                exercise['id'],
                exercise['time'])
            return "ok"
        else:
            return {'message': 'authorization failed'}, 401

class DeleteTrack(Resource):
    """Delete exercise_tracking record"""
    parser = reqparse.RequestParser()
    parser.add_argument('client_username', type=str, required=True)
    parser.add_argument('token', type=str, required=True)
    parser.add_argument('id', type=str, required=True)

    def post(self):
        data = self.parser.parse_args()
        client_username = data.get('client_username', '')
        token = data.get('token', '')
        userID = authorize(client_username, token)

        if userID:
            id = data.get('id', '')
            id = int(id)
            ExerciseTracking.delete_tracking(ExerciseTracking, userID, id)
            return "ok"
        else:
            return {'message': 'authorization failed'}, 401

class ListExercise(Resource):
    """Finds 10 most similar food by name"""
    parser = reqparse.RequestParser()
    parser.add_argument('client_username', type=str, required=True)
    parser.add_argument('token', type=str, required=True)

    def post(self):
        data = self.parser.parse_args()
        client_username = data.get('client_username', '')
        token = data.get('token', '')

        userID = authorize(client_username, token)
        if userID:
            exercises = Exercise.get_exercise(Exercise)
            return jsonify(json_list=[self.serialize(i) for i in exercises])
        else:
            return {'message': 'authorization failed'}, 401

    def serialize(self, item):
        return {
            "id": item.id,
            "name": item.name,
        }

api.add_resource(GetExercise, '/mobile-fitness/exercise')
api.add_resource(ListExercise, '/mobile-fitness/exercise/get')
api.add_resource(AddExercise, '/mobile-fitness/exercise/add')
api.add_resource(DeleteTrack, '/mobile-fitness/exercise/delete')