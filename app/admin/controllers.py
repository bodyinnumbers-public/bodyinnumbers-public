from flask import Blueprint, request, render_template, flash, g, session, redirect, url_for
from app import db, bcrypt
from datetime import datetime
from flask_bcrypt import Bcrypt, check_password_hash, generate_password_hash
from app.auth.models import User, Role
from app.measurement.models import Measurement
from app.measurement.forms import NewMeasurementForm, EditMeasurementForm
from app.equipment.models import MeasuringEquipment
from app.experiments.models import Experiments
from flask_login import login_user, logout_user, login_required, current_user
from flask import json


admin = Blueprint('admin', __name__, template_folder='templates', url_prefix='/app/admin')

@admin.route('/')
@login_required
def show_all_users():
	"""Show users"""
	all_users = db.session.query(User)
	user_roles = db.session.query(User).join(Role, User.roles)
	

	return render_template("users.html", data = all_users)


@admin.route('/change_permission<int:id_user>')
@login_required
def change_permission():
	"""change permission"""
	user = db.session.query(User).filter_by(id = id_user)
	if user.roles[0].name == "admin":
		user.roles.remove(role)
	else:
		role = db.session.query(Role).filter_by(name = "admin")
		user.append(role)
	return redirect(url_for('admin.show_all_users'))


@admin.route('/init-experiments')
@login_required
def initExperiments():
    """init experiments"""
    reg = '[{"id":0,"name":"Pohlaví","formType":"radio","required":false,"formLabels":["Muž","Žena"],"units":""},{"id":1,"name":"Věk","formType":"integer","required":false,"formLabels":[""],"units":""},{"id":2,"name":"Pravák / Levák","formType":"radio","required":false,"formLabels":["Pravák","Levák"],"units":""},{"id":3,"name":"Podpis","formType":"checkbox","required":false,"formLabels":[""],"units":""},{"id":4,"name":"ID dotazníku","formType":"integer","required":false,"formLabels":[""],"units":""}]'
    registrationExp = Experiments(
        experimentName = "Registrace",
        experimentScheme = json.loads(reg),
        description = "Registation"
    )
    
    heightweight = '[{"id":0,"name":"Výška","formType":"integer","required":false,"formLabels":[""],"units":"cm"},{"id":1,"name":"Váha","formType":"float","required":false,"formLabels":[""],"units":"kg"},{"id":2,"name":"BMI","formType":"float","required":false,"formLabels":[""],"units":""},{"id":3,"name":"Svalová hmota","formType":"float","required":true,"formLabels":[""],"units":"%"},{"id":4,"name":"Voda","formType":"float","required":true,"formLabels":[""],"units":"%"},{"id":5,"name":"Tuk","formType":"float","required":true,"formLabels":[""],"units":"%"}]'
    heightWeightExp = Experiments(
        experimentName = "Výška a váha",
        experimentScheme = json.loads(heightweight),
        description = "Výška, váha, BMI, svalová hmota, voda, tuk"
    )
    
    #questions = '[{"id":0,"name":"Sportujete pravidelně?","formType":"radio","required":false,"formLabels":["Ano","Ne"],"units":""},{"id":1,"name":"Pokud ne. Chtěl byste sportovat?","formType":"radio","required":false,"formLabels":["Ano","Ne"],"units":""},{"id":2,"name":"Pokud ano. Máte přátele se kterými byste mohl sportovat?","formType":"radio","required":false,"formLabels":["Ano","Ne"],"units":""},{"id":3,"name":"Stravujete se pravidelně?","formType":"radio","required":false,"formLabels":["Ano","Ne"],"units":""},{"id":4,"name":"Dodržujete pitný režim? (2-3 litry tekutin denně)","formType":"radio","required":false,"formLabels":["Ano","Ne"],"units":""},{"id":5,"name":"Jíte zdravě? např. drůbež, ryby, ovoce, zelenina, pitný režim (voda)","formType":"radio","required":false,"formLabels":["Ano","Ne"],"units":""},{"id":6,"name":"Užíváte nějaké doplňky stravy? např. vitamíny, activia, doplňky na klouby a kosti","formType":"radio","required":false,"formLabels":["Ano","Ne"],"units":""},{"id":7,"name":"Jste kuřák?","formType":"radio","required":false,"formLabels":["Ano","Ne","Občasný"],"units":""},{"id":8,"name":"Pokud ano. Kolik cigaret vykouříte?","formType":"radio","required":false,"formLabels":["20 a více cigaret denně","Do 20 cigaret denně","Do 10 cigaret denně","Do 10 cigaret týdně","Do 10 cigaret měsíčně"],"units":""},{"id":9,"name":"Jak často konzumujete alkoholické nápoje? ","formType":"radio","required":false,"formLabels":["Nekonzumuji","Příležitostně","Jednou za týden","Vícekrát za týden","Každý den"],"units":""},{"id":10,"name":"Podstupujete pravidelné lékařské prohlídky?","formType":"radio","required":false,"formLabels":["Ano","Ne"],"units":""},{"id":11,"name":"Máte přítelkyni/přítele či manžela/manželku?","formType":"radio","required":false,"formLabels":["Ano","Ne"],"units":""},{"id":12,"name":"Dopřáváte si správnou relaxaci a odpočinek? např. masáže, wellness, dostatečný spánek (min. 6h)","formType":"radio","required":false,"formLabels":["Ano","Ne"],"units":""}]'
    #questionsExp = Experiments(
    #    experimentName = "Dotazník",
    #    experimentScheme = json.loads(questions),
    #    description = "dotazník"
    #)
    
    questions = '[{"id":0,"name":"Cítíte se fyzicky a psychicky zdráv/a?","formType":"radio","required":true,"formLabels":["Ano","Jen fyzicky","Jen psychicky","Ne"],"units":""},{"id":1,"name":"Jakou fyzickou aktivitu upřednostňujete?","formType":"radio","required":true,"formLabels":["Např. běhání, jogging, tanec, cyklistika, plavání apod. (aerobní sporty)","Např. posilování, bosu, pilates, box apod. (anaerobní sporty)","Nesportuji vůbec"],"units":""},{"id":2,"name":"Jak často provozujete fyzickou aktivitu?","formType":"radio","required":true,"formLabels":["Profesionálně (pravidelně)","Rekreačně (občas)","Neprovozuji fyzickou aktivitu vůbec"],"units":""},{"id":3,"name":"Dodržujete pravidelný stravovací režim?","formType":"radio","required":true,"formLabels":["Ano, jím 5 jídel denně","Ano, jím jen 3 jídla denně","Ne, jím jen, když mám hlad a čas","Ne, jím jen, když si na to vzpomenu"],"units":""},{"id":4,"name":"Dodržujete pravidelný pitný režim?","formType":"radio","required":true,"formLabels":["Ano, vypiji do 1,5 l čisté vody denně","Ano, vypiji nad 1,5 l čisté vody denně","Do denního příjmu tekutin zařazuji hlavně čaj, kávu, limonádu, alkohol apod."],"units":""},{"id":5,"name":"Jak často pijete nápoje s obsahem kofeinu? (např. káva, čaj, guarana, maté, kolové a energetické nápoje apod.)","formType":"radio","required":true,"formLabels":["Jednou denně","Víckrát, než jednou denně","Nepravidelně","Nápoje s kofeinem nepiji vůbec"],"units":""},{"id":6,"name":"Nejčastěji se stravuji:","formType":"radio","required":true,"formLabels":["Racionálně pestrou stravu s hodně ovoce a zeleniny (jím vše a vyváženě)","Fastfoody (bagety, pizzy, hamburgery apod.)","Ve stravě se omezuji/držím dietu"],"units":""},{"id":7,"name":"Jak často jíte nějakou sladkost? (př. bonbon, čokoláda, sušenky, slazené jogurty, kobliha, apod.). Předpokládejte, že jíte více než jeden kus sladkosti (např. balení, jedna tabulka, …).","formType":"radio","required":true,"formLabels":["Nejím sladkosti vůbec","Jednu a více denně","Jednou týdně","Jednou měsíčně"],"units":""},{"id":8,"name":"Užíváte pravidelně nějaké léky (u žen antikoncepce) či nějaké doplňky stravy?","formType":"radio","required":true,"formLabels":["Ano, obojí","Ano, pouze léky (u žen antikoncepce)","Ano, pouze doplňky stravy","Neužívám ani jedno"],"units":""},{"id":9,"name":"Trápí vás nějaké chronické onemocnění?","formType":"radio","required":true,"formLabels":["Ano","Ne"],"units":""},{"id":10,"name":"Chodíte k lékaři na pravidelné preventivní prohlídky? Pravidelné návštěvy lékaře se myslí (např. půlroční prohlídky, měsíční, …)","formType":"radio","required":true,"formLabels":["Ano","Ne"],"units":""},{"id":11,"name":"Kouříte?","formType":"radio","required":true,"formLabels":["Nekouřím vůbec","Kouřím příležitostně","Denně vykouřím 1-5 cigaret","Denně vykouřím 6-10 cigaret","Denně vykouřím více než 11 cigaret"],"units":""},{"id":12,"name":"Jak často konzumujete alkohol?","formType":"radio","required":true,"formLabels":["Nekonzumuji, jsem abstinent","Jen příležitostně","1x do měsíce","Každý týden","Každý den"],"units":""},{"id":13,"name":"Máte ve své blízkosti min. 1 přítele, o kterém víte, že vás kdykoliv podrží, pomůže a že se na něj můžete spolehnout?","formType":"radio","required":true,"formLabels":["Ano","Myslím si, že ano","Ne"],"units":""},{"id":14,"name":"Aktuálně zažívám:","formType":"radio","required":true,"formLabels":["Období bez stresu","Nízký stupeň stresu","Střední stupeň stresu","Vysoký stupeň stresu"],"units":""},{"id":15,"name":"Nejčastěji relaxuji:","formType":"radio","required":true,"formLabels":["Aktivně (procházka, sport, práce na zahrádce, sex apod.)","Pasivně (koukání na TV, čtení, spaní, pletení, hraní PC her apod.)","Nerelaxuji vůbec"],"units":""},{"id":16,"name":"Sex provozuji: (nepovinná otázka)","formType":"radio","required":true,"formLabels":["Každý den","Pravidelně min. 1 týdně","Pravidelně min. 1 měsíčně","Nepravidelně","Neprovozuji vůbec"],"units":""},{"id":17,"name":"Denně spím:","formType":"radio","required":true,"formLabels":["Méně než 6h","6-8 hodin","8-10 hodin","Více jak 10 hodin"],"units":""},{"id":18,"name":"Měsíčně vydělávám tolik financí, že","formType":"radio","required":true,"formLabels":["Na své finance nemusím vůbec myslet (je mi to jedno)","Myslím si, že mám na všechno, co potřebuji","Když kupuji věcí běžné spotřeby, občas se nad financemi zamyslím","Trápí mě, že nemám dostatek peněz"],"units":""}]'
    questionsExp = Experiments(
        experimentName = "Motivační stupeň",
        experimentScheme = json.loads(questions),
        description = "Motivační stupeň"
    )
    
    iq = '[{"id":0,"name":"IQ","formType":"integer","required":false,"formLabels":[""],"units":""}]'
    iqExp = Experiments(
        experimentName = "IQ",
        experimentScheme = json.loads(iq),
        description = "hodnota IQ"
    )

    react1 = '[{"id":0,"name":"Reakční doba horních končetin","formType":"integer","required":false,"formLabels":[""],"units":"ms"},{"id":1,"name":"Zameškáno horní končetiny","formType":"integer","required":false,"formLabels":[""],"units":""},{"id":2,"name":"Chyba horní končetiny","formType":"integer","required":false,"formLabels":[""],"units":""}]'
    reactions1 = Experiments(
        experimentName = "Reakční doba horních končetin",
        experimentScheme = json.loads(react1),
        description = "Reakční doba horních končetin"
    )
    
    react2 = '[{"id":0,"name":"Reakční doba dolní končetiny","formType":"integer","required":false,"formLabels":[""],"units":"ms"},{"id":1,"name":"Směrodatná odchylka dolní končetiny","formType":"integer","required":false,"formLabels":[""],"units":"ms"},{"id":2,"name":"Nejlepší čas dolní končetiny","formType":"integer","required":false,"formLabels":[""],"units":"ms"},{"id":3,"name":"Nejhorší čas dolní končetiny","formType":"integer","required":false,"formLabels":[""],"units":"ms"}]'
    reactions2 = Experiments(
        experimentName = "Reakční doba dolních končetin",
        experimentScheme = json.loads(react2),
        description = "Reakční doba dolních končetin"
    )
    
    col = '[{"id":0,"name":"Barvocit","formType":"checkbox","required":false,"formLabels":["Obr. 1","Obr. 2","Obr. 3","Obr. 4","Obr. 5","Obr. 6","Obr. 7","Obr. 8"],"units":""}]'
    colors = Experiments(
        experimentName = "Barvocit",
        experimentScheme = json.loads(col),
        description = "Barvocit"
    )
    
    spiro = '[{"id":0,"name":"Vitální kapacita plic","formType":"float","required":false,"formLabels":[""],"units":"l"},{"id":1,"name":"Vydechnutý objem za první s.","formType":"float","required":false,"formLabels":[""],"units":"l"},{"id":2,"name":"Vrcholový výdechový průtok","formType":"float","required":false,"formLabels":[""],"units":"l/s"}]'
    spirometry = Experiments(
        experimentName = "Spirometrie",
        experimentScheme = json.loads(spiro),
        description = "Spirometrie"
    )
    
    press = '[{"id":0,"name":"Systolický tlak","formType":"integer","required":false,"formLabels":[""],"units":"mmHg"},{"id":1,"name":"Diastolický tlak","formType":"integer","required":false,"formLabels":[""],"units":"mmHg"},{"id":2,"name":"Puls","formType":"integer","required":false,"formLabels":[""],"units":"puls/min"}]'
    pressure = Experiments(
        experimentName = "Krevní tlak",
        experimentScheme = json.loads(press),
        description = "Krevní tlak a puls"
    )
    
    gluc = '[{"id":0,"name":"Glykemie","formType":"float","required":true,"formLabels":[""],"units":"mmol/l"},{"id":1,"name":"Jídlo před měřením","formType":"radio","required":true,"formLabels":["Ano","Ne"],"units":""}]'
    glucose = Experiments(
        experimentName = "Glykemie + meta",
        experimentScheme = json.loads(gluc),
        description = "Měření glukózy v krvi"
    )
    
    flex = '[{"id":0,"name":"Pružnost","formType":"integer","required":false,"formLabels":[""],"units":"cm"}]'
    flexibility = Experiments(
        experimentName = "Pružnost",
        experimentScheme = json.loads(flex),
        description = "Měření pružnosti páteře"
    )
    
    bal = '[{"id":0,"name":"Váha levá noha","formType":"float","required":true,"formLabels":[""],"units":"kg"},{"id":1,"name":"Váha pravá noha","formType":"float","required":true,"formLabels":[""],"units":"kg"}]'
    balance = Experiments(
        experimentName = "Rovnováha",
        experimentScheme = json.loads(bal),
        description = "Rovnováha"
    )
    
    forc = '[{"id":0,"name":"Síla levá ruka","formType":"integer","required":true,"formLabels":[""],"units":"N"},{"id":1,"name":"Síla pravá ruka","formType":"integer","required":true,"formLabels":[""],"units":"N"}]'
    force1 = Experiments(
        experimentName = "Síla",
        experimentScheme = json.loads(forc),
        description = "Síla"
    )
    force2 = Experiments(
        experimentName = "Síla při předpažení",
        experimentScheme = json.loads(forc),
        description = "Síla předpažení"
    )
    
    
    spiro2 = '[{"id":0,"name":"Vitální kapacita plic","formType":"float","required":false,"formLabels":[""],"units":"l"},{"id":1,"name":"Vydechnutý objem za první s.","formType":"float","required":false,"formLabels":[""],"units":"l"},{"id":2,"name":"Vrcholový výdechový průtok","formType":"float","required":false,"formLabels":[""],"units":"l/s"}]'
    spirometry2 = Experiments(
        experimentName = "Spirometrie zátěž",
        experimentScheme = json.loads(spiro2),
        description = "Spirometrie zátěž"
    )
    
    brain = '[{"id":0,"name":"BCI","formType":"checkbox","required":true,"formLabels":[""],"units":""}]'
    bci = Experiments(
        experimentName = "BCI",
        experimentScheme = json.loads(brain),
        description = "BCI"
    )
    
    db.session.add(registrationExp)
    db.session.add(heightWeightExp)
    db.session.add(questionsExp)
    db.session.add(iqExp)
    db.session.add(reactions1)
    db.session.add(reactions2)
    db.session.add(colors)
    db.session.add(spirometry)
    db.session.add(pressure)
    db.session.add(glucose)
    db.session.add(flexibility)
    db.session.add(balance)
    db.session.add(force1)
    db.session.add(force2)
    db.session.add(spirometry2)
    db.session.add(bci)
    
    db.session.commit()
    
    return "ok"
