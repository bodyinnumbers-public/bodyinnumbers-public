/**
 * Created by Cheathunter on 13. 7. 2016.
 */

function showForm( jQuery ) {

    /*$('.close').click( function() {
        unloadAddPopup();
    });*/

    $('#add_link').click( function() {
        loadAddPopup();
    });

    //$.post('/app/planning_add', $('#fld').serialize())
    $("#form_add_plan").submit(function(event) {

        /* stop form from submitting normally */
        event.preventDefault();

        /* get the action attribute from the <form action=""> element */
        var $form = $(this),
            url = $form.attr('action');

        /* Send the data using post */
        var posting = $.post(url, {
            csrf_token: $('#csrf_token').val(),
            institution: $('#institution').val(),
            begin: $('#begin').val(),
            end: $('#end').val(),
            town: $('#town').val(),
            street: $('#street').val(),
            number: $('#number').val()
        });

        /* Continue working with the results */
        posting.done(function(data) {
            var state = document.getElementById('stateP');
            if (data.result != -1) {
                $('#measurement_id').val(data.result);
                $('.close').attr('href', '/app/planning/show');
                $('.close').attr('onclick', '');
                state.style.color = 'green';
                document.getElementById("stateP").innerHTML = "Plán měření přidán!";
                document.getElementById("headline").innerHTML = "Přidat úkol k měření";
                nextForm();
            } else {
                state.style.color = 'red';
                document.getElementById("stateP").innerHTML = "Zadané údaje jsou špatné!";
            }
        });
    });

    $("#form_add_task").submit(function(event) {

        /* stop form from submitting normally */
        event.preventDefault();

        /* get the action attribute from the <form action=""> element */
        var $form = $(this),
            url = $form.attr('action');

        /* Send the data using post */
        var posting = $.post(url, {
            csrf_token: $('#csrf_token').val(),
            experimentName: $('#experimentName').val(),
            experimentExecutor: $('#experimentExecutor').val(),
            measurement_id: $('#measurement_id').val()
        });

        /* Continue working with the results */
        posting.done(function(data) {
            var state = document.getElementById('stateP');
            if (data.result != -1) {
                $('#experimentName').val('');
                $('#experimentExecutor').val('');
                state.style.color = 'green';
                document.getElementById("stateP").innerHTML = "Činnost přidána!";
            } else {
                state.style.color = 'red';
                document.getElementById("stateP").innerHTML = "Zadané údaje se neshodují s údaji v databázi!";
            }
        });
    });

    function loadAddPopup() {    // To Load the Popupbox
        $('.before_next').show('slow');
        $('.after_next').css('display', 'none');
        $('.show_all').css('display', 'none');
        $('#add_popup').show("slow");
        $(".close").css({ // this is just for style
            "opacity": "0.3"
        });
    }
    
}

function unloadAddPopup() {    // TO Unload the Popupbox
    $('#add_popup').hide("slow");
    $(".close").css({ // this is just for style
        "opacity": "1"
    });
    window.history.back();
}

function loadShowDetail() {
    $('#show_popup').show("slow");
}

function unloadShowDetail() {    // TO Unload the Popupbox
    $('#show_popup').hide("slow");
    $(".close").css({
        "opacity": "1"
    });
}


function showEdit(id, category, name, description) {
    $('#deviceName_e').val(name);
    $('#deviceCategory_e').val(category);
    $('#device_id').val(id);
    $('#description_e').val(description);

    $('#edit_popup').show("slow");
}

function showEdit(id, institution, town, street, number, begin, end) {
    $('#institution_e').val(institution);
    $('#town_e').val(town);
    $('#street_e').val(street);
    $('#number_e').val(number);
    $('#begin_e').val(begin);
    $('#end_e').val(end);
    $('#measure_id').val(id);

    $('#edit_popup').show("slow");
}

function closeEdit() {
    $('#edit_popup').hide("slow");
    window.history.back();
}

function loadAddCategory() {
    $('#add_cat_popup').show("slow");
}

function unloadAddCategory() {
    $('#add_cat_popup').hide("slow");
    window.history.back();
}

function nextForm() {
    $('.before_next').hide('slow');
    $('.after_next').css('display', 'block');
}

function showExperiments(key) {
    $('.show_all').show("slow");
    $("#show_a tbody").remove();
    var table = document.getElementById('show_a');
    var tableBody = document.createElement('TBODY');
    table.appendChild(tableBody);

    var tr = document.createElement('TR');
    tableBody.appendChild(tr);
    var th = document.createElement('TH');
    tr.appendChild(th);

    if (key == 2) {
        th.appendChild(document.createTextNode('Jméno experimentátora'));

        $.getJSON('/app/show_executors', function (data) {
            createRow(data, tableBody, key);
        });
        return false;
    } else if (key == 1) {
        th.appendChild(document.createTextNode('Název experimentu'));

        $.getJSON('/app/show_experiments', function (data) {
            createRow(data, tableBody, key);
        });
        return false;
    }

}

function createRow(data, tableBody, key) {
    for (i = 0; i < data.result.length; i++) {
        var tr = document.createElement('TR');
        var td = document.createElement('TD');
        var a = document.createElement('a');
        a.appendChild(document.createTextNode(data.result[i]));
        //a.id = data.ids[i]; //funguje!
        a.setAttribute('href', '#');
        a.addEventListener("click", function(event) {
            fillTextField(this.childNodes[0].data, this.id, key);
            event.preventDefault();
        });
        td.appendChild(a);
        tr.appendChild(td);
        tableBody.appendChild(tr);
    }
}

function fillTextField(value, id, key) {
    if (key == 1) {
        $('#experimentName').val(value);
        //$('#experiment_id').val(id);
    } else if (key == 2) {
        $('#experimentExecutor').val(value);
        //$('#executor_id').val(id);
    }
}
