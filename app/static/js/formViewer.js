var form;
var inputStrings = [];
var experimentNames = [];

function showForm() {
	form = document.getElementById('form');
	
	//inputStrings.push('[{"id":0,"name":"Systolický tlak","formType":"integer","required":true,"formLabels":["test1"],"units":"mm/hg"},{"id":1,"name":"Diastolický tlak","formType":"integer","required":true,"formLabels":["test2"],"units":"mm/hg"}]');
	experimentNames.push("Krevní tlak");
	
	var name = document.createElement('h4');
	var t = document.createTextNode('ID měřené osoby');
	name.appendChild(t);
	
	var inputElement = document.createElement('input');
	inputElement.setAttribute('type','text');
	
	var br = document.createElement('br');
	
	form.appendChild(name);
	form.appendChild(inputElement);
	form.appendChild(br);
	
	var len = inputStrings.length;
	for(var i = 0; i < len; i++) {
		showExperiment(experimentNames[i], inputStrings[i]);
	}
	
	var button = document.createElement('button');
	button.setAttribute('type', 'button');
	var b = document.createTextNode('Uložit');
	button.appendChild(b);
	form.appendChild(button);
}

function showExperiment(expName, inputString) {
	var heading = document.createElement('h3');
	var t = document.createTextNode(expName);
	heading.appendChild(t);
	form.appendChild(heading);
	
	var input = JSON.parse(inputString);
	var len = input.length; //length of JSON array
	for(var i = 0; i < len; i++) {
		var formType = input[i].formType;//formType from JSON
		switch(formType) {
			case "integer":
				addInteger(input[i]);
				break;
			case "float":
				addFloat(input[i]);
				break;
			case "text":
				addText(input[i]);
				break;
			case "checkbox":
				addCheckbox(input[i]);
				break;
			case "radio":
				addRadio(input[i]);
				break;
			case "file":
				addFile(input[i]);
				break;
			default:
				alert("JSON read error!");
		}
	}
}


function addInteger(input) {
	var name = document.createElement('h4');
	var text = input.name;
	if(input.units != '') text += ' [' + input.units + ']';
	var t = document.createTextNode(text);
	name.appendChild(t);
	
	var inputElement = document.createElement('input');
	inputElement.setAttribute('type','number');
	inputElement.setAttribute('step','1');
	inputElement.setAttribute('placeholder', input.formLabels[0])
	
	var br = document.createElement('br');
	
	form.appendChild(name);
	form.appendChild(inputElement);
	form.appendChild(br);
}

function addFloat(input) {
	var name = document.createElement('h4');
	var text = input.name;
	if(input.units != '') text += ' [' + input.units + ']';
	var t = document.createTextNode(text);
	name.appendChild(t);
	
	var inputElement = document.createElement('input');
	inputElement.setAttribute('type','number');
	inputElement.setAttribute('step','any');
	inputElement.setAttribute('placeholder', input.formLabels[0])
	
	var br = document.createElement('br');
	
	form.appendChild(name);
	form.appendChild(inputElement);
	form.appendChild(br);
}

function addText(input) {
	var name = document.createElement('h4');
	var t = document.createTextNode(input.name);
	name.appendChild(t);
	
	var inputElement = document.createElement('input');
	inputElement.setAttribute('type','text');
	inputElement.setAttribute('placeholder', input.formLabels[0])
	
	var br = document.createElement('br');
	
	form.appendChild(name);
	form.appendChild(inputElement);
	form.appendChild(br);
}

function addCheckbox(input) {
	var name = document.createElement('h4');
	var t = document.createTextNode(input.name);
	name.appendChild(t)
	
	var len = input.formLabels.length;
	for(var i = 0; i < len; i++) {
		var label = document.createElement('label');
		var t = document.createTextNode(input.formLabels[i]);
		label.setAttribute('for', '');
		label.setAttribute('id','');
		label.appendChild(t);
		
		var inputElement = document.createElement('input');
		inputElement.setAttribute('type','checkbox');
	
		var br = document.createElement('br');
		
		form.appendChild(inputElement);
		form.appendChild(label);
		form.appendChild(br);
	}

}

function addRadio(input) {
	var name = document.createElement('h4');
	var t = document.createTextNode(input.name);
	name.appendChild(t);
	
	var br = document.createElement('br');
	
	form.appendChild(br);
}

function addFile(input) {
	var name = document.createElement('h4');
	var t = document.createTextNode(input.name);
	name.appendChild(t);
	
	var inputElement = document.createElement('input');
	inputElement.setAttribute('type','file');
	
	var br = document.createElement('br');
	
	form.appendChild(br);
}

window.onload = showForm;
