/*
-------------------div(id=formWrapper) -----------------------
|															 |
|  --------------- form(id=form+number) -------------------  |
|  |                                                      |  |
|  |               zbytek tlačítek...                     |  |
|  |	------- div(id=inputsWrapper+number) -----------  |  |
|  |    |                                              |  |  |
|  |    |

*/

var count = 0;
var id_generator = 0;
var expFormScheme = [];
// array for select box - value, text
var formTypes = [];
formTypes.push(['integer', 'Integer (spinbox)']);
formTypes.push(['float', 'Float (spinbox)']);
formTypes.push(['text', 'Short text']);
formTypes.push(['checkbox', 'Checkbox']);
formTypes.push(['radio', 'Radiobutton']);
formTypes.push(['file', 'Input file']);

/* object */
function experimentForm(id) {
		this.id = id;
		this.name = '';
		this.formType = 'integer';
		this.required = true;
		this.formLabels = [];
		this.units = '';
}

/* Add new form to HTML */
function addForm() {
	var expForm = new experimentForm(id_generator);
	expFormScheme.push(expForm);
	
	//div wrapper
	var formWrapper = document.createElement('div');
	formWrapper.setAttribute('id', 'formWrapper' + id_generator);
	formWrapper.setAttribute('class', 'form');
	
	//form
	var f = document.createElement('form');
	f.setAttribute('method','post');
	f.setAttribute('action','#');
	f.setAttribute('id', 'form' + id_generator);

	// input wrapper
	var inputWrapper = document.createElement('div');
	inputWrapper.setAttribute('id', 'inputWrapper' + id_generator);
	//inputWrapper.setAttribute('class', '??');
	
	//required checkbox label
	var requiredLabel = document.createElement('label');
	var t = document.createTextNode('Required');
	requiredLabel.setAttribute('for', 'required');
	requiredLabel.appendChild(t);
	
	//required checkbox
	var requiredBox = document.createElement('input');
	requiredBox.setAttribute('type', 'checkbox');
	requiredBox.setAttribute('name','required');
	requiredBox.setAttribute('onclick','changeRequired(' + id_generator + ');');
	requiredBox.setAttribute('id','required' + id_generator);
	requiredBox.checked = true;

	//delete button
	var del = document.createElement('button'); //delete form button
	del.setAttribute('type', 'button');
	del.setAttribute('onclick','deleteForm(' + id_generator + ')');
	var t = document.createTextNode('×');
	del.appendChild(t);
	
	//div for buttons add and remove
	var divButtons = document.createElement('div');
	divButtons.setAttribute('id', 'buttons' + id_generator);
	divButtons.setAttribute('class', 'buttons');
	divButtons.style.visibility = "hidden";
	
	//button add input
	var add = document.createElement('button');
	add.setAttribute('onclick','addInput(' + id_generator + ')');
	add.setAttribute('type', 'button');
	var t = document.createTextNode('+');
	add.appendChild(t);
	
	//button remove input
	var remove = document.createElement('button');
	remove.setAttribute('onclick','removeInput(' + id_generator + ');');
	remove.setAttribute('type', 'button');
	var t = document.createTextNode('-');
	remove.appendChild(t);
	
	//combobox form type 
	var formType = document.createElement('select');
	formType.setAttribute('onchange', 'changeForm(' + id_generator + ');');
	formType.setAttribute('id', 'select' + id_generator);
	
	for(var i = 0; i < formTypes.length; i++) {
		var formTypeItem = document.createElement('option');
		formTypeItem.setAttribute('value', formTypes[i][0]);
		var t = document.createTextNode(formTypes[i][1]);
		formTypeItem.appendChild(t);
		formType.appendChild(formTypeItem);
	}
	
	f.appendChild(requiredLabel);
	f.appendChild(requiredBox);
	f.appendChild(del);
	f.appendChild(formType);
	
	divButtons.appendChild(add);
	divButtons.appendChild(remove);
	f.appendChild(divButtons);
	f.appendChild(inputWrapper);
	formWrapper.appendChild(f);
	
    document.getElementById("experiment-form").appendChild(formWrapper);
    
    //add heading
    addHeading(id_generator);
	//add first input
	addInput(id_generator);
    
	count++;
	id_generator++;
}

/* Remove form from HTML */
function deleteForm(idForm) {
	index = getIndexFromId(idForm);
	expFormScheme.splice(index, 1);
	
	var node = document.getElementById('form' + idForm);
	while (node.firstChild) {
		node.removeChild(node.firstChild);
	}
	node = document.getElementById('formWrapper' + idForm);
	node.parentNode.removeChild(node);
}

/* Handler combobox select formType, changes type of form */
function changeForm(id) {
	var newFormType = document.getElementById('select' + id).value;
	
	index = getIndexFromId(id);
	expFormScheme[index].formType = newFormType;
	// remove old elements from form
	
	var node = document.getElementById('inputWrapper' + id);
	while (node.firstChild) {
		node.removeChild(node.firstChild);
	}
	
	
	//var hlabel = document.getElementById('headingLabel' + id);
	//var heading =  document.getElementById('heading' + id);
	//for(var i = 0; i < expFormScheme[index].formLabels.length; i++) {
		//var label = document.getElementById('form' + id + 'inputLabel' + i);
		//var input = document.getElementById('form' + id + 'input' + i);
		//var lastbr = document.getElementById('form' + id).getElementsByTagName("br").length - 1;
		//var br = document.getElementById('form' + id).getElementsByTagName("br")[lastbr];
		//label.parentNode.removeChild(label);
		//input.parentNode.removeChild(input);
		//br.parentNode.removeChild(br);
	//}
	expFormScheme[index].formLabels.splice(0, expFormScheme[index].formLabels.length);
	addHeading(id);
	addInput(id);
	
	var buttons = document.getElementById('buttons' + id);
	if(newFormType == 'checkbox' || newFormType == 'radio') {
		buttons.style.visibility = "visible";
	}
	else {
		buttons.style.visibility = "hidden";
	}
}

/* Add heading */
function addHeading(idForm) {
	
	//heading label
	var headingLabel = document.createElement('label');
	var t = document.createTextNode('Heading');
	headingLabel.setAttribute('for', 'form' + idForm + 'heading');
	headingLabel.setAttribute('id','headingLabel' + idForm);
	headingLabel.appendChild(t);
	
	
	//heading input
	var heading = document.createElement('input'); //input element
	heading.setAttribute('type','text');
	heading.setAttribute('name','heading' + idForm);
	heading.setAttribute('id','heading' + idForm);
	heading.setAttribute('onblur', 'headingChanged(' + idForm + ');');
	
	inputWrapper = document.getElementById('inputWrapper' + idForm);
	var br = document.createElement('br');
	inputWrapper.appendChild(headingLabel);
	inputWrapper.appendChild(heading);
	inputWrapper.appendChild(br);
	
}

/* Add input text field */
function addInput(idForm) {
	index = getIndexFromId(idForm);
	formType = expFormScheme[index].formType;
	idInput = expFormScheme[index].formLabels.length;
	expFormScheme[index].formLabels.push('');
	
	var br = document.createElement('br');
	
	//input image
	var inputLabel = document.createElement('label');
	var t = document.createTextNode(formType);
	inputLabel.setAttribute('for', 'form' + idForm + 'input' + idInput);
	inputLabel.setAttribute('id','form' + idForm + 'inputLabel' + idInput);
	inputLabel.appendChild(t);
	
	//input name
	var inputName = document.createElement('input'); //input element
	inputName.setAttribute('type','text');
	inputName.setAttribute('name','input' + idInput);
	inputName.setAttribute('id','form' + idForm + 'input' + idInput);
	inputName.setAttribute('onblur', 'inputChanged(' + idForm + ', ' + idInput + ');');
	
	inputWrapper = document.getElementById('inputWrapper' + idForm);

	inputWrapper.appendChild(inputLabel);
	inputWrapper.appendChild(inputName);
	inputWrapper.appendChild(br);
	
	if (formType == 'integer' || formType == 'float') {
		unitLabel = document.createElement('label');
		unitLabel.setAttribute('for', 'form' + idForm + 'units' + idInput);
		unitLabel.setAttribute('id','form' + idForm + 'unitsLabel' + idInput);
		var t = document.createTextNode('Jednotky');
		unitLabel.appendChild(t);
		
		units = document.createElement('input');
		units.setAttribute('type','text');
		units.setAttribute('id','units' + idForm);
		units.setAttribute('onblur', 'unitsChanged(' + idForm + ');');
		
		inputWrapper.appendChild(units);
		inputWrapper.appendChild(unitLabel);
	}
}

/* Remove last input text field form HTML */
function removeInput(idForm) {
	index = getIndexFromId(idForm);
	last = expFormScheme[index].formLabels.length - 1;
	if(last != 0) {
		var inputLabel = document.getElementById('form' + idForm + 'inputLabel' + last);
		var input = document.getElementById('form' + idForm + 'input' + last);
		var lastbr = document.getElementById('form' + idForm).getElementsByTagName("br").length - 1;
		var br = document.getElementById('form' + idForm).getElementsByTagName("br")[lastbr];
		inputLabel.parentNode.removeChild(inputLabel);
		input.parentNode.removeChild(input);
		br.parentNode.removeChild(br);
		expFormScheme[index].formLabels.splice(last, 1);
	}
}

/* Handler text change in text field */
function inputChanged(idForm, idInput) {
	field = document.getElementById('form' + idForm + 'input' + idInput);
	index = getIndexFromId(idForm);
	expFormScheme[index].formLabels[idInput] = field.value;
}

/* Handler text change in heading text field */
function headingChanged(idForm) {
	field = document.getElementById('heading' + idForm);
	index = getIndexFromId(idForm);
	expFormScheme[index].name = field.value;
}

/* Handler text change in units text field */
function unitsChanged(idForm) {
	field = document.getElementById('units' + idForm);
	index = getIndexFromId(idForm);
	expFormScheme[index].units = field.value;
}

/* Handler required checkbox */
function changeRequired(idForm) {
	index = getIndexFromId(idForm);
	checkbox = document.getElementById('required' + idForm);
	expFormScheme[index].required = checkbox.checked;
}

function removeButtons() {
	//TODO
}

/* Switch to score editor (next step) */
function toScoreEditor() {
	result = JSON.stringify(expFormScheme);
	var t = document.createTextNode(result);
	var p = document.createElement('p');
	p.appendChild(t);
	var d = document.getElementsByTagName("body")[0];
	d.appendChild(p);
	//alert(result);
}

/* Return index in expFormScheme array from id */
function getIndexFromId(id) {
	for(var i = 0; i < expFormScheme.length; i++) {
		if(expFormScheme[i].id == id) {
			return i;
		}
	}
	return -1;
}

function insertData(){
	result = JSON.stringify(expFormScheme)
	var schemeInput = document.getElementById("experimentScheme");
	schemeInput.value = result;
}
