"""
from flask import Blueprint, request, render_template, redirect, jsonify, abort, url_for
from app import app, db
from app.auth.models import create_data
from app.admin.controllers import show_all_users
from flask_login import login_required
from flask_login import current_user
from app.storage.models import Storage
from werkzeug.routing import Rule
import csv
import io
import json

storage = Blueprint('storage', __name__, template_folder='templates', url_prefix='/app')


"""