from app import db
from werkzeug.datastructures import FileStorage
from sqlalchemy.dialects.postgresql import JSON
import hashlib
import os

class Base(db.Model):
    """Base table"""
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    date_modified = db.Column(db.DateTime, default=db.func.current_timestamp(), onupdate=db.func.current_timestamp())


class Storage(Base, db.Model):
    """Table with information about equipment categories"""
    __tablename__ = 'storage'

    fileName = db.Column(db.String(255), nullable=False)
    extentionName = db.Column(db.String(10), nullable=False)
    controlHash = db.Column(db.String(40), nullable=False)
    fileDescription = db.Column(db.String(256), nullable=False)
    fileTag = db.Column(JSON) # array of string tags
    path = db.Column(db.String(256), nullable=False)
    
    
    def saveFile(f, tag, filePath="", name="", description=""):
        """Save file into server directory with record in DB"""
        realFileName, extention = f.filename.split('.')
        if name == "":
            name = realFileName
            
        fileHash = hashlib.sha1(f.stream.read()).hexdigest()
        f.seek(0)
        duplicity = db.session.query(Storage).filter_by(controlHash=fileHash).count()
        #if duplicity > 0:
            #return -1
            
        if len(extention) > 10:
            return -2
        
        newFile = Storage(
            fileName = name,
            extentionName = extention,
            controlHash = fileHash,
            fileDescription = description,
            fileTag = tag,
            path = filePath
        )
        db.session.add(newFile)
        db.session.commit()
        
        #f.save(os.path.join(app.config['STORAGE_FOLDER'], str(newFile.id) + extention))
        f.save(os.path.join("uploads", str(newFile.id) + "." + extention))
        return newFile.id


    def getFile(fileID):
        """Return one file + file metadata by id"""
        storedFile = db.session.query(Storage).filter_by(id=fileID).first()
        file = None
        with open(os.path.join("uploads", str(storedFile.id) + '.' + storedFile.extentionName), 'rb') as fp:
            file = fp.read()
        #file.save('document-test/test_new.pdf')
        return file, storedFile

    def getFilePath(fileID):
        """Return one file + file metadata by id"""
        storedFile = db.session.query(Storage).filter_by(id=fileID).first()
        file = os.path.join("uploads", str(storedFile.id) + '.' + storedFile.extentionName)

        return file, storedFile

    def deleteFile(fileID):
        """delete file by id"""
        check = db.session.query(Storage).filter_by(controlHash=fileHash).count()
        if check == 1:
            storedFile = db.session.query(Storage).filter_by(id=fileID).first()
            f.remove(os.path.join(app.config['STORAGE_FOLDER'], str(fileID) + extention))
            
            return "ok"
        else:
            return "File does not exist"

    def modifyFile():
        """change content of file"""

        
        
    def getFileInfo(fileID):

        
        return 
