from flask import Blueprint, request, render_template, redirect, jsonify, abort, url_for
from app import app, db #, socketio
from app.measurement.models import Measurement, Measured_person, Measuring_station, Measured_values
from app.auth.models import User
from app.experiments.models import Experiments
from flask_login import login_required
from app.statistics.forms import CodeForm, StatisticsMeasurementForm
#from flask_socketio import SocketIO, emit
import json
from statistics import mean

statistics = Blueprint('statistics', __name__, template_folder='templates', url_prefix='') 

# změnit port
# registrace, co tam bude?
# 

@statistics.route('/show-results', methods=['GET', 'POST'])
@login_required
def showResultsForm():
    form = CodeForm(request.form)
    if form.validate_on_submit():
        #return url_for('showReactionTime')
        redirect('showResults', hexID=form.code.data)
    return render_template("result_form.html", form=form)

@statistics.route('/<string:hexID>')
@login_required
def showResults(hexID):
    """show table with client data"""
    person = Measured_person.getPersonByHexId(hexID)
    if not person:
        abort(404)
    data = Measured_values.getValues(person.id)


    experimentNames = []
    experimentsData = []
    for i, d in enumerate(data):
        names = []
        values = []
        units = []
        ex = Experiments.getExperiment(d.data_from_experiment)
        #print(json.dumps(ex.experimentScheme, ensure_ascii=False))
        scheme = ex.experimentScheme
        jsondata = d.data
        #print(json.dumps(d.data))
        #print(ex.experimentName)
        print(".....")
        print(d.data_from_experiment)
        f = json.dumps(d.data)
        experimentNames.append(ex.experimentName)
        
        for index, v in enumerate(jsondata):
            #if scheme[index]["formType"] == "radio" and v["values"][0]:
            if scheme[int(v["id"])]["formType"] == "radio" and v["values"][0]:
                #print(int(v["values"][0]))
                values.append(scheme[int(v["id"])]["formLabels"][int(v["values"][0])])
                #values.append(scheme[index]["formLabels"][int(v["values"][0])])
            else:
                values.append(v["values"][0])
            #units.append(scheme[index]["units"])
            units.append(scheme[int(v["id"])]["units"])
            #names.append(scheme[index]["name"])
            names.append(scheme[int(v["id"])]["name"])
            print("=======")
            print(int(v["id"]))
            #print(scheme[int(v["id"])]["name"])

        experimentsData.append(zip(names, values, units))
        #break
    #return render_template("results.html", person_id=hexID, data=zip(names, values, units))
    return render_template("results.html", person_id=hexID, data=zip(experimentNames, experimentsData))

@statistics.route('/all')
@login_required
def showAllResults():
    data = Measured_values.getAllValues()
    j = data[6].data
    names = []
    values = []
    units = []
    experimentNames = []
    for i, d in enumerate(data):
        ex = Experiments.getExperiment(d.data_from_experiment)
        print(json.dumps(ex.experimentScheme))
        scheme = ex.experimentScheme
        jsondata = d.data
        print(ex.experimentName)
        f = json.dumps(d.data)
        experimentNames.append(ex.experimentName)
        if i is 6:
            print(jsondata[0]["values"])
            for index, v in enumerate(jsondata):
                
                values.append(v["values"][0])
                units.append(scheme[index]["units"])
                names.append(scheme[index]["name"])
    return render_template("allresults.html", data=zip(names, values, units))

@statistics.route('/reaction-time2')
@login_required
def showReactionTime2():
    """Shows page with charts"""
    menHands = []
    menHandIDs = []
    womenHands = []
    womenHandIDs = []
    menLegs = []
    menLegIDs = []
    womenLegs = []
    womenLegIDs = []
    
    #expHands = Experiments.getExperimentByName("Reakční doba horních končetin")[0]         ###
    #expLegs = Experiments.getExperimentByName("Reakční doba horních končetin")         ###
    #expReg = Experiments.getExperimentByName("Registrace")         ###
    
    expHands = 5
    expLegs = 6
    expReg = 1
    
    valHands = Measured_values.getValuesByExperiment(expHands)
    valLegs = Measured_values.getValuesByExperiment(expLegs)

    aaa = 0
    bbb = "dvgr"
    
    for v in valHands:
        reg = Measured_values.getRegistration(v.measured_person_id, expReg)
        hashID = Measured_person.getPersonById(reg.measured_person_id)
        if v.data[0]["values"][0]:
            #if pohlavi zena nebo muz           ###
            if reg.data[0]["values"][0] == "0":
                womenHandIDs.append(hashID.personHash)
                womenHands.append(int(v.data[0]["values"][0]))
            else:
                menHandIDs.append(hashID.personHash)
                menHands.append(int(v.data[0]["values"][0]))
            
    for v in valLegs:
        reg = Measured_values.getRegistration(v.measured_person_id, expReg)
        hashID = Measured_person.getPersonById(reg.measured_person_id)
        if v.data[0]["values"][0]:
            if reg.data[0]["values"][0] == "0":
                womenLegIDs.append(hashID.personHash)
                womenLegs.append(int(v.data[0]["values"][0]))
            else:
                menLegIDs.append(hashID.personHash)
                menLegs.append(int(v.data[0]["values"][0]))
        
    
    menHands, menHandIDs = (list(t) for t in zip(*sorted(zip(menHands, menHandIDs))))###
    womenHands, womenHandIDs = (list(t) for t in zip(*sorted(zip(womenHands, womenHandIDs))))###
    
    menLegs, menLegIDs = (list(t) for t in zip(*sorted(zip(menLegs, menLegIDs))))###
    womenLegs, womenLegIDs = (list(t) for t in zip(*sorted(zip(womenLegs, womenLegIDs))))###
    
    
    womenHandsSort = sorted(womenHands)
    menHandsSort = sorted(menHands)
    womenLegsSort = sorted(womenLegs)
    menLegsSort = sorted(menLegs)
    
    num = range(1, 6)
    
    
    menHandsZip = zip(num, menHandIDs[:5], menHandsSort[:5])
    womenHandsZip = zip(num, womenHandIDs[:5], womenHandsSort[:5])
    menLegsZip = zip(num, menLegIDs[:5], menLegsSort[:5])
    womenLegsZip = zip(num, womenLegIDs[:5], womenLegsSort[:5])
    
    avgHandMen = sum(menHands)/float(len(menHands))
    avgHandWomen = sum(womenHands)/float(len(womenHands))
    avgLegMen = sum(menLegs)/float(len(menLegs))
    avgLegWomen = sum(womenLegs)/float(len(womenLegs))
    
    return render_template("reaction_time2.html", menHandsZip=menHandsZip, menLegsZip=menLegsZip, womenHandsZip=womenHandsZip, womenLegsZip=womenLegsZip, avgHandMen=avgHandMen, avgHandWomen=avgHandWomen, avgLegWomen=avgLegWomen, avgLegMen=avgLegMen, menCount=len(menLegIDs), womenCount=len(womenLegIDs))
    

@statistics.route('/summary-statistics', methods=['GET', 'POST'])
@login_required
def showSummaryStatistics():
    """Reports and summary statistics"""
    
    statisticsForm = StatisticsMeasurementForm(request.form)
    if statisticsForm.data and statisticsForm.validate_on_submit():
    #provizorni presmerovani
        if statisticsForm.hands.data:
            return redirect(url_for('statistics.showReactionTime', measurement=statisticsForm.measurement.data))
        elif statisticsForm.legs.data:
            return redirect(url_for('statistics.showReactionTime', measurement=statisticsForm.measurement.data))
        
    return render_template("summary_statistics.html", form=statisticsForm)

@statistics.route('/reaction-time/<int:measurement>', methods=['GET', 'POST'])
@login_required
def showReactionTime(measurement):
    """Shows page with charts"""
    
    #measurement = Measurement.getMeasurementByName("reakce")
    #measurementID = measurement.id
    measurementID = measurement
    valHands = Measured_values.getValuesForHandsReactions(measurementID)
    valLegs = Measured_values.getValuesForLegsReactions(measurementID)
    expReg = Experiments.getExperimentByName("Registrace")
    handsReactions = []
    handsPersonIDs = []
    handsSex = []
    handsAge = []
    legsReactions = []
    legsPersonIDs = []
    legsSex = []
    legsAge = []
    
    handsDecadeSum = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    legsDecadeSum = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    handsDecadeCount = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    legsDecadeCount = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    
    for val in valHands:
        for reaction in val.data:
            if reaction["id"] == 0:
                handsReactions.append(reaction["values"][0])
                person = Measured_person.getPersonById(val.measured_person_id)
                handsPersonIDs.append(person.personHash)
                reg = Measured_values.getRegistration(val.measured_person_id, expReg.id)
                for r in reg.data:
                    if r["id"] == 0:
                        handsSex.append(r["values"][0])
                    if r["id"] == 1:
                        handsAge.append(r["values"][0])
                        decade = int(r["values"][0] / 10)
                        if decade > 9:
                            decade = 9
                        handsDecadeSum[decade] += reaction["values"][0]
                        handsDecadeCount[decade] += 1
                break
                
    for val in valLegs:
        for reaction in val.data:
            if reaction["id"] == 0:
                legsReactions.append(reaction["values"][0])
                person = Measured_person.getPersonById(val.measured_person_id)
                legsPersonIDs.append(person.personHash)
                reg = Measured_values.getRegistration(val.measured_person_id, expReg.id)
                for r in reg.data:
                    if r["id"] == 0:
                        legsSex.append(r["values"][0])
                    if r["id"] == 1:
                        legsAge.append(r["values"][0])
                        decade = int(r["values"][0] / 10)
                        if decade > 9:
                            decade = 9
                        legsDecadeSum[decade] += reaction["values"][0]
                        legsDecadeCount[decade] += 1
                break
    
    for i, count in enumerate(handsDecadeCount):
        if count == 0:
            continue
        handsDecadeSum[i] = int(round(handsDecadeSum[i] / count))
        
    for i, count in enumerate(legsDecadeCount):
        if count == 0:
            continue
        legsDecadeSum[i] = int(round(legsDecadeSum[i] / count))
    
    #return json.dumps(handsDecadeSum)
    
    handsMean = 0
    legsMean = 0
    handsZip = []
    legsZip = []
    if handsReactions:
        handsMean = int(round(mean(handsReactions)))
        handsSorted, handsIDsSorted, handsSexSorted = (list(t) for t in zip(*sorted(zip(handsReactions, handsPersonIDs, handsSex))))
        handsZip = zip(handsSorted[:10], handsIDsSorted[:10], handsSexSorted[:10])
    if legsReactions:
        legsMean = int(round(mean(legsReactions)))
        legsSorted, legsIDsSorted, legsSexSorted = (list(t) for t in zip(*sorted(zip(legsReactions, legsPersonIDs, legsSex))))
        legsZip = zip(legsSorted[:10], legsIDsSorted[:10], legsSexSorted[:10])
    
    
    # histograms
    step = 50 # ms
    start = 275 #ms
    stop = 1125 #ms
    steps = 17
    histLabels = list(range(300, 1101, step))
    histHands = [0] * steps
    histLegs = [0] * steps
    
    for item in handsReactions:
        if item >= start and item <= stop:
            index = int((item - start) / step)
            histHands[index] += 1
    
    for item in legsReactions:
        if item >= start and item <= stop:
            index = int((item - start) / step)
            histLegs[index] += 1
            
    return render_template("reaction_time.html", handsMean=handsMean, legsMean=legsMean, handsZip=handsZip, legsZip=legsZip, histLabels=json.dumps(histLabels), histHands=json.dumps(histHands), histLegs=json.dumps(histLegs), handsDecades=handsDecadeSum, legsDecades=legsDecadeSum)
