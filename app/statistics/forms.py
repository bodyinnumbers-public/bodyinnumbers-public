from flask_wtf import Form
from wtforms import TextField, IntegerField, SubmitField

class CodeForm(Form):
    """form for code""" 
    code = TextField('Person code', render_kw={"placeholder": "Your Code..."})


class StatisticsMeasurementForm(Form):
    """form for measurement select"""
    measurement = IntegerField('Measurement ID:')
    hands = SubmitField(label='handsReaction')
    legs = SubmitField(label='legsReaction')
    brain = SubmitField(label='brain')
    lungs = SubmitField(label='lungs')
    fitness = SubmitField(label='fitness')
    heart = SubmitField(label='heart')
    #submit = SubmitField('Submit »')
