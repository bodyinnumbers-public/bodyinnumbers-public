from flask import Blueprint, request, render_template, flash, g, session, redirect, url_for
from flask_security import Security, SQLAlchemyUserDatastore, UserMixin, RoleMixin, login_required
projects = Blueprint('projects', __name__,)


def projects():
	"""show all projects to admin"""
	
def experiment():
	"""show all experiments to admin"""
	
def measuring():
	"""show all measuring to admin"""
	
def projectContent():
	"""show all measuring in one project to user"""
	
def projectInfo():
	"""show info and ability to change name and metadata"""
	
def newProject():
	"""create new project"""
	
def newMeasuring():
	"""create new measuring"""
	
def newExperiment():
	"""create new experiment"""
