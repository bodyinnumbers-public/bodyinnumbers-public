from flask_wtf import Form
from wtforms import StringField, SubmitField, SelectField, TextAreaField, HiddenField  # Import Form elements such as StringField (optional)
from wtforms.validators import DataRequired, length  # Import Form validators


class NewExperimentForm(Form):
    """Form for adding new experiment"""
    experimentName = StringField('Název experimentu', [DataRequired(), length(max=64)])
    measuringEquipment = SelectField('Přístroj', coerce=int)
    description = TextAreaField('Popis', [length(max=256)])
    experimentScheme = StringField('Scheme') #HiddenField('Scheme')
    save = SubmitField('Save')
