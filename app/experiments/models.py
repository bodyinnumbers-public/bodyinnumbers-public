from app import db
from app.equipment.models import MeasuringEquipment
#from app.measurement.models import Measurement
from sqlalchemy.dialects.postgresql import JSON
from flask import json


class Base(db.Model):
    """Base table"""
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    date_modified = db.Column(db.DateTime, default=db.func.current_timestamp(), onupdate=db.func.current_timestamp())


class Experiments(Base, db.Model):
    """Table for storing info of experiments"""
    __tablename__ = 'experiments'

    experimentName = db.Column(db.String(64), nullable=True)
    measuringEquipment = db.Column(db.Integer, db.ForeignKey(MeasuringEquipment.id))
    description = db.Column(db.String(256), nullable=True)
    experimentScheme = db.Column(JSON)
    scoreEvaluationScheme = db.Column(JSON)
    graphicEditor = db.Column(JSON)
    measuring_station = db.relationship('Measuring_station', backref="experiments")
    measured_values = db.relationship('Measured_values', backref="experiments")
    
    @property
    def json(self):
        return to_json(self, self.__class__)

    def getExperiment(expID):
        """return experiment by id"""
        return db.session.query(Experiments).filter_by(id = expID).first()
        
    def getExperimentByName(expName):
        """"""
        return db.session.query(Experiments).filter_by(experimentName = expName).first()

    def getAllExperiments():
        return db.session.query(Experiments)
        


def to_json(inst, cls):
    """
    Jsonify the sql alchemy query result.
    """
    convert = dict()
    # add your coversions for things like datetime's 
    # and what-not that aren't serializable.
    d = dict()
    for c in cls.__table__.columns:
        v = getattr(inst, c.name)
        if c.type in convert.keys() and v is not None:
            try:
                d[c.name] = convert[c.type](v)
            except:
                d[c.name] = "Error:  Failed to covert using ", str(convert[c.type])
        elif v is None:
            d[c.name] = str()
        else:
            d[c.name] = v
    return json.dumps(d)
