from flask import Blueprint, request, render_template
from app import db
from app.experiments.models import Experiments, MeasuringEquipment
from app.experiments.forms import NewExperimentForm
from flask_login import login_required
from flask import json
from app.experiments.models import Experiments
from sqlalchemy.ext.hybrid import hybrid_property
from app import db

experiments = Blueprint('experiments', __name__, template_folder='templates', url_prefix='/app')


@experiments.route('/experiments', methods=['GET', 'POST'])
@login_required
def get_experiments():
    """Adding experiments"""

    form_add = NewExperimentForm(request.form)
    form_add.measuringEquipment.choices = [(g.id, g.deviceName) for g in MeasuringEquipment.query.order_by('id')]
    #form_add.measuringEquipment.choices = [(1, "jedna"), (2, "dva")]
    
    data = db.session.query(Experiments).order_by(Experiments.date_created.desc())
    eqp = db.session.query(MeasuringEquipment).order_by(MeasuringEquipment.id)
    
    
    if form_add.save.data and form_add.validate_on_submit():
        print(form_add.experimentScheme.data)
        jsobject = json.loads(form_add.experimentScheme.data)
        meas_ex_inst = Experiments(
            experimentName = form_add.experimentName.data,
            measuringEquipment = form_add.measuringEquipment.data,
            description = form_add.description.data,
            experimentScheme = jsobject
            # created_by_user_id = 1
            # db.session.query(User).filter_by(id = currnet_user.get_id())
        )
        db.session.add(meas_ex_inst)
        db.session.commit()
        
    #return data[0]

    return render_template("experiments.html", title='Experimenty', data=data, eqp=eqp, form_add=form_add)

@experiments.route('/esave')
@login_required
def esave():
    js = '{"firstName":"John", "lastName":"Doe"}'
    jsobject = json.loads(js)
    exp = Experiments(
        experimentName = 'pokus',
        description = 'popis',
        experimentScheme = jsobject
    )
    db.session.add(exp)
    db.session.commit()
    return 'ok'
    

@experiments.route('/elook')
@login_required
def elook():
    data = db.session.query(Experiments)
    return json.dumps(data[0].experimentScheme)



@experiments.route('/get-questions')
@login_required
def getQuestions():
    """install question experiment"""
    
    d = db.session.query(Experiments)
    return json.dumps(d[0].experimentScheme)
