## AUTH MODULE

##	roles and permissions in system:
##		ordinery user
##		experimentator
##		manager
##		admin

from flask_sqlalchemy import SQLAlchemy
from flask import Blueprint, request, render_template, flash, g, session, redirect, url_for
# from werkzeug import check_password_hash, generate_password_hash # Import password / encryption helper tools
from flask_bcrypt import Bcrypt, check_password_hash, generate_password_hash
from flask_login import login_user, logout_user, login_required, current_user
from app import db  # Import the database object from the main app module
from app.auth.forms import LoginForm, RegistrationForm  # Import module forms
from app.auth.models import User, Role
from flask_bcrypt import Bcrypt, check_password_hash, generate_password_hash
from app import app, bcrypt

# Define the blueprint: 'auth', set its url prefix: app.url/auth
auth = Blueprint('auth', __name__, template_folder='templates', url_prefix='/auth')

@auth.route('/new', methods=['GET', 'POST'])
def makeDODUsers():
    """smazat"""
    User.makeNewUsers()
    return "ok"


# Set the route and accepted methods
@auth.route('/login/', methods=['GET', 'POST'])
def login():
    """Form + validation + login users"""
    form = LoginForm(request.form)
    if form.validate_on_submit():
        user = db.session.query(User).filter_by(email=form.email.data).first()
        if user:
            if bcrypt.check_password_hash(user.password, form.password.data):
                login_user(user)
                flash('Přihlášení proběhlo úspěšně')
                # next = request.args.get('next')
                # if not next_is_valid(next):
                #	return abort(400)
                # return redirect(next or url_for('index'))
                return redirect(url_for('measurement.project'))

    return render_template('login.html', form=form)


@auth.route('/registration/', methods=['GET', 'POST'])
def registration():
    """registration of new users"""
    form = RegistrationForm(request.form)

    if form.validate_on_submit():
        user = User(
            firstName=form.firstName.data,
            lastName=form.lastName.data,
            email=form.email.data,
            password=bcrypt.generate_password_hash(form.password.data),
            status=1
        )
        role = db.session.query(Role).filter_by(name="admin").first()
        user.roles.append(role)
        db.session.add(user)
        db.session.commit()

        return redirect(url_for('general.index'))

    return render_template('registration.html', form=form)


@auth.route('/complete-registration/<int:identificator>')
def complete_registration():
    """Confirmation of registration - link from mail"""

@auth.route('/logout/', methods=['GET', 'POST'])
@login_required
def logout():
    logout_user()
    #flash("Uživatel byl odhlášen")
    return redirect(url_for('general.index'))


def login_required(role="ANY"):
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):

            if not current_user.is_authenticated():
                return current_app.login_manager.unauthorized()
            urole = current_app.login_manager.reload_user().get_urole()
            if ((urole != role) and (role != "ANY")):
                return current_app.login_manager.unauthorized()
            return fn(*args, **kwargs)

        return decorated_view

    return wrapper



@auth.route('/tmp', methods=['GET', 'POST'])
def createDotaznik():
    """"""
    User.createFormular()
    return "ok"
