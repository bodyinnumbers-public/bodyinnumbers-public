from flask_wtf import Form, RecaptchaField # Import Form and RecaptchaField (optional)
from wtforms import TextField, PasswordField, RadioField # Import Form elements such as TextField and BooleanField (optional)
from wtforms.widgets import CheckboxInput
from wtforms.validators import Required, Email, EqualTo # Import Form validators
#from wtfrecaptcha.fields import RecaptchaField


class LoginForm(Form):
	"""Form for login"""
	#email = TextField('Email Address', [Email(), Required(message='Forgot your email address?')])
	#password = PasswordField('Password', [Required(message='Must provide a password. ;-)')])
	email = TextField('Email Address')
	password = PasswordField('Password')

class RegistrationForm(Form):
	"""Form for registration"""
	firstName = TextField('First name', [Required()])
	lastName = TextField('Last name', [Required()])
	email = TextField('Email Address', [Email(), Required(message='Forgot your email address?')])
	password = PasswordField('New Password', [Required(), EqualTo('confirm', message='Passwords must match')])
	confirm  = PasswordField('Repeat Password')
	#accountType = RadioField('Account type', choices=[('experimentator','Experimentator'),('project_manager','Project manager')])
	#group = TextField('Group')
	#licence = CheckboxInput()
	#captcha = RecaptchaField()
	
	
	#			->	přiřazení ke group (experimentator)
	#			->	zaplacení poplatku (project_manager)
