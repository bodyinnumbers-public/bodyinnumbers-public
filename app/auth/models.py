from sqlalchemy.ext.hybrid import hybrid_property
from flask_bcrypt import Bcrypt, check_password_hash, generate_password_hash
from app import app, bcrypt, login_manager
from datetime import datetime
from app import db
# from app.measurement.models import Measurement
from flask_login import LoginManager, UserMixin
from hashlib import md5
from app.experiments.models import Experiments
from flask import json

# from werkzeug import check_password_hash, generate_password_hash # Import password / encryption helper tools


class Base(db.Model):
    """Base class for other database tables. It solves id of table"""
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    date_modified = db.Column(db.DateTime, default=db.func.current_timestamp(), onupdate=db.func.current_timestamp())


# Define models
roles_users = db.Table('roles_users',
                       db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
                       db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))


class Role(Base, db.Model):
    """Roles in system"""
    __tablename__ = 'role'
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

    def __init__(self, name, description):
        """New role"""
        self.name = name
        self.description = description


class User(Base, db.Model, UserMixin):
    """Table of users"""
    __tablename__ = 'user'

    firstName = db.Column(db.String(128), nullable=False)
    lastName = db.Column(db.String(128), nullable=False)
    email = db.Column(db.String(128), nullable=False, unique=True)
    password = db.Column(db.String(192), nullable=False)
    registered_on = db.Column(db.DateTime, nullable=False)
    roles = db.relationship('Role', secondary=roles_users, backref=db.backref('users', lazy='dynamic'))  # permission
    status = db.Column(db.SmallInteger, nullable=False)  # info about confirmation and ban
    token = db.Column(db.String(64))
    tokenExpiration = db.Column(db.DateTime)
    measuring_station = db.relationship('Measuring_station', backref="user")
    measured_values = db.relationship('Measured_values', backref="user")

    @hybrid_property
    def name(self):
        return '{0} {1}'.format(self.firstName, self.lastName)

    @name.setter
    def name(self, value):
        self.firstName, self.lastName = value.split(' ', 1)

    @name.expression
    def name(cls):
        return db.func.concat(cls.firstName, ' ', cls.lastName)

    # measure = db.relationship('Measurement', backref="user", cascade="all, delete-orphan" , lazy='dynamic')

    # New instance instantiation procedure
    def __init__(self, firstName, lastName, email, password, status=0):
        """New user"""
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.password = password
        self.registered_on = datetime.now()
        self.status = status
        self.token = ''
        self.secretExpiration = datetime.now()

    def getUser(user_email):
        return db.session.query(User).filter_by(email=user_email).first()

    def saveToken(user_id, new_token, new_expiration):
        """save generated string for restful mobile services token"""
        
        user = db.session.query(User).filter_by(id=user_id).first()
        user.tokenExpiration = new_expiration
        user.token = new_token
        db.session.commit()

    def getSecret(self, user_id):
        user = db.session.query(User).filter_by(id=user_id).first()
        return user.secret, user.secretExpiration

    def get_id(self):
        return self.id

    def get_status(self):
        return self.status

    def activate_user(self):
        self.status = 1

    def get_email(self):
        return self.email

    def get_password(self):
        return self.password

    def is_active(self):
        return self.status == 1

    def get_urole(self):
        return 'admin'
        
    #################################
    def makeNewUsers():
        admin_role = db.session.query(Role).filter_by(name="admin").first()
        user = db.session.query(User).filter_by(email="registrace@zcu.cz").first()
        if not user:
            passwd = '0000'
            passwd.encode('utf-8')
            pwd = bcrypt.generate_password_hash(passwd).decode('utf8')
            # passwd.encode('utf-8')
            user = User(
                firstName="Reakce",
                lastName="",
                email="registrace@zcu.cz",
                status=1,
                password=pwd
            )
            db.session.add(user)
            db.session.commit()
            
            
        user = db.session.query(User).filter_by(email="formular@zcu.cz").first()
        if not user:
            passwd = '0000'
            passwd.encode('utf-8')
            pwd = bcrypt.generate_password_hash(passwd).decode('utf8')
            # passwd.encode('utf-8')
            user = User(
                firstName="Formulář",
                lastName="",
                email="formular@zcu.cz",
                status=1,
                password=pwd
            )
            db.session.add(user)
            db.session.commit()
            
            
        user = db.session.query(User).filter_by(email="srdce@zcu.cz").first()
        if not user:
            passwd = '0000'
            passwd.encode('utf-8')
            pwd = bcrypt.generate_password_hash(passwd).decode('utf8')
            # passwd.encode('utf-8')
            user = User(
                firstName="Srdce",
                lastName="",
                email="srdce@zcu.cz",
                status=1,
                password=pwd
            )
            db.session.add(user)
            db.session.commit()
            
        user = db.session.query(User).filter_by(email="fitness@zcu.cz").first()
        if not user:
            passwd = '0000'
            passwd.encode('utf-8')
            pwd = bcrypt.generate_password_hash(passwd).decode('utf8')
            # passwd.encode('utf-8')
            user = User(
                firstName="Fitness",
                lastName="",
                email="fitness@zcu.cz",
                status=1,
                password=pwd
            )
            db.session.add(user)
            db.session.commit()
            
            
            user = db.session.query(User).filter_by(email="barvocit@zcu.cz").first()
        if not user:
            passwd = '0000'
            passwd.encode('utf-8')
            pwd = bcrypt.generate_password_hash(passwd).decode('utf8')
            # passwd.encode('utf-8')
            user = User(
                firstName="Barvocit",
                lastName="",
                email="barvocit@zcu.cz",
                status=1,
                password=pwd
            )
            db.session.add(user)
            db.session.commit()
            
            
        user = db.session.query(User).filter_by(email="spirometrie@zcu.cz").first()
        if not user:
            passwd = '0000'
            passwd.encode('utf-8')
            pwd = bcrypt.generate_password_hash(passwd).decode('utf8')
            # passwd.encode('utf-8')
            user = User(
                firstName="Spirometrie",
                lastName="",
                email="spirometrie@zcu.cz",
                status=1,
                password=pwd
            )
            db.session.add(user)
            db.session.commit()
    
    
    #################################
    
    
    def createFormular():
        """"""
        formulText = '[{"id":0,"name":"Cítíte se fyzicky a psychicky zdráv/a?","formType":"radio","required":true,"formLabels":["Ano","Jen fyzicky","Jen psychicky","Ne"],"units":""},{"id":1,"name":"Jakou fyzickou aktivitu upřednostňujete?","formType":"radio","required":true,"formLabels":["Např. běhání, jogging, tanec, cyklistika, plavání apod. (aerobní sporty)","Např. posilování, bosu, pilates, box apod. (anaerobní sporty)","Nesportuji vůbec"],"units":""},{"id":2,"name":"Jak často provozujete fyzickou aktivitu?","formType":"radio","required":true,"formLabels":["Profesionálně (pravidelně)","Rekreačně (občas)","Neprovozuji fyzickou aktivitu vůbec"],"units":""},{"id":3,"name":"Dodržujete pravidelný stravovací režim?","formType":"radio","required":true,"formLabels":["Ano, jím 5 jídel denně","Ano, jím jen 3 jídla denně","Ne, jím jen, když mám hlad a čas","Ne, jím jen, když si na to vzpomenu"],"units":""},{"id":4,"name":"Dodržujete pravidelný pitný režim?","formType":"radio","required":true,"formLabels":["Ano, vypiji do 1,5 l čisté vody denně","Ano, vypiji nad 1,5 l čisté vody denně","Do denního příjmu tekutin zařazuji hlavně čaj, kávu, limonádu, alkohol apod."],"units":""},{"id":5,"name":"Jak často pijete nápoje s obsahem kofeinu? (např. káva, čaj, guarana, maté, kolové a energetické nápoje apod.)","formType":"radio","required":true,"formLabels":["Jednou denně","Víckrát, než jednou denně","Nepravidelně","Nápoje s kofeinem nepiji vůbec"],"units":""},{"id":6,"name":"Nejčastěji se stravuji:","formType":"radio","required":true,"formLabels":["Racionálně pestrou stravu s hodně ovoce a zeleniny (jím vše a vyváženě)","Fastfoody (bagety, pizzy, hamburgery apod.)","Ve stravě se omezuji/držím dietu"],"units":""},{"id":7,"name":"Jak často jíte nějakou sladkost? (př. bonbon, čokoláda, sušenky, slazené jogurty, kobliha, apod.). Předpokládejte, že jíte více než jeden kus sladkosti (např. balení, jedna tabulka, …).","formType":"radio","required":true,"formLabels":["Nejím sladkosti vůbec","Jednu a více denně","Jednou týdně","Jednou měsíčně"],"units":""},{"id":8,"name":"Užíváte pravidelně nějaké léky (u žen antikoncepce) či nějaké doplňky stravy?","formType":"radio","required":true,"formLabels":["Ano, obojí","Ano, pouze léky (u žen antikoncepce)","Ano, pouze doplňky stravy","Neužívám ani jedno"],"units":""},{"id":9,"name":"Trápí vás nějaké chronické onemocnění?","formType":"radio","required":true,"formLabels":["Ano","Ne"],"units":""},{"id":10,"name":"Chodíte k lékaři na pravidelné preventivní prohlídky? Pravidelné návštěvy lékaře se myslí (např. půlroční prohlídky, měsíční, …)","formType":"radio","required":true,"formLabels":["Ano","Ne"],"units":""},{"id":11,"name":"Kouříte?","formType":"radio","required":true,"formLabels":["Nekouřím vůbec","Kouřím příležitostně","Denně vykouřím 1-5 cigaret","Denně vykouřím 6-10 cigaret","Denně vykouřím více než 11 cigaret"],"units":""},{"id":12,"name":"Jak často konzumujete alkohol?","formType":"radio","required":true,"formLabels":["Nekonzumuji, jsem abstinent","Jen příležitostně","1x do měsíce","Každý týden","Každý den"],"units":""},{"id":13,"name":"Máte ve své blízkosti min. 1 přítele, o kterém víte, že vás kdykoliv podrží, pomůže a že se na něj můžete spolehnout?","formType":"radio","required":true,"formLabels":["Ano","Myslím si, že ano","Ne"],"units":""},{"id":14,"name":"Aktuálně zažívám:","formType":"radio","required":true,"formLabels":["Období bez stresu","Nízký stupeň stresu","Střední stupeň stresu","Vysoký stupeň stresu"],"units":""},{"id":15,"name":"Nejčastěji relaxuji:","formType":"radio","required":true,"formLabels":["Aktivně (procházka, sport, práce na zahrádce, sex apod.)","Pasivně (koukání na TV, čtení, spaní, pletení, hraní PC her apod.)","Nerelaxuji vůbec"],"units":""},{"id":16,"name":"Sex provozuji: (nepovinná otázka)","formType":"radio","required":true,"formLabels":["Každý den","Pravidelně min. 1 týdně","Pravidelně min. 1 měsíčně","Nepravidelně","Neprovozuji vůbec"],"units":""},{"id":17,"name":"Denně spím:","formType":"radio","required":true,"formLabels":["Méně než 6h","6-8 hodin","8-10 hodin","Více jak 10 hodin"],"units":""},{"id":18,"name":"Měsíčně vydělávám tolik financí, že","formType":"radio","required":true,"formLabels":["Na své finance nemusím vůbec myslet (je mi to jedno)","Myslím si, že mám na všechno, co potřebuji","Když kupuji věcí běžné spotřeby, občas se nad financemi zamyslím","Trápí mě, že nemám dostatek peněz"],"units":""}]'
        formular = Experiments(
            experimentName = "Dotazník",
            experimentScheme = json.loads(formulText),
            description = "Dotazník pro měřené osoby"
        )
        db.session.add(formular)
        
        admin_role = db.session.query(Role).filter_by(name="admin").first()
        passwd = '0000'
        passwd.encode('utf-8')
        pwd = bcrypt.generate_password_hash(passwd).decode('utf8')
        user = User(
            firstName="Reakční",
            lastName="Doba",
            email="reakce@zcu.cz",
            status=1,
            password=pwd
        )
        user.roles.append(admin_role)
        db.session.add(user)
        
        db.session.commit()
    

    

    def avatar(self, size):
        """Function for getting prototype of avatar"""
        return 'http://www.gravatar.com/avatar/%s?d=mm&s=%d' % (md5(self.email.encode('utf-8')).hexdigest(), size)


db.session.commit()
db.create_all()
db.session.commit()


# Create a user to test with
@app.before_first_request
def create_data():
    """add new user with admin permission"""

    db.create_all()

    admin_role = db.session.query(Role).filter_by(name="admin").first()
    if not admin_role:
        admin_role = Role(
            name="admin",
            description="admin permission to manage the system"
        )
        db.session.add(admin_role)
        db.session.commit()

    experimentator_role = db.session.query(Role).filter_by(name="experimentator").first()
    if not experimentator_role:
        experimentator_role = Role(
            name="experimentator",
            description="experimentator permission"
        )
        db.session.add(experimentator_role)
        db.session.commit()

    user = db.session.query(User).filter_by(email="admin@mail.com").first()
    if not user:
        passwd = 'admin'
        passwd.encode('utf-8')
        pwd = bcrypt.generate_password_hash(passwd).decode('utf8')
        # passwd.encode('utf-8')
        user = User(
            firstName="Admin",
            lastName="Admin",
            email="admin@mail.com",
            status=1,
            password=pwd
        )
        user.roles.append(admin_role)
        db.session.add(user)
        db.session.commit()
        
        
        reg = '[{"id":0,"name":"Pohlaví","formType":"radio","required":false,"formLabels":["Muž","Žena"],"units":""},{"id":1,"name":"Věk","formType":"integer","required":false,"formLabels":[""],"units":""},{"id":2,"name":"Pravák / Levák","formType":"radio","required":false,"formLabels":["Pravák","Levák"],"units":""},{"id":3,"name":"Podpis","formType":"checkbox","required":false,"formLabels":[""],"units":""},{"id":4,"name":"ID dotazníku","formType":"integer","required":false,"formLabels":[""],"units":""}]'
        registrationExp = Experiments(
            experimentName = "Registrace",
            experimentScheme = json.loads(reg),
            description = "Registation"
        )
        
        heightweight = '[{"id":0,"name":"Výška","formType":"integer","required":false,"formLabels":[""],"units":"cm"},{"id":1,"name":"Váha","formType":"float","required":false,"formLabels":[""],"units":"kg"},{"id":2,"name":"BMI","formType":"float","required":false,"formLabels":[""],"units":""},{"id":3,"name":"Svalová hmota","formType":"float","required":true,"formLabels":[""],"units":"%"},{"id":4,"name":"Voda","formType":"float","required":true,"formLabels":[""],"units":"%"},{"id":5,"name":"Tuk","formType":"float","required":true,"formLabels":[""],"units":"%"}]'
        heightWeightExp = Experiments(
            experimentName = "Výška a váha",
            experimentScheme = json.loads(heightweight),
            description = "Výška, váha, BMI, svalová hmota, voda, tuk"
        )
        
        questions = '[{"id":0,"name":"Sportujete pravidelně?","formType":"radio","required":false,"formLabels":["Ano","Ne"],"units":""},{"id":1,"name":"Pokud ne. Chtěl byste sportovat?","formType":"radio","required":false,"formLabels":["Ano","Ne"],"units":""},{"id":2,"name":"Pokud ano. Máte přátele se kterými byste mohl sportovat?","formType":"radio","required":false,"formLabels":["Ano","Ne"],"units":""},{"id":3,"name":"Stravujete se pravidelně?","formType":"radio","required":false,"formLabels":["Ano","Ne"],"units":""},{"id":4,"name":"Dodržujete pitný režim? (2-3 litry tekutin denně)","formType":"radio","required":false,"formLabels":["Ano","Ne"],"units":""},{"id":5,"name":"Jíte zdravě? např. drůbež, ryby, ovoce, zelenina, pitný režim (voda)","formType":"radio","required":false,"formLabels":["Ano","Ne"],"units":""},{"id":6,"name":"Užíváte nějaké doplňky stravy? např. vitamíny, activia, doplňky na klouby a kosti","formType":"radio","required":false,"formLabels":["Ano","Ne"],"units":""},{"id":7,"name":"Jste kuřák?","formType":"radio","required":false,"formLabels":["Ano","Ne","Občasný"],"units":""},{"id":8,"name":"Pokud ano. Kolik cigaret vykouříte?","formType":"radio","required":false,"formLabels":["20 a více cigaret denně","Do 20 cigaret denně","Do 10 cigaret denně","Do 10 cigaret týdně","Do 10 cigaret měsíčně"],"units":""},{"id":9,"name":"Jak často konzumujete alkoholické nápoje? ","formType":"radio","required":false,"formLabels":["Nekonzumuji","Příležitostně","Jednou za týden","Vícekrát za týden","Každý den"],"units":""},{"id":10,"name":"Podstupujete pravidelné lékařské prohlídky?","formType":"radio","required":false,"formLabels":["Ano","Ne"],"units":""},{"id":11,"name":"Máte přítelkyni/přítele či manžela/manželku?","formType":"radio","required":false,"formLabels":["Ano","Ne"],"units":""},{"id":12,"name":"Dopřáváte si správnou relaxaci a odpočinek? např. masáže, wellness, dostatečný spánek (min. 6h)","formType":"radio","required":false,"formLabels":["Ano","Ne"],"units":""}]'
        questionsExp = Experiments(
            experimentName = "Dotazník",
            experimentScheme = json.loads(questions),
            description = "dotazník"
        )
        
        iq = '[{"id":0,"name":"IQ","formType":"integer","required":false,"formLabels":[""],"units":""}]'
        iqExp = Experiments(
            experimentName = "IQ",
            experimentScheme = json.loads(iq),
            description = "hodnota IQ"
        )


        db.session.add(registrationExp)
        db.session.add(heightWeightExp)
        db.session.add(questionsExp)
        db.session.add(iqExp)
        
        
        react1 = '[{"id":0,"name":"Reakční doba horních končetin","formType":"integer","required":false,"formLabels":[""],"units":"ms"},{"id":1,"name":"Zameškáno horní končetiny","formType":"integer","required":false,"formLabels":[""],"units":""},{"id":2,"name":"Chyba horní končetiny","formType":"integer","required":false,"formLabels":[""],"units":""}]'
        reactions1 = Experiments(
            experimentName = "Reakční doba horních končetin",
            experimentScheme = json.loads(react1),
            description = "Reakční doba horních končetin"
        )
        
        react2 = '[{"id":0,"name":"Reakční doba dolní končetiny","formType":"integer","required":false,"formLabels":[""],"units":"ms"},{"id":1,"name":"Směrodatná odchylka dolní končetiny","formType":"integer","required":false,"formLabels":[""],"units":"ms"},{"id":2,"name":"Nejlepší čas dolní končetiny","formType":"integer","required":false,"formLabels":[""],"units":"ms"},{"id":3,"name":"Nejhorší čas dolní končetiny","formType":"integer","required":false,"formLabels":[""],"units":"ms"}]'
        reactions2 = Experiments(
            experimentName = "Reakční doba dolních končetin",
            experimentScheme = json.loads(react2),
            description = "Reakční doba dolních končetin"
        )
        
        col = '[{"id":0,"name":"Barvocit","formType":"checkbox","required":false,"formLabels":["Obr. 1","Obr. 2","Obr. 3","Obr. 4","Obr. 5","Obr. 6","Obr. 7","Obr. 8"],"units":""}]'
        colors = Experiments(
            experimentName = "Barvocit",
            experimentScheme = json.loads(col),
            description = "Barvocit"
        )
        
        spiro = '[{"id":0,"name":"Vitální kapacita plic","formType":"float","required":false,"formLabels":[""],"units":"l"},{"id":1,"name":"Vydechnutý objem za první s.","formType":"float","required":false,"formLabels":[""],"units":"l"},{"id":2,"name":"Vrcholový výdechový průtok","formType":"float","required":false,"formLabels":[""],"units":"l/s"}]'
        spirometry = Experiments(
            experimentName = "Spirometrie",
            experimentScheme = json.loads(spiro),
            description = "Spirometrie"
        )
        
        press = '[{"id":0,"name":"Systolický tlak","formType":"integer","required":false,"formLabels":[""],"units":"mmHg"},{"id":1,"name":"Diastolický tlak","formType":"integer","required":false,"formLabels":[""],"units":"mmHg"},{"id":2,"name":"Puls","formType":"integer","required":false,"formLabels":[""],"units":"puls/min"}]'
        pressure = Experiments(
            experimentName = "Krevní tlak",
            experimentScheme = json.loads(press),
            description = "Krevní tlak a puls"
        )
        
        gluc = '[{"id":0,"name":"Glukóza","formType":"float","required":false,"formLabels":[""],"units":"mmol/l"}]'
        glucose = Experiments(
            experimentName = "Glykemie",
            experimentScheme = json.loads(gluc),
            description = "Měření glukózy v krvi"
        )
        
        flex = '[{"id":0,"name":"Pružnost","formType":"integer","required":false,"formLabels":[""],"units":"cm"}]'
        flexibility = Experiments(
            experimentName = "Pružnost",
            experimentScheme = json.loads(flex),
            description = "Měření pružnosti páteře"
        )
        
        
        db.session.add(reactions1)
        db.session.add(reactions2)
        db.session.add(colors)
        db.session.add(spirometry)
        db.session.add(pressure)
        db.session.add(glucose)
        db.session.add(flexibility)
        
        db.session.commit()

@login_manager.user_loader
def load_user(user_id):
    # return User.get(int(id))
    return db.session.query(User).filter_by(id=user_id).first()




