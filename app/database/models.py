from flask_sqlalchemy import SQLAlchemy
from app import app
from app import db

class Base(db.Model):
	"""Base class for other database tables. It solves id of table"""
	__abstract__  = True

	id = db.Column(db.Integer, primary_key=True)
	date_created = db.Column(db.DateTime,  default=db.func.current_timestamp())
	date_modified = db.Column(db.DateTime,  default=db.func.current_timestamp(), onupdate=db.func.current_timestamp())

 
