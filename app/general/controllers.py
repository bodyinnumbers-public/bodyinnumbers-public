from app import app, bcrypt
from app import db
from flask import Blueprint, request, render_template, flash, g, session, redirect, url_for
from flask_wtf import form
from flask_login import login_user, logout_user, login_required, current_user
from app.auth.models import User,Role, roles_users
from app.measurement.models import Measured_person
from app.general.forms import ChangePassword, ChangeAvatar
#from werkzeug import security
from flask_bcrypt import Bcrypt, check_password_hash, generate_password_hash


general = Blueprint('general', __name__, template_folder='templates')

@general.route('/')
def to_index():
    return redirect('index')
    

@general.route('/index')
def index():
    """Main page"""
    personNumber = Measured_person.countPersons()
    return render_template("home.html", personNumber=personNumber)

@general.route('/profile', methods=['GET', 'POST'])
@login_required
def profile():
    """User profile - show permissions, can add photo, his statistics, password change"""

    roles = ""
    index = 0
    for r in current_user.roles:
        if index != 0:
            roles += ", "
        roles += r.name
        ++index

    min_lenght = 8
    form_change = ChangePassword(request.form)
    form_avatar = ChangeAvatar(request.form)

    if form_change.change.data and form_change.validate_on_submit():
        if bcrypt.check_password_hash(current_user.password, form_change.oldPassword.data):
            if form_change.newPassword.data == form_change.newPasswordRepeat.data:
                if len(form_change.newPassword.data) >= min_lenght:
                    user_inst = db.session.query(User).filter_by(id=current_user.id).first()
                    passwd = form_change.newPassword.data
                    passwd.encode('utf-8')
                    pwd = bcrypt.generate_password_hash(passwd).decode('utf8')
                    user_inst.password = pwd
                    db.session.flush()
                    db.session.commit()
                    flash("Heslo bylo změněno!")
                else:
                    flash("Nové heslo je příliš krátké. Musí mít minimálně {} znaků.".format(min_lenght))
            else:
                flash("Hesla se neshodují")
        else:
            flash("Vaše aktuální heslo bylo zadáno špatně!")

    """if form_avatar.validate_on_submit():
        filename = secure_filename(form_avatar.file.data.filename)
        form_avatar.file.data.save('uploads/' + filename)
        flash("Profilový obrazek byl změněn")"""


    return render_template("profile.html", current_user = current_user, roles = roles, form_change = form_change, form_avatar= form_avatar)
