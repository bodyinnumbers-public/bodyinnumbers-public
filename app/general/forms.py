from flask_wtf import Form
from wtforms import SubmitField, StringField, PasswordField, FileField# Import Form elements such as TextField and BooleanField (optional)
from wtforms.validators import DataRequired, length # Import Form validators

class ChangePassword(Form):
    """Form for change password"""
    oldPassword = PasswordField('Staré heslo', [ length(max=64)])
    newPassword = PasswordField('Nové heslo', [length(max=64)])
    newPasswordRepeat = PasswordField('Nové heslo pro kontrolu', [length(max=64)])
    change = SubmitField('Změnit')


class ChangeAvatar(Form):
    """Form for change avatar"""
    file = FileField('Vložte soubor')
    upload = SubmitField('Nahrát')