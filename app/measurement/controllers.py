from flask import Flask, Blueprint, request, render_template, url_for, redirect, jsonify, send_from_directory, after_this_request
from app import app, db
from app.measurement.models import Measurement, Measured_person, Measuring_station, Measured_values
from app.auth.models import User
from app.experiments.models import Experiments
from app.measurement.forms import NewMeasurementForm, EditMeasurementForm, NewMeasuringStation, CodeGenerator
from sqlalchemy import desc
from flask_login import login_required, current_user
from os import urandom, path, remove
import os
import binascii
from flask_weasyprint import HTML, render_pdf
import pyqrcode
import json
import xlsxwriter
import statistics
import shutil
from flask_wtf import Form
from wtforms import StringField, SubmitField, BooleanField  # Import Form elements such as TextField and BooleanField (optional)
from wtforms.validators import DataRequired, length  # Import Form validators
from wtforms.fields.html5 import DateTimeLocalField, DateTimeField
from werkzeug.utils import secure_filename
import flask_excel as excel
import datetime
from datetime import timedelta

from app.qrgenerator import controllers

measurement = Blueprint('measurement', __name__, template_folder='templates', url_prefix='/app')

@measurement.route('/measurement/go')
@login_required
def go_measure():
    """Page shows measure forms for data insertion"""
    measureToday = Measurement.allMeasurementToday()
    return render_template("go_measure.html", data=measureToday)


@measurement.route('/measurement/new-data/<int:measurement_id>', methods=['GET', 'POST'])
@login_required
def new_data(measurement_id):
    """Page shows measure forms for data insertion"""
    experimentIDs = Measuring_station.getExperimentsId(measurement_id, current_user.id)
    data = []
    names = []
    experiment_ids = []
    
    #dynamic forms
    class DynamicForm(Form): pass
    
    setattr(DynamicForm, "person_id", StringField("ID", [DataRequired()]))
    for index, ex in enumerate(experimentIDs):
        experiment_ids.append(ex.experiments_id)
        exp = Experiments.getExperiment(ex.experiments_id)
        setattr(DynamicForm, "field"+str(index), StringField("name"))
        names.append(exp.experimentName)
        data.append(exp.experimentScheme)
    setattr(DynamicForm, "submit", SubmitField("Submit"))
    
    form = DynamicForm(request.form)
    
    if form.data and form.validate_on_submit():
        personID = ""
        for index, field in enumerate(form):
            #nacteni id hash z formulare
            if index is 0:
                personID = field.data
            elif index > 0:
                
                if field.name == "csrf_token" or field.name == "submit":
                    continue
                exp = Experiments.getExperiment(experimentIDs[index-1].experiments_id)
                
                if exp.experimentName == 'Registrace' or exp.experimentName == 'registrace' or exp.experimentName == 'Registration' or exp.experimentName == 'registrace':
                    Measured_person.registerPerson(current_user.id, personID, measurement_id, experimentIDs[index-1].experiments_id, field.data)
                else:
                    #return str(field.data)
                    Measured_person.saveData(current_user.id, personID, measurement_id, experimentIDs[index-1].experiments_id, field.data)
                    
    form.person_id.data = ""
    return render_template("new_data.html", data=data, names=names, form=form, measurementID = measurement_id, experiment_ids = experiment_ids)

@measurement.route('/project')
@login_required
def project():
    """Show info about project."""

    return render_template("project.html")


@measurement.route('/planning/show', methods=['GET', 'POST'])
@login_required
def measure_planning():
    """Show all future (planned) measuring"""

    form_add = NewMeasurementForm(request.form)
    form_edit = EditMeasurementForm(request.form)
    form_task = NewMeasuringStation(request.form)
    data = db.session.query(Measurement).order_by(Measurement.begin.desc())

    if form_edit.edit.data and form_edit.validate_on_submit():
        measure_inst = db.session.query(Measurement).filter_by(id=form_edit.measure_id.data).first()
        if measure_inst:
            measure_inst.begin = form_edit.begin_e.data
            measure_inst.end = form_edit.end_e.data
            measure_inst.town = form_edit.town_e.data
            measure_inst.street = form_edit.street_e.data
            measure_inst.number = form_edit.number_e.data
            measure_inst.institution = form_edit.institution_e.data
            db.session.flush()
            db.session.commit()

    return render_template("planning.html", data=data, form_add=form_add, form_edit=form_edit, form_task=form_task)


@measurement.route('/name_autocomplete', methods=['GET'])
def name_autocomplete():
    """Finds proper name of experiments matching with input and sends it as a list"""
    search = request.args.get('qn')
    if search == " ":
        query = db.session.query(Experiments.experimentName)
    else:
        query = db.session.query(Experiments.experimentName).filter(
            Experiments.experimentName.ilike('%' + str(search) + '%'))

    results = [mv[0] for mv in query.all()]
    return jsonify(matching_results_n=results)


@measurement.route('/executor_autocomplete', methods=['GET'])
def executor_autocomplete():
    """Finds proper name of experiment executor matching with input and sends it as a list"""
    search = request.args.get('qe')
    if search == " ":
        query = db.session.query(User.name)
    else:
        query = db.session.query(User.name).filter(User.name.ilike('%' + str(search) + '%'))

    results = [mv[0] for mv in query.all()]
    return jsonify(matching_results_e=results)


@measurement.route('/show_experiments')
@login_required
def show_experiments():
    """Finds names of experiment executors and sends it as a list"""

    query = db.session.query(Experiments.experimentName).order_by(Experiments.id)
    results = [mv[0] for mv in query.all()]

    return jsonify(result=results)


@measurement.route('/show_executors')
@login_required
def show_executors():
    """Finds names of experiment executors and sends it as a list"""

    query = db.session.query(User.name).order_by(User.id)
    results = [mv[0] for mv in query.all()]

    return jsonify(result=results)


@measurement.route('/planning/detail/show/<int:id_detail>')
@login_required
def show_measurement_detail(id_detail):
    """Show pop up detail TODO: !!!"""


@measurement.route('/planning_add', methods=['GET', 'POST'])
@login_required
def planning_add():
    """Add new measuring to plan TODO: kontrola prijatych dat, vyresit vytvoreni konstruktoru"""
    form_add = NewMeasurementForm(request.form)
    form_task = NewMeasuringStation(request.form)
    result = -1

    if form_add.add and form_add.validate_on_submit():
        measure_inst = Measurement(
            institution=form_add.institution.data,
            begin=form_add.begin.data,
            end=form_add.end.data,
            town=form_add.town.data,
            street=form_add.street.data,
            number=form_add.number.data

            # created_by_user_id = 1
            # db.session.query(User).filter_by(id = currnet_user.get_id())
        )
        db.session.add(measure_inst)
        db.session.commit()
        result = measure_inst.id

    elif form_task.validate_on_submit():
        experiment_id = db.session.query(Experiments.id).filter_by(experimentName=form_task.experimentName.data).first()
        if experiment_id:
            query = db.session.query(User.id).filter(User.name.ilike('%' + str(form_task.experimentExecutor.data) + '%'))
            executor_id = [mv[0] for mv in query.all()].pop()
            if executor_id:
                task_inst = Measuring_station(
                    measurement_id=form_task.measurement_id.data,
                    user_id=executor_id,
                    experiments_id=experiment_id.id
                )
                db.session.add(task_inst)
                db.session.commit()
                result = 1

    return jsonify(result=result)


@measurement.route('/planning/edit/<int:id_edit>')
@login_required
def edit_planned_measurement(id_edit):
    """Show pop up window for edit measurement TODO: !!! ajax:("""
    return redirect('app/planning/show')


@measurement.route('/planning/remove/<int:id_remove>')
@login_required
def remove_planned_measurement(id_remove):
    """Remove selected measurement"""

    Measurement.query.filter_by(id=id_remove).delete()
    db.session.commit()
    return redirect('app/planning/show')


@measurement.route('/measured/show')
@login_required
def measured():
    """Show table with measuring"""
    data = db.session.query(Measurement).order_by(desc(Measurement.date_created))
    return render_template("measured.html", data=data)


@measurement.route('/measurement/edit/<int:id_edit>')
@login_required
def edit_measurement(id_edit):
    """Show pop up window for edit measurement TODO: !!! ajax:("""
    return redirect('app/planning/show')


@measurement.route('/measurement/remove/<int:id_remove>')
@login_required
def remove_measurement(id_remove):
    """Remove selected measurement"""

    Measurement.query.filter_by(id=id_remove).delete()
    db.session.commit()
    return redirect('app/planning/show')



# uz se nevyuziva
@measurement.route('/measured/measured_persons')
@login_required
def measured_persons():
    """Show measured persons"""
    names = []
    data = db.session.query(Measured_person)
    for d in data:
        """vyber id mereni a vypis jeho nazev"""
        m = Measured_values.getOneValue(d.id) # measurement_id
        measurement_id = m.measurement_id
        print(measurement_id)
        names.append(Measurement.getMeasurementById(measurement_id))
        
    return render_template("measured_persons.html", data=zip(data, names))
    

@measurement.route('/measured/pokus/<string:hexID>')
@login_required
def showPokus(hexID):
    ''''''
    person = Measured_person.getPersonByHexId(hexID)
    data = Measured_values.getValues(person.id)
    #return str(len(person)) + " : " + str(len(data))
    return str(data[3].data)

@measurement.route('/measured/person_detail/<string:hexID>')
@login_required
def showPersonDetail(hexID):
    """show table with client data"""
    person = Measured_person.getPersonByHexId(hexID)
    if not person:
        abort(404)
    data = Measured_values.getValues(person.id)
    
    experimentNames = []
    experimentsData = []
    #return json.dumps(data[1].data)
    for i, d in enumerate(data):
        names = []
        values = []
        units = []
        types = []
        ex = Experiments.getExperiment(d.data_from_experiment)
        #return json.dumps(ex.experimentScheme)
        scheme = ex.experimentScheme
        jsondata = d.data
        #print(ex.experimentName)
        f = json.dumps(d.data)
        experimentNames.append(ex.experimentName)
        for index, v in enumerate(jsondata):
            #if scheme[index]["formType"] == "radio" and v["values"][0]:
            
            types.append(scheme[int(v["id"])]["formType"])
            if scheme[int(v["id"])]["formType"] == "radio":
                if len(v["values"]) > 0:
                    #return str(v)
                    #print(int(v["values"][0]))
                    a = int(v["id"])
                    b = int(v["values"][0])
                    #return str(scheme[a]["formLabels"])
                    values.append(scheme[a]["formLabels"][b])
                    #values.append(scheme[index]["formLabels"][int(v["values"][0])])
                else:
                    values.append("-")
            else:
                if len(v["values"]) > 0:
                    #return str(v["values"][0])
                    print("tady hledame chybu")
                    print(v["values"])
                    values.append(v["values"][0])
                else:
                    values.append("-")
            #units.append(scheme[index]["units"])
            
            units.append(scheme[int(v["id"])]["units"])
            #names.append(scheme[index]["name"])
            names.append(scheme[int(v["id"])]["name"])
            print(int(v["id"]))
            #print(scheme[int(v["id"])]["name"])
            
        experimentsData.append(zip(names, values, units, types))

    #return render_template("results.html", person_id=hexID, data=zip(names, values, units))
    return render_template("person_detail.html", person_id=hexID, data=zip(experimentNames, experimentsData))


@measurement.route('/forms')
@login_required
def show_forms():
    """Show forms for data input"""
    data = '[{"id":0,"name":"Systolický tlak","formType":"integer","required":true,"formLabels":["test1"],"units":"mm/hg"},{"id":1,"name":"Diastolický tlak","formType":"integer","required":true,"formLabels":["test2"],"units":"mm/hg"}]'
    return render_template("show_forms.html", data=data)
    

@measurement.route('/export', methods=['GET', 'POST'])
@login_required
def exportData():
    """export data from db"""
    return render_template("export.html")
    
    
@measurement.route('/import', methods=['GET', 'POST'])
@login_required
def importData():
    """import excel file with data Mensa"""
    if request.method == 'POST':
        #experimentIDs = Measuring_station.getExperimentsId(request.form["measurement_id"], current_user.id)
        #measurement_id = request.form["measurement_id"]
        measurement = Measurement.getMeasurementByName("Mensa")
        measurement_id = measurement.id
        for i, person in enumerate(request.get_array(field_name='file')):
            
            if i is not 0:
                #print(i)
                #print(person[0])
                
                number = binascii.hexlify(urandom(8)).decode("utf-8")
                personID = str(number)
                print(personID)
                
                experimentID = 1 # 0= registrace
                data = [] # pole jednoho experimentu
                for j in range(0, 5):
                    d = {}
                    val = []
                    val.append(person[j])
                    d["id"] = j
                    d["values"] = val
                    data.append(d)
                    
                datastring = json.dumps(data)
                Measured_person.registerPerson(current_user.id, personID, measurement_id, experimentID, datastring)
                #print(json.dumps(data))
                
                
                experimentID = 2 # vyska vaha
                data = [] # pole jednoho experimentu
                for j in range(5, 7):
                    d = {}
                    val = []
                    val.append(person[j])
                    d["id"] = j-5
                    d["values"] = val
                    data.append(d)
                #print(json.dumps(data))
                datastring = json.dumps(data)
                Measured_person.saveData(current_user.id, personID, measurement_id, experimentID, datastring)
                    
                experimentID = 3 # dotaznik
                data = [] # pole jednoho experimentu
                for j in range(7, 20):
                    d = {}
                    val = []
                    val.append(person[j])
                    d["id"] = j
                    d["values"] = val
                    data.append(d)
                #print(json.dumps(data))
                datastring = json.dumps(data)
                Measured_person.saveData(current_user.id, personID, measurement_id, experimentID, datastring)
                    
                experimentID = 4 # IQ
                data = [] # pole jednoho experimentu
                for j in range(20, 21):
                    d = {}
                    val = []
                    val.append(person[j])
                    d["id"] = j
                    d["values"] = val
                    data.append(d)
                #print(json.dumps(data))
                datastring = json.dumps(data)
                Measured_person.saveData(current_user.id, personID, measurement_id, experimentID, datastring)
            
            #exp = Experiments.getExperiment(experimentID)

            
            
            #myDict = {'id': 5,'value': 3}
            #data.append()
            
        #return request.form["measurement_id"]
        return "Done!"
    return render_template("import.html")
    

@measurement.route('/import2', methods=['GET', 'POST'])
@login_required
def importData2():
    """import dotaznik DVT"""
    if request.method == 'POST':
        #experimentIDs = Measuring_station.getExperimentsId(request.form["measurement_id"], current_user.id)
        #measurement_id = request.form["measurement_id"]
        measurement = Measurement.getMeasurementByName("DVT")
        measurement_id = measurement.id
        print("???????????????????????????????")
        print(measurement_id)
        for i, person in enumerate(request.get_array(field_name='file')):
            
            if i is not 0:
                #print(i)
                #print(person[0])
                
                number = binascii.hexlify(urandom(8)).decode("utf-8")
                personID = str(number)
                print(personID)
                
                experimentID = 1 # 0= registrace
                data = [] # pole jednoho experimentu
                for j in range(0, 2):
                    d = {}
                    val = []
                    val.append(person[j])
                    d["id"] = j
                    d["values"] = val
                    data.append(d)
                    
                for j in range(2, 4):
                    d = {}
                    val = []
                    val.append(person[j])
                    d["id"] = j+1
                    d["values"] = val
                    data.append(d)
                    
                datastring = json.dumps(data)
                Measured_person.registerPerson(current_user.id, personID, measurement_id, experimentID, datastring)
                #print(json.dumps(data))
                
                

                    
                experimentID = 3 # dotaznik
                data = [] # pole jednoho experimentu
                for j in range(4, 17):
                    d = {}
                    val = []
                    val.append(person[j])
                    d["id"] = j-4
                    d["values"] = val
                    data.append(d)
                #print(json.dumps(data))
                datastring = json.dumps(data)
                Measured_person.saveData(current_user.id, personID, measurement_id, experimentID, datastring)
                    
        return "Done!"
    return render_template("import2.html")




@measurement.route('/import3', methods=['GET', 'POST'])
@login_required
def importData3():
    """import DVT mereni"""
    if request.method == 'POST':
        #experimentIDs = Measuring_station.getExperimentsId(request.form["measurement_id"], current_user.id)
        #measurement_id = request.form["measurement_id"]
        measurement = Measurement.getMeasurementByName("DVT")
        measurement_id = measurement.id
        print(measurement_id)
        print("===vrm===")
        for i, person in enumerate(request.get_array(field_name='file')):
            
            if i is not 0:
                
                
                person_id = -1
                personID = ''
                values = Measured_values.getValuesByMeasurementExperiment(measurement_id, 1) #   je id registrace
                for v in values:
                    #print(v.measurement_id)
                    for n in v.data:
                        #print(n["id"])
                        if n["id"] is 4:
                            #print(n["values"][0])
                            if int(n["values"][0]) is int(person[0]):
                                print("kvok")
                                print(int(n["values"][0]))
                                person_id = v.measured_person_id
                            
                if person_id is not -1:
                    print("-------------")
                    print(int(person[0]))
                    personID = Measured_person.getPersonById(person_id).personHash
                    print(personID)
                else:
                    print("=======frnk==========")
                    break
                    
                
                
                #number = binascii.hexlify(urandom(8)).decode("utf-8")
                #personID = str(number)
                #print(personID)
                
                experimentID = 5 # reakcni doba ruce
                data = [] # pole jednoho experimentu
                for j in range(1, 4):
                    d = {}
                    val = []
                    val.append(person[j])
                    d["id"] = j-1
                    d["values"] = val
                    data.append(d)
                #print(json.dumps(data))
                datastring = json.dumps(data)
                Measured_person.saveData(current_user.id, personID, measurement_id, experimentID, datastring)
                
                
                
                experimentID = 6 # reakcni doba nohy
                data = [] # pole jednoho experimentu
                for j in range(4, 8):
                    d = {}
                    val = []
                    val.append(person[j])
                    d["id"] = j-4
                    d["values"] = val
                    data.append(d)
                #print(json.dumps(data))
                datastring = json.dumps(data)
                Measured_person.saveData(current_user.id, personID, measurement_id, experimentID, datastring)
                
                
                experimentID = 7 # barvocit
                data = [] # pole jednoho experimentu
                for j in range(8, 9):
                    d = {}
                    val = []
                    val.append(person[j])
                    d["id"] = j-8
                    d["values"] = val
                    data.append(d)
                #print(json.dumps(data))
                datastring = json.dumps(data)
                Measured_person.saveData(current_user.id, personID, measurement_id, experimentID, datastring)
                
                
                experimentID = 8 # spirometrie
                data = [] # pole jednoho experimentu
                for j in range(9, 12):
                    d = {}
                    val = []
                    val.append(person[j])
                    d["id"] = j-9
                    d["values"] = val
                    data.append(d)
                #print(json.dumps(data))
                datastring = json.dumps(data)
                Measured_person.saveData(current_user.id, personID, measurement_id, experimentID, datastring)
                
                experimentID = 9 # tlak
                data = [] # pole jednoho experimentu
                for j in range(12, 15):
                    d = {}
                    val = []
                    val.append(person[j])
                    d["id"] = j-12
                    d["values"] = val
                    data.append(d)
                #print(json.dumps(data))
                datastring = json.dumps(data)
                Measured_person.saveData(current_user.id, personID, measurement_id, experimentID, datastring)
                
                
                experimentID = 10 # glukoza
                data = [] # pole jednoho experimentu
                for j in range(15, 16):
                    d = {}
                    val = []
                    val.append(person[j])
                    d["id"] = j-15
                    d["values"] = val
                    data.append(d)
                #print(json.dumps(data))
                datastring = json.dumps(data)
                Measured_person.saveData(current_user.id, personID, measurement_id, experimentID, datastring)
                
                experimentID = 2 # vyska vaha
                data = [] # pole jednoho experimentu
                for j in range(16, 22):
                    d = {}
                    val = []
                    val.append(person[j])
                    d["id"] = j-16
                    d["values"] = val
                    data.append(d)
                #print(json.dumps(data))
                datastring = json.dumps(data)
                Measured_person.saveData(current_user.id, personID, measurement_id, experimentID, datastring)
                    

                    
                experimentID =11 # pružnost
                data = [] # pole jednoho experimentu
                for j in range(22, 23):
                    d = {}
                    val = []
                    val.append(person[j])
                    d["id"] = j-22
                    d["values"] = val
                    data.append(d)
                #print(json.dumps(data))
                datastring = json.dumps(data)
                Measured_person.saveData(current_user.id, personID, measurement_id, experimentID, datastring)
            
            #exp = Experiments.getExperiment(experimentID)

            
            
            #myDict = {'id': 5,'value': 3}
            #data.append()
            
        #return request.form["measurement_id"]
        return "Done!"
    return render_template("import3.html")




@measurement.route('/measured/projects')
@login_required
def show_all_projects():
    """Shows all projects"""
    data = db.session.query(Measurement).order_by(Measurement.begin.desc())
    return render_template("all_projects.html", data=data)
    


@measurement.route('/measured/project_persons2/<int:measurementID>', methods=['GET', 'POST'])
@login_required
def project_persons2(measurementID):
    """Show measured persons"""
    names = []
    values = Measurement.getUniqueValuesByMeasurement(measurementID)
    newData = []
    for d in values:
        
#        #print(measurement_id)
#        names.append(Measurement.getMeasurementById(measurement_id))
        if measurement_id  == measurementID:
            names.append(Measurement.getMeasurementById(measurement_id))
            newData.append(d)
#        
    return render_template("project_persons.html", data=zip(newData, names))


@measurement.route('/measured/project_persons_old/<int:measurementID>', methods=['GET', 'POST'])
@login_required
def project_persons_old(measurementID):
    """Show measured persons"""
    names = []
    data = db.session.query(Measured_person)
    newData = []
    processInfo = []
    for d in data:
        """vyber id mereni a vypis jeho nazev"""
        m = Measured_values.getOneValue(d.id) # measurement_id
        measurement_id = m.measurement_id
#        #print(measurement_id)
#        names.append(Measurement.getMeasurementById(measurement_id))
        if measurement_id  == measurementID:
            names.append(Measurement.getMeasurementById(measurement_id))
            newData.append(d)
            infoObject = {}
            #infoObject[""]
    return render_template("project_persons.html", data=zip(newData, names))
    
@measurement.route('/measured/project_persons/<int:measurementID>', methods=['GET', 'POST'])
@login_required
def project_persons(measurementID):
    """Show measured persons"""
    
    data = Measured_person.getPersons(measurementID)
    processInfo = []
    processTimes = []
    processPercentage = []
    timeSum = timedelta(0)
    timeCount = 0
    institution = Measurement.getMeasurementById(measurementID).institution
    experimentNumber = Measurement.countExperimentsOfMeasurement(measurementID)
    
    for d in data:
        experimentsDone = Measurement.doneExperiments(measurementID, d.id)
        NumberExpDone = experimentsDone.count()
        
        if NumberExpDone == 1:
            processInfo.append("Registrated")
            processTimes.append(None)
            processPercentage.append(None)
        elif experimentNumber > NumberExpDone and NumberExpDone > 1:
            percentage = int(((float)(NumberExpDone*100))/experimentNumber)
            
            processInfo.append("In progress")
            processTimes.append(None)
            processPercentage.append(str(percentage))
        else:
            diff = experimentsDone[NumberExpDone - 1].date_created - d.date_created
            processInfo.append("Done")
            timeSum += diff
            timeCount += 1
            diff = divmod(diff.days * 86400 + diff.seconds, 60)
            processTimes.append(str(diff[0]) + "min " + str(diff[1]) + "s")
            processPercentage.append(None)
    
    if timeCount > 0:
        avg = int(timeSum.total_seconds() / timeCount)
        avgMin = int(avg / 60)
        avgSec = avg % 60
        avgTime = str(avgMin) + "min " + str(avgSec) + "s"
        #avg = divmod(avgt.days * 86400 + avgt.seconds, 60)
        #return str(avg)
    else:
        avgTime = "-"
    
    return render_template("project_persons.html", data=zip(data, processInfo, processPercentage, processTimes), institution=institution, avgTime = avgTime)

@measurement.route('/measured/export/', methods=['GET','POST'])
@login_required
def export():

    shutil.rmtree('temp', ignore_errors=True)
    dt = datetime.datetime.now()
    name = 'export' + str("%s-%s-%s" % (dt.day, dt.month, dt.year) ) + '.xlsx'

    workbook = xlsxwriter.Workbook(name)
    worksheet = workbook.add_worksheet('proMatlab')

    worksheet.merge_range('B1:D1', 'ZÁKLADNÍ DATA')
    worksheet.merge_range('E1:L1', 'MOZEK A SMYSLY')
    worksheet.merge_range('U1:AA1', 'SRDCE A KREV')
    worksheet.merge_range('AB1:AD1', 'PLÍCE')
    worksheet.merge_range('AE1:AJ1', 'TĚLESNÉ PROPORCE')
    worksheet.merge_range('AL1:BC1', 'DOTAZNÍK')

    worksheet.write('A2', 'ID')
    worksheet.write('B2', 'Skupina')
    worksheet.write('C2', 'Pohlaví')
    worksheet.write('D2', 'Věk')
    worksheet.write('E2', 'Reakční doba horní končetiny [ms]')
    worksheet.write('F2', 'Zameškáno horní končetiny')
    worksheet.write('G2', 'Chyba horní končetiny')
    worksheet.write('H2', 'Reakční doba dolní končetiny [ms]')
    worksheet.write('I2', 'Směrodatná odchylka dolní končetiny')
    worksheet.write('J2', 'Nejlepší čas dolní končetiny [ms]')
    worksheet.write('K2', 'Nejhorší čas dolní končetiny [ms]')
    worksheet.write('L2', 'Barvocit1')
    worksheet.write('M2', 'Barvocit2')
    worksheet.write('N2', 'Barvocit3')
    worksheet.write('O2', 'Barvocit4')
    worksheet.write('P2', 'Barvocit5')
    worksheet.write('Q2', 'Barvocit6')
    worksheet.write('R2', 'Barvocit7')
    worksheet.write('S2', 'Barvocit8')
    worksheet.write('T2', 'Systolikcý tlak [mm / Hg]')
    worksheet.write('U2', 'Diastolikcý tlak [mm / Hg]')
    worksheet.write('V2', 'Puls [puls / min]')
    worksheet.write('W2', 'HR [puls / min]')
    worksheet.write('X2', 'ST segment [mm]')
    worksheet.write('Y2', 'QRS interval [s]')
    worksheet.write('Z2', 'Glukóza [mmol / l]')
    worksheet.write('AA2', 'Jídlo před měřením')
    worksheet.write('AB2', 'Vitální kapacita plic [l]')
    worksheet.write('AC2', 'Vydechnutý objem za první s. [l]')
    worksheet.write('AD2', 'Vrcholový výdechový průtok [l / s]')
    worksheet.write('AE2', 'Výška [cm]')
    worksheet.write('AF2', 'Váha [kg]')
    worksheet.write('AG2', 'BMI')
    worksheet.write('AH2', 'Svalová hmota [%]')
    worksheet.write('AI2', 'Voda [%]')
    worksheet.write('AJ2', 'Tuk [%]')
    worksheet.write('AK2', 'Pružnost [cm]')
    worksheet.write('AL2', 'POCIT RESPONDENTA')
    worksheet.write('AM2', 'SPORT')
    worksheet.write('AN2', 'STRAVA - PRAVIDELNOST')
    worksheet.write('AO2', 'STRAVA')
    worksheet.write('AP2', 'PITNÝ REŽIM')
    worksheet.write('AQ2', 'PŘÍJEM KOFEINU')
    worksheet.write('AR2', 'SLADKOSTI')
    worksheet.write('AS2', 'LÉKY')
    worksheet.write('AT2', 'CHRONICKÁ ONEMOCNĚNÍ')
    worksheet.write('AU2', 'LÉKAŘ')
    worksheet.write('AV2', 'KOUŘENÍ')
    worksheet.write('AW2', 'ALKOHOL')
    worksheet.write('AX2', 'PŘÁTELÉ')
    worksheet.write('AY2', 'STRES')
    worksheet.write('AZ2', 'RELAX')
    worksheet.write('BA2', 'SEX')
    worksheet.write('BB2', 'SPÁNEK')
    worksheet.write('BC2', 'FINANCE')

    """Show measured persons"""
    #22.4.2017 heidtke - multiple experiments
    measurementIDs = request.form.getlist('measurement') 
    startingRow = 3
    for measurementID in measurementIDs:
        #8.4.2017 heidtke
        persons = db.session.query(Measured_values.measured_person_id).filter_by(measurement_id = measurementID).distinct().order_by(Measured_values.measured_person_id).all()
        for i,da in enumerate(persons):
            person = Measured_person.getPersonById(da.measured_person_id)
            pdata = Measured_values.getValues(da.measured_person_id).filter_by(measurement_id = measurementID) #only specific measurement
            worksheet.write('A' + str(startingRow + i), person.personHash)
            #measurementName = db.session.query(Measurement.institution).filter_by(id=measurementID).first()[0]
            worksheet.write('B' + str(startingRow + i), str(measurementID))
            
            for d in pdata:
                #Registrace
                if d.data_from_experiment == 1:
                        worksheet.write('C' + str(startingRow + i),str(d.data[0]["values"][0]))
                        worksheet.write('D' + str(startingRow + i),str(d.data[1]["values"][0]))
                #Výška a váha
                elif d.data_from_experiment == 2:
                        worksheet.write('AE' + str(startingRow + i),str(d.data[0]["values"][0]))
                        worksheet.write('AF' + str(startingRow + i),str(d.data[1]["values"][0]))
                        worksheet.write('AG' + str(startingRow + i),str(d.data[2]["values"][0]))
                        worksheet.write('AH' + str(startingRow + i),str(d.data[3]["values"][0]))
                        worksheet.write('AI' + str(startingRow + i),str(d.data[4]["values"][0]))
                        worksheet.write('AJ' + str(startingRow + i),str(d.data[5]["values"][0]))
                #Dotazník
                elif d.data_from_experiment == 3:
                        worksheet.write('AK' + str(startingRow + i),str(d.data[0]["values"][0]))
                        worksheet.write('AL' + str(startingRow + i),str(d.data[1]["values"][0]))
                        worksheet.write('AM' + str(startingRow + i),str(d.data[2]["values"][0]))
                        worksheet.write('AN' + str(startingRow + i),str(d.data[3]["values"][0]))
                        worksheet.write('AO' + str(startingRow + i),str(d.data[4]["values"][0]))
                        worksheet.write('AP' + str(startingRow + i),str(d.data[5]["values"][0]))
                        worksheet.write('AQ' + str(startingRow + i),str(d.data[6]["values"][0]))
                        worksheet.write('AR' + str(startingRow + i),str(d.data[7]["values"][0]))
                        worksheet.write('AS' + str(startingRow + i),str(d.data[8]["values"][0]))
                        worksheet.write('AT' + str(startingRow + i),str(d.data[9]["values"][0]))
                        worksheet.write('AU' + str(startingRow + i),str(d.data[10]["values"][0]))
                        worksheet.write('AV' + str(startingRow + i),str(d.data[11]["values"][0]))
                        worksheet.write('AW' + str(startingRow + i),str(d.data[12]["values"][0]))
                        worksheet.write('AX' + str(startingRow + i),str(d.data[13]["values"][0]))
                        worksheet.write('AY' + str(startingRow + i),str(d.data[14]["values"][0]))
                        worksheet.write('AZ' + str(startingRow + i),str(d.data[15]["values"][0]))
                        worksheet.write('BA' + str(startingRow + i),str(d.data[16]["values"][0]))
                        worksheet.write('BB' + str(startingRow + i),str(d.data[17]["values"][0]))
                        worksheet.write('BC' + str(startingRow + i),str(d.data[18]["values"][0]))
                #hodnota IQ
                #elif d.data_from_experiment == 4:
                #Reakční doba horních končetin
                elif d.data_from_experiment == 5:
                        worksheet.write('E' + str(startingRow + i),str(d.data[0]["values"][0]))
                        worksheet.write('F' + str(startingRow + i),str(d.data[1]["values"][0]))
                        worksheet.write('G' + str(startingRow + i),str(d.data[2]["values"][0]))
                #Reakční doba dolních končetin
                elif d.data_from_experiment == 6:
                        worksheet.write('H' + str(startingRow + i),str(d.data[0]["values"][0]))
                        worksheet.write('I' + str(startingRow + i),str(d.data[1]["values"][0]))
                        worksheet.write('J' + str(startingRow + i),str(d.data[2]["values"][0]))
                        worksheet.write('K' + str(startingRow + i),str(d.data[3]["values"][0]))
                #Barvocit
                elif d.data_from_experiment == 7:
                        worksheet.write('L' + str(startingRow + i),str(d.data[0]["values"][0][0]))
                        worksheet.write('M' + str(startingRow + i),str(d.data[0]["values"][0][1]))
                        worksheet.write('N' + str(startingRow + i),str(d.data[0]["values"][0][2]))
                        worksheet.write('O' + str(startingRow + i),str(d.data[0]["values"][0][3]))
                        worksheet.write('P' + str(startingRow + i),str(d.data[0]["values"][0][4]))
                        worksheet.write('Q' + str(startingRow + i),str(d.data[0]["values"][0][5]))
                        worksheet.write('R' + str(startingRow + i),str(d.data[0]["values"][0][6]))
                        worksheet.write('S' + str(startingRow + i),str(d.data[0]["values"][0][7]))
                #Spirometrie
                elif d.data_from_experiment == 8:
                        worksheet.write('AB' + str(startingRow + i),str(d.data[0]["values"][0]))
                        worksheet.write('AC' + str(startingRow + i),str(d.data[1]["values"][0]))
                        worksheet.write('AD' + str(startingRow + i),str(d.data[2]["values"][0]))   
                #Krevní tlak
                elif d.data_from_experiment == 9:
                        worksheet.write('T' + str(startingRow + i),str(d.data[0]["values"][0]))
                        worksheet.write('U' + str(startingRow + i),str(d.data[1]["values"][0]))
                        worksheet.write('V' + str(startingRow + i),str(d.data[2]["values"][0]))
                #Glykemie
                #elif d.data_from_experiment == 10:
                #Pružnost
                elif d.data_from_experiment == 11:
                        worksheet.write('AK' + str(startingRow + i),str(d.data[0]["values"][0]))

        startingRow += len(persons)
    workbook.close()

    os.makedirs('temp',exist_ok=True)
    os.rename(os.getcwd() + '\\' + name,os.getcwd() + '\\' + 'temp' + '\\' + name)

    """
    @after_this_request
    def remove_file(response):
        try:
            os.remove(os.getcwd() + '\\' + 'app\\static\\tmp' + '\\' + name)
        except Exception as error:
            app.logger.error("Soubor neexistuje.", error)
        return response
    """


    return send_from_directory(os.getcwd() + '\\' + 'temp', name, as_attachment=True)

#heidtke
@app.route('/measured/ajax_stats')
def ajax_stats():
    measurementID = request.args.get('m', 0, type=int)
    gender = request.args.get('g', 0, type=int)
    print(gender)
    ageRange = request.args.get('a', 0, type=int)
    print(ageRange)

    array = calculateStats(measurementID,gender,ageRange)
    return jsonify(result=array)

def calculatePersonStats(d):

    # pdata = Measured_values.getValues(personID)

    # for d in pdata:
        # Výška a váha
        height = None
        weight = None
        bmi = None
        muscle_mass = None
        water = None
        fat = None
        react_hands = None
        react_hands_missed = None
        react_hands_misstaken = None
        react_legs = None
        worst_react_time_legs = None
        best_react_time_legs = None
        systolic_press = None
        diastolic_press = None
        puls = None
        vital_lung_capacity = None
        exhalation_flow = None
        exhaled_volume = None
        coloring_0 = None
        coloring_1 = None
        coloring_2 = None
        coloring_3 = None
        coloring_4 = None
        coloring_5 = None
        coloring_6 = None
        coloring_7 = None

        if d.data_from_experiment == 2:
            height = d.data[0]["values"][0]  # výška
            weight = d.data[1]["values"][0]  # váha
            bmi = d.data[2]["values"][0]  # BMI
            muscle_mass = d.data[3]["values"][0]  # svalová hmota
            water = d.data[4]["values"][0]  # voda
            fat = d.data[5]["values"][0]  # tuk

        # Reakční doba horních končetin
        elif d.data_from_experiment == 5:
            react_hands = d.data[0]["values"][0]  # Reakční doba horní končetiny
            react_hands_missed = d.data[1]["values"][0]  # Zameškáno horní končetiny
            react_hands_misstaken = d.data[2]["values"][0]  # Chyba horní končetiny

        # Reakční doba dolních končetin
        elif d.data_from_experiment == 6:
            react_legs = (d.data[0]["values"][0])  # Reakční doba dolní končetiny
            # d.data[1]["values"][0]  # Směrodatná odchylka dolní končetiny
            best_react_time_legs = (d.data[2]["values"][0])  # Nejlepší čas dolní končetiny
            worst_react_time_legs = (d.data[3]["values"][0])  # Nejhorší čas dolní končetiny

        # Krevní tlak
        elif d.data_from_experiment == 9:
            systolic_press = (d.data[0]["values"][0])  # Systolikcý tlak
            diastolic_press = (d.data[1]["values"][0])  # Diastolikcý tlak
            puls = (d.data[2]["values"][0])  # Puls

        # Spirometrie
        elif d.data_from_experiment == 8:
            vital_lung_capacity = (d.data[0]["values"][0])  # Vitální kapacita plic
            exhaled_volume = (d.data[1]["values"][0])  # Vydechnutý objem za první s
            exhalation_flow = (d.data[2]["values"][0])  # Vrcholový výdechový průtok

        # Barvocit
        elif d.data_from_experiment == 7:
            coloring_0 = (d.data[0]["values"][0][0])
            coloring_1 = (d.data[0]["values"][0][1])
            coloring_2 = (d.data[0]["values"][0][2])
            coloring_3 = (d.data[0]["values"][0][3])
            coloring_4 = (d.data[0]["values"][0][4])
            coloring_5 = (d.data[0]["values"][0][5])
            coloring_6 = (d.data[0]["values"][0][6])
            coloring_7 = (d.data[0]["values"][0][7])

        array = {"height":height,"weight":weight,"bmi":bmi,"muscle_mass":muscle_mass,"water":water,"fat":fat,"react_hands":react_hands,"react_hands_missed":react_hands_missed,"react_hands_misstaken":react_hands_misstaken,
                 "react_legs":react_legs,"best_react_time_legs":best_react_time_legs,"worst_react_time_legs":worst_react_time_legs,"systolic_press":systolic_press,"diastolic_press":diastolic_press,
                 "puls":puls,"vital_lung_capacity":vital_lung_capacity,"exhaled_volume":exhaled_volume,"exhalation_flow":exhalation_flow,"coloring_0":coloring_0,"coloring_1":coloring_1,"coloring_2":coloring_2,
                 "coloring_3":coloring_3,"coloring_4":coloring_4,"coloring_5":coloring_5,"coloring_6":coloring_6,"coloring_7":coloring_7}
        return array

#heidtke- code from stats()
def calculateStats(measurementID,g,a):
    list_weight = []
    list_height = []
    list_bmi = []
    list_muscle_mass = []
    list_water = []
    list_fat = []
    list_age = []
    list_react_hands = []
    list_react_hands_missed = []
    list_react_hands_misstaken = []
    list_react_legs = []
    list_best_react_time_legs = []
    list_worst_react_time_legs = []
    list_systolic_press = []
    list_diastolic_press = []
    list_puls = []
    list_vital_lung_capacity = []
    list_exhaled_volume = []
    list_exhalation_flow = []
    list_coloring_0 = []
    list_coloring_1 = []
    list_coloring_2 = []
    list_coloring_3 = []
    list_coloring_4 = []
    list_coloring_5 = []
    list_coloring_6 = []
    list_coloring_7 = []

    man = 0
    woman = 0

    age_0_10 = 0
    age_11_20 = 0
    age_21_30 = 0
    age_31_40 = 0
    age_41_50 = 0
    age_51_60 = 0
    age_61_70 = 0
    age_71_80 = 0
    age_81_90 = 0
    age_91_plus = 0

    avg_age = 0

    min_weight = 0
    avg_weight = 0
    max_weight = 0
    q1_weight = 0
    q2_weight = 0
    q3_weight = 0

    min_height = 0
    avg_height = 0
    max_height = 0
    q1_height = 0
    q2_height = 0
    q3_height = 0

    
    min_systolic_press = 0
    avg_systolic_press = 0
    max_systolic_press = 0
    q1_systolic_press = 0
    q2_systolic_press = 0
    q3_systolic_press = 0

    min_diastolic_press = 0
    avg_diastolic_press = 0
    max_diastolic_press = 0
    q1_diastolic_press = 0
    q2_diastolic_press = 0
    q3_diastolic_press = 0

    min_puls = 0
    avg_puls = 0
    max_puls = 0
    q1_puls = 0
    q2_puls = 0
    q3_puls = 0

    min_vital_lung_capacity = 0
    avg_vital_lung_capacity = 0
    max_vital_lung_capacity = 0
    q1_vital_lung_capacity = 0
    q2_vital_lung_capacity = 0
    q3_vital_lung_capacity = 0

    min_exhaled_volume = 0
    avg_exhaled_volume = 0
    max_exhaled_volume = 0
    q1_exhaled_volume = 0
    q2_exhaled_volume = 0
    q3_exhaled_volume = 0

    min_exhalation_flow = 0
    avg_exhalation_flow = 0
    max_exhalation_flow = 0
    q1_exhalation_flow = 0
    q2_exhalation_flow = 0
    q3_exhalation_flow = 0

    min_bmi = 0
    avg_bmi = 0
    max_bmi = 0
    q1_bmi = 0
    q2_bmi = 0
    q3_bmi = 0

    min_muscle_mass = 0
    avg_muscle_mass = 0
    max_muscle_mass = 0
    q1_muscle_mass = 0
    q2_muscle_mass = 0
    q3_muscle_mass = 0

    min_water = 0
    avg_water = 0
    max_water = 0
    q1_water = 0
    q2_water = 0
    q3_water = 0

    min_fat = 0
    avg_fat = 0
    max_fat = 0
    q1_fat = 0
    q2_fat = 0
    q3_fat = 0

    succ_rate_coloring_0 = 0
    succ_rate_coloring_1 = 0
    succ_rate_coloring_2 = 0
    succ_rate_coloring_3 = 0
    succ_rate_coloring_4 = 0
    succ_rate_coloring_5 = 0
    succ_rate_coloring_6 = 0
    succ_rate_coloring_7 = 0

    count_missed_hands = 0
    count_misstaken_hands = 0

    q1_react_time_legs = 0
    q2_react_time_legs = 0
    q3_react_time_legs = 0
    best_react_time_legs = 0
    avg_react_legs = 0
    worst_react_time_legs = 0

    q1_react_time_hands = 0
    q2_react_time_hands = 0
    q3_react_time_hands = 0
    min_r_hands = 0
    max_r_hands = 0
    avg_react_hands = 0

    is_gender = -1
    is_age = -1


    persons = Measured_values.getPersons(measurementID)
    count_persons = Measured_values.getPersonsCount(measurementID)

    for i, da in enumerate(persons):
        pdata = Measured_values.getValues(da.measured_person_id)

        for d in pdata:
            # Registrace
            if d.data_from_experiment == 1:

                value = d.data[0]["values"][0]
                if  value == 0:
                    man = man + 1
                    if g != -1:
                        is_gender = 0
                else:
                    woman = woman + 1
                    if g != -1:
                        is_gender = 1

                value = d.data[1]["values"][0]
                if value != None:
                    list_age.append(value) #age
                    if value >= 0 and value <= 10:
                        age_0_10 = age_0_10 + 1
                        if a != -1:
                            is_age = 0
                    elif value >= 11 and value <= 20:
                        age_11_20 = age_11_20 + 1
                        if a != -1:
                            is_age = 1
                    elif value >= 21 and value <= 30:
                        age_21_30 = age_21_30 + 1
                        if a != -1:
                            is_age = 2
                    elif value >= 31 and value <= 40:
                        age_31_40 = age_31_40 + 1
                        if a != -1:
                            is_age = 3
                    elif value >= 41 and value <= 50:
                        age_41_50 = age_41_50 + 1
                        if a != -1:
                            is_age = 4
                    elif value >= 51 and value <= 60:
                        age_51_60 = age_51_60 + 1
                        if a != -1:
                            is_age = 5
                    elif value >= 61 and value <= 70:
                        age_61_70 = age_61_70 + 1
                        if a != -1:
                            is_age = 6
                    elif value >= 71 and value <= 80:
                        age_71_80 = age_71_80 + 1
                        if a != -1:
                            is_age = 7
                    elif value >= 81 and value <= 90:
                        age_81_90 = age_81_90 + 1
                        if a != -1:
                            is_age = 8
                    else:
                        age_91_plus = age_91_plus + 1
                        if a != -1:
                            is_age = 9


            if (g == -1 and a == -1):
                # Výška a váha
                if d.data_from_experiment == 2:
                    if d.data[0]["values"][0] != None:
                        list_height.append(d.data[0]["values"][0]) # výška
                    if d.data[1]["values"][0] != None:
                        list_weight.append(d.data[1]["values"][0]) # váha
                    if d.data[2]["values"][0] != None:
                        list_bmi.append(d.data[2]["values"][0]) # BMI
                    if d.data[3]["values"][0] != None:
                        list_muscle_mass.append(d.data[3]["values"][0]) # svalová hmota
                    if d.data[4]["values"][0] != None:
                        list_water.append(d.data[4]["values"][0]) # voda
                    if d.data[5]["values"][0] != None:
                        list_fat.append(d.data[5]["values"][0]) # tuk


                # Reakční doba horních končetin
                elif d.data_from_experiment == 5:
                    if d.data[0]["values"][0] != None:
                        list_react_hands.append(d.data[0]["values"][0])  # Reakční doba horní končetiny
                    if d.data[1]["values"][0] != None:
                        list_react_hands_missed.append(d.data[1]["values"][0])  # Zameškáno horní končetiny
                    if d.data[2]["values"][0] != None:
                        list_react_hands_misstaken.append(d.data[2]["values"][0])  # Chyba horní končetiny

                # Reakční doba dolních končetin
                elif d.data_from_experiment == 6:
                    if d.data[0]["values"][0] != None:
                        list_react_legs.append(d.data[0]["values"][0])  # Reakční doba dolní končetiny
                    #d.data[1]["values"][0]  # Směrodatná odchylka dolní končetiny
                    if d.data[2]["values"][0] != None:
                        list_best_react_time_legs.append(d.data[2]["values"][0])  # Nejlepší čas dolní končetiny
                    if d.data[3]["values"][0] != None:
                        list_worst_react_time_legs.append(d.data[3]["values"][0])  # Nejhorší čas dolní končetiny
                # Krevní tlak
                elif d.data_from_experiment == 9:
                    if d.data[0]["values"][0] != None:
                        list_systolic_press.append(d.data[0]["values"][0])  # Systolikcý tlak
                    if d.data[1]["values"][0] != None:
                        list_diastolic_press.append(d.data[1]["values"][0])  # Diastolikcý tlak
                    if d.data[2]["values"][0] != None:
                        list_puls.append(d.data[2]["values"][0])  # Puls
                    # Spirometrie
                elif d.data_from_experiment == 8:
                    if d.data[0]["values"][0] != None:
                        list_vital_lung_capacity.append(d.data[0]["values"][0])  # Vitální kapacita plic
                    if d.data[1]["values"][0] != None:
                        list_exhaled_volume.append(d.data[1]["values"][0])  # Vydechnutý objem za první s
                    if d.data[2]["values"][0] != None:
                        list_exhalation_flow.append(d.data[2]["values"][0])  # Vrcholový výdechový průtok

                # Barvocit
                elif d.data_from_experiment == 7:
                    if d.data[0]["values"][0] != None:
                        list_coloring_0.append(d.data[0]["values"][0][0])
                        list_coloring_1.append(d.data[0]["values"][0][1])
                        list_coloring_2.append(d.data[0]["values"][0][2])
                        list_coloring_3.append(d.data[0]["values"][0][3])
                        list_coloring_4.append(d.data[0]["values"][0][4])
                        list_coloring_5.append(d.data[0]["values"][0][5])
                        list_coloring_6.append(d.data[0]["values"][0][6])
                        list_coloring_7.append(d.data[0]["values"][0][7])

            elif (g == is_gender and a == is_age):
                array_person = calculatePersonStats(d)
                if array_person["height"] != None:
                    list_height.append(array_person["height"])
                if array_person["weight"] != None:
                    list_weight.append(array_person["weight"])
                if array_person["bmi"] != None:
                    list_bmi.append(array_person["bmi"])
                if array_person["muscle_mass"] != None:
                    list_muscle_mass.append(array_person["muscle_mass"])
                if array_person["water"] != None:
                    list_water.append(array_person["water"])
                if array_person["fat"] != None:
                    list_fat.append(array_person["fat"])
                if array_person["react_hands"] != None:
                    list_react_hands.append(array_person["react_hands"])
                if array_person["react_hands_missed"] != None:
                    list_react_hands_missed.append(array_person["react_hands_missed"])
                if array_person["react_hands_misstaken"] != None:
                    list_react_hands_misstaken.append(array_person["react_hands_misstaken"])
                if array_person["react_legs"] != None:
                    list_react_legs.append(array_person["react_legs"])
                if array_person["best_react_time_legs"] != None:
                    list_best_react_time_legs.append(array_person["best_react_time_legs"])
                if array_person["worst_react_time_legs"] != None:
                    list_worst_react_time_legs.append(array_person["worst_react_time_legs"])
                if array_person["systolic_press"] != None:
                    list_systolic_press.append(array_person["systolic_press"])
                if array_person["diastolic_press"] != None:
                    list_diastolic_press.append(array_person["diastolic_press"])
                if array_person["puls"] != None:
                    list_puls.append(array_person["puls"])
                if array_person["vital_lung_capacity"] != None:
                    list_vital_lung_capacity.append(array_person["vital_lung_capacity"])
                if array_person["exhaled_volume"] != None:
                    list_exhaled_volume.append(array_person["exhaled_volume"])
                if array_person["exhalation_flow"] != None:
                    list_exhalation_flow.append(array_person["exhalation_flow"])
                if array_person["coloring_0"] != None:
                    list_coloring_0.append(array_person["coloring_0"])
                if array_person["coloring_1"] != None:
                    list_coloring_1.append(array_person["coloring_1"])
                if array_person["coloring_2"] != None:
                    list_coloring_2.append(array_person["coloring_2"])
                if array_person["coloring_3"] != None:
                    list_coloring_3.append(array_person["coloring_3"])
                if array_person["coloring_4"] != None:
                    list_coloring_4.append(array_person["coloring_4"])
                if array_person["coloring_5"] != None:
                    list_coloring_5.append(array_person["coloring_5"])
                if array_person["coloring_6"] != None:
                    list_coloring_6.append(array_person["coloring_6"])
                if array_person["coloring_7"] != None:
                    list_coloring_7.append(array_person["coloring_7"])


            """
            
            # Glykemie
            elif d.data_from_experiment == 10:
                worksheet.write('S' + str(i + 3), str(d.data[0]["values"][0]))
            # Pružnost
            elif d.data_from_experiment == 11:
                worksheet.write('AD' + str(i + 3), str(d.data[0]["values"][0]))
            """
    try:
        avg_age = round(statistics.mean(list_age))

        min_height = min(list_height)
        avg_height = round(statistics.mean(list_height))
        max_height = max(list_height)
        q1_height = statistics.median_low(list_height)
        q2_height = statistics.median(list_height)
        q3_height = statistics.median_high(list_height)

        min_weight = min(list_weight)
        avg_weight = round(statistics.mean(list_weight))
        max_weight = max(list_weight)
        q1_weight = statistics.median_low(list_weight)
        q2_weight = statistics.median(list_weight)
        q3_weight = statistics.median_high(list_weight)

        min_bmi = min(list_bmi)
        avg_bmi = round(statistics.mean(list_bmi),1)
        max_bmi = max(list_bmi)
        q1_bmi = statistics.median_low(list_bmi)
        q2_bmi = statistics.median(list_bmi)
        q3_bmi = statistics.median_low(list_bmi)

        min_muscle_mass = min(list_muscle_mass)
        avg_muscle_mass = round(statistics.mean(list_muscle_mass),1)
        max_muscle_mass = max(list_muscle_mass)
        q1_muscle_mass = statistics.median_low(list_muscle_mass)
        q2_muscle_mass = statistics.median(list_muscle_mass)
        q3_muscle_mass = statistics.median_low(list_muscle_mass)

        min_water = min(list_water)
        avg_water = round(statistics.mean(list_water),1)
        max_water = max(list_water)
        q1_water = statistics.median_low(list_water)
        q2_water = statistics.median(list_water)
        q3_water = statistics.median_low(list_water)

        min_fat = min(list_fat)
        avg_fat = round(statistics.mean(list_fat),1)
        max_fat = max(list_fat)
        q1_fat = statistics.median_low(list_fat)
        q2_fat = statistics.median(list_fat)
        q3_fat = statistics.median_low(list_fat)

        avg_react_hands = round(statistics.mean(list_react_hands))

        avg_react_legs = round(statistics.mean(list_react_legs))

        min_systolic_press = min(list_systolic_press)
        avg_systolic_press = round(statistics.mean(list_systolic_press))
        max_systolic_press = max(list_systolic_press)
        q1_systolic_press = statistics.median_low(list_systolic_press)
        q2_systolic_press = statistics.median(list_systolic_press)
        q3_systolic_press = statistics.median_low(list_systolic_press)

        min_diastolic_press = min(list_diastolic_press)
        avg_diastolic_press = round(statistics.mean(list_diastolic_press))
        max_diastolic_press = max(list_diastolic_press)
        q1_diastolic_press = statistics.median_low(list_diastolic_press)
        q2_diastolic_press = statistics.median(list_diastolic_press)
        q3_diastolic_press = statistics.median_low(list_diastolic_press)

        min_puls = min(list_puls)
        avg_puls = round(statistics.mean(list_puls))
        max_puls = max(list_puls)
        q1_puls = statistics.median_low(list_puls)
        q2_puls = statistics.median(list_puls)
        q3_puls = statistics.median_low(list_puls)

        min_vital_lung_capacity = min(list_vital_lung_capacity)
        avg_vital_lung_capacity = round(statistics.mean(list_vital_lung_capacity),2)
        max_vital_lung_capacity = max(list_vital_lung_capacity)
        q1_vital_lung_capacity = statistics.median_low(list_vital_lung_capacity)
        q2_vital_lung_capacity = statistics.median(list_vital_lung_capacity)
        q3_vital_lung_capacity = statistics.median_low(list_vital_lung_capacity)

        min_exhaled_volume = min(list_exhaled_volume)
        avg_exhaled_volume = round(statistics.mean(list_exhaled_volume),2)
        max_exhaled_volume = max(list_exhaled_volume)
        q1_exhaled_volume  = statistics.median_low(list_exhaled_volume)
        q2_exhaled_volume  = statistics.median(list_exhaled_volume)
        q3_exhaled_volume  = statistics.median_low(list_exhaled_volume)

        min_exhalation_flow = min(list_exhalation_flow)
        avg_exhalation_flow = round(statistics.mean(list_exhalation_flow),2)
        max_exhalation_flow = max(list_exhalation_flow)
        q1_exhalation_flow  = statistics.median_low(list_exhalation_flow)
        q2_exhalation_flow  = statistics.median(list_exhalation_flow)
        q3_exhalation_flow  = statistics.median_low(list_exhalation_flow)
    except statistics.StatisticsError:
        print('no data')

    except ValueError:
        print('no data')

    pom = 0
    for i in list_react_hands_missed:
        if list_react_hands_missed[pom] > 0:
            count_missed_hands = count_missed_hands + 1
        pom = pom + 1

    pom = 0
    for i in list_react_hands_misstaken:
        if list_react_hands_misstaken[pom] > 0:
            count_misstaken_hands = count_misstaken_hands + 1
        pom = pom + 1
    try:
        succ_rate_coloring_0 = round((sum(list_coloring_0) / len(list_coloring_0)) * 100, 1)
        succ_rate_coloring_1 = round((sum(list_coloring_1) / len(list_coloring_1)) * 100, 1)
        succ_rate_coloring_2 = round((sum(list_coloring_2) / len(list_coloring_2)) * 100, 1)
        succ_rate_coloring_3 = round((sum(list_coloring_3) / len(list_coloring_3)) * 100, 1)
        succ_rate_coloring_4 = round((sum(list_coloring_4) / len(list_coloring_4)) * 100, 1)
        succ_rate_coloring_5 = round((sum(list_coloring_5) / len(list_coloring_5)) * 100, 1)
        succ_rate_coloring_6 = round((sum(list_coloring_6) / len(list_coloring_6)) * 100, 1)
        succ_rate_coloring_7 = round((sum(list_coloring_7) / len(list_coloring_7)) * 100, 1)
    except ZeroDivisionError:
        print("no data")

    try:
        best_react_time_legs = min(list_best_react_time_legs)
        worst_react_time_legs = max(list_worst_react_time_legs)
        q1_react_time_legs = statistics.median_low(list_best_react_time_legs)
        q2_react_time_legs = statistics.median(list_best_react_time_legs)
        q3_react_time_legs = statistics.median_high(list_best_react_time_legs)

        q1_react_time_hands = statistics.median_low(list_react_hands)
        q2_react_time_hands = statistics.median_low(list_react_hands)
        q3_react_time_hands = statistics.median_low(list_react_hands)
        min_r_hands = min(list_react_hands)
        max_r_hands = max(list_react_hands)


    except ValueError:
        print("no data")

    array = {"measurementID":measurementID,"persons_count":count_persons, "man":man, "woman":woman, "avg_age":avg_age,
             "age_decade":{"age_0_10":age_0_10,"age_11_20":age_11_20,"age_21_30":age_21_30,"age_31_40":age_31_40,"age_41_50":age_41_50,"age_51_60":age_51_60,"age_61_70":age_61_70,"age_71_80":age_71_80,"age_81_90":age_81_90,"age_91_plus":age_91_plus}
             , "avg_react_hands":avg_react_hands,"min_r_hands" : min_r_hands, "max_r_hands" : max_r_hands
             , "q1_react_time_hands":q1_react_time_hands, "q2_react_time_hands":q2_react_time_hands, "q3_react_time_hands":q3_react_time_hands
             , "avg_react_legs":avg_react_legs, "count_missed_hands":count_missed_hands, "count_misstaken_hands":count_misstaken_hands
             , "best_react_time_legs":best_react_time_legs
             , "worst_react_time_legs":worst_react_time_legs
             , "q1_react_time_legs":q1_react_time_legs, "q2_react_time_legs":q2_react_time_legs, "q3_react_time_legs":q3_react_time_legs
             , "min_systolic_press":min_systolic_press, "avg_systolic_press":avg_systolic_press, "max_systolic_press":max_systolic_press, "q1_systolic_press":q1_systolic_press, "q2_systolic_press":q2_systolic_press, "q3_systolic_press":q3_systolic_press
             , "min_diastolic_press":min_diastolic_press, "avg_diastolic_press":avg_diastolic_press, "max_diastolic_press":max_diastolic_press, "q1_diastolic_press":q1_diastolic_press, "q2_diastolic_press":q2_diastolic_press, "q3_diastolic_press":q3_diastolic_press
             , "min_puls":min_puls, "avg_puls":avg_puls, "max_puls":max_puls,"q1_puls":q1_puls,"q2_puls":q2_puls,"q3_puls":q3_puls
             , "min_vital_lung_capacity":min_vital_lung_capacity, "avg_vital_lung_capacity":avg_vital_lung_capacity, "max_vital_lung_capacity" : max_vital_lung_capacity, "q1_vital_lung_capacity" : q1_vital_lung_capacity, "q2_vital_lung_capacity" : q2_vital_lung_capacity, "q3_vital_lung_capacity" : q3_vital_lung_capacity
             , "min_exhaled_volume":min_exhaled_volume, "avg_exhaled_volume":avg_exhaled_volume, "max_exhaled_volume":max_exhaled_volume, "q1_exhaled_volume":q1_exhaled_volume, "q2_exhaled_volume":q2_exhaled_volume, "q3_exhaled_volume":q3_exhaled_volume
             , "min_exhalation_flow":min_exhalation_flow, "avg_exhalation_flow":avg_exhalation_flow, "max_exhalation_flow":max_exhalation_flow, "q1_exhalation_flow":q1_exhalation_flow, "q2_exhalation_flow":q2_exhalation_flow, "q3_exhalation_flow":q3_exhalation_flow
             , "min_height":min_height, "avg_height":avg_height, "max_height":max_height, "q1_height":q1_height, "q2_height":q2_height, "q3_height":q3_height
             , "min_weight":min_weight, "avg_weight":avg_weight, "max_weight":max_weight, "q1_weight":q1_weight, "q2_weight":q2_weight, "q3_weight":q3_weight
             , "min_bmi":min_bmi, "avg_bmi":avg_bmi, "max_bmi":max_bmi, "q1_bmi":q1_bmi, "q2_bmi":q2_bmi, "q3_bmi":q3_bmi
             , "min_muscle_mass":min_muscle_mass, "avg_muscle_mass":avg_muscle_mass, "max_muscle_mass":max_muscle_mass, "q1_muscle_mass":q1_muscle_mass, "q2_muscle_mass":q2_muscle_mass, "q3_muscle_mass":q3_muscle_mass
             , "min_water":min_water, "avg_water":avg_water, "max_water":max_water, "q1_water":q1_water, "q2_water":q2_water, "q3_water":q3_water
             , "min_fat":min_fat, "avg_fat":avg_fat, "max_fat":max_fat, "q1_fat":q1_fat, "q2_fat":q2_fat, "q3_fat":q3_fat
             , "succ_rate_coloring_0":succ_rate_coloring_0,"succ_rate_coloring_1":succ_rate_coloring_1,"succ_rate_coloring_2":succ_rate_coloring_2,"succ_rate_coloring_3":succ_rate_coloring_3,"succ_rate_coloring_4":succ_rate_coloring_4,"succ_rate_coloring_5":succ_rate_coloring_5,"succ_rate_coloring_6":succ_rate_coloring_6,"succ_rate_coloring_7":succ_rate_coloring_7,
             "":""}

    return array;
#soukupm3
@measurement.route('/measured/stats/<int:measurementID>')
@login_required
def stats(measurementID,g=-1,a=-1):
   
    array = calculateStats(measurementID,g,a)
    data = json.loads(json.dumps(array))
    return render_template('stats.html', data=data)

@measurement.route('/jsonn')
@login_required
def show_me():
    """Show"""
    experiments = Experiments.getAllExperiments()
    #return experimentIDs[0].id# json.dumps(experimentIDs)
    iq = '[{"id":0,"name":"IQ","formType":"integer","required":false,"formLabels":[""],"units":""}]'
    for u in experiments:
        print(json.dumps(u.experimentScheme).encode('utf8'))
        #print(u.experimentName)

    return experiments[0].experimentScheme[0]["name"]
    #return "ahoj"
    
    
    
#======================================================================

@measurement.route('/measurement/planning/detail/<int:id_measurement>', methods=['GET', 'POST'])
@login_required
def planning_detail(id_measurement):
    """Show detail"""
    data = Measurement.getMeasurementById(id_measurement)
    codes = ["d7ada61597fe6e4a","d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a","d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a","d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a","d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a", "d7ada61597fe6e4a"]
    
    codesForm = CodeGenerator()
    if codesForm.data and codesForm.validate_on_submit():
        codes = []
        for i in range(codesForm.codeNumbers.data):
            codes.append(controllers.genHex())
        return redirect(url_for('measurement.planning_detail', id_measurement=id_measurement))

    return render_template("measurement_detail.html", data=data, codes=codes, form=codesForm)
    

