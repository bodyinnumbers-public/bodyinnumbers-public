from app import db
from app.auth.models import User
from app.experiments.models import Experiments
from sqlalchemy import func, and_, bindparam
from datetime import date
from flask import json
from sqlalchemy.dialects.postgresql import JSON


class Base(db.Model):
    """Base table"""
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    date_modified = db.Column(db.DateTime, default=db.func.current_timestamp(), onupdate=db.func.current_timestamp())


class Measurement(Base, db.Model):
    """Table for storing info of measurement"""
    __tablename__ = 'measurement'

    institution = db.Column(db.String(128), nullable=True)
    begin = db.Column(db.DateTime, nullable=True)
    end = db.Column(db.DateTime, nullable=True)
    town = db.Column(db.String(128), nullable=True)
    street = db.Column(db.String(128), nullable=True)
    number = db.Column(db.Integer, nullable=True)
    measured_values = db.relationship('Measured_values', backref="measurement")
    measuring_station = db.relationship('Measuring_station', backref="measurement")
    
    @property
    def json(self):
        return to_json(self, self.__class__)
    
    
    def allMeasurementToday():
        """Return all measurement for today"""
        #return db.session.query(Measurement).filter_by(func.date(begin) == date.today()).all() #only measurement begins today
        return db.session.query(Measurement).filter((func.date(Measurement.begin) <= date.today()) & (func.date(Measurement.end) >= date.today())).all() 
        
    def getMeasurementByName(name):
        """get measurement ID by name"""
        return db.session.query(Measurement).filter_by(institution = name).first()
        
    def getMeasurementById(m_id):
        """get measurement by ID"""
        return db.session.query(Measurement).filter_by(id = m_id).first()
        
    def countExperimentsOfMeasurement(measurementID):
        return db.session.query(Experiments).join(Measuring_station).filter_by(measurement_id = measurementID).count()
        
    def doneExperiments(measurementID, personID):
        return db.session.query(Measured_values).filter_by(measurement_id = measurementID).filter_by(measured_person_id = personID).order_by(Measured_values.date_created.asc())

class Measured_values(Base, db.Model):
    """Table with measured data"""
    __tablename__ = 'measured_values'

    data = db.Column(JSON)
    inserted_by = db.Column(db.Integer, db.ForeignKey('user.id'))
    data_from_experiment = db.Column(db.Integer, db.ForeignKey('experiments.id'))
    measurement_id = db.Column(db.Integer, db.ForeignKey('measurement.id'))
    measured_person_id = db.Column(db.Integer, db.ForeignKey('measured_person.id'))


    def getAllValues():
        """return values"""
        return db.session.query(Measured_values).all()
        
    def getValues(personID):
        """return values"""
        return db.session.query(Measured_values).filter_by(measured_person_id = personID)
        
    def countValues(personID):
        return db.session.query(Measured_values).filter_by(measured_person_id = personID).count()
        
    def getRegistration(personID, registrationID):
        return db.session.query(Measured_values).filter(and_(Measured_values.measured_person_id == personID, Measured_values.data_from_experiment == registrationID)).first()
        
    def getOneValue(personID):
        return db.session.query(Measured_values).filter_by(measured_person_id = personID).first()
        
    def getValuesByMeasurementExperiment(measurementID, experimentID):
        """"""
        return db.session.query(Measured_values).filter(and_(Measured_values.measurement_id == measurementID, Measured_values.data_from_experiment == experimentID)).all()
        
    def getValuesByExperiment(experimentID):
        """"""
        return db.session.query(Measured_values).filter_by(data_from_experiment = experimentID).all()
        
    def getUniqueValuesByMeasurement(measurementID):
        """"""
        return db.session.query(Measured_values).filter(measurement_id = measurementID).distinct(Measured_values.measured_person_id).all()

    def getPersons(measurementID):
        """"""
        return db.session.query(Measured_values.measured_person_id).filter_by(measurement_id = measurementID).distinct(Measured_values.measured_person_id).order_by(Measured_values.measured_person_id).all()

    def getPersonsCount(measurementID):
        """"""
        return db.session.query(Measured_values).filter_by(measurement_id = measurementID).distinct(Measured_values.measured_person_id).count()

    def getGenderCount(measurementID,gender):
        """"""
        # print(db.session.query(Measured_values.data[0]["values"][0]).filter_by(measurement_id = measurementID).filter_by(data_from_experiment = 1).filter(Measured_values.data[0]["values"][0] == 1).\
        #     distinct(Measured_values.measured_person_id))
        tuples = db.session.query(Measured_values.data[0]["values"][0]).filter_by(measurement_id = measurementID).filter_by(data_from_experiment = 1).distinct(Measured_values.measured_person_id).all()
        count = 0
        for i,tuple in enumerate(tuples):
            if tuple[0] == gender:
                count = count + 1
        return count
        
    def getValuesForHandsReactions(measurementID):
        """..."""
        experiment = db.session.query(Experiments).filter_by(experimentName = "Reakční doba horních končetin").first()
        exID = experiment.id
        return db.session.query(Measured_values).filter_by(measurement_id = measurementID).filter_by(data_from_experiment = exID).all()

    def getValuesForLegsReactions(measurementID):
        """..."""
        experiment = db.session.query(Experiments).filter_by(experimentName = "Reakční doba dolních končetin").first()
        exID = experiment.id
        return db.session.query(Measured_values).filter_by(measurement_id = measurementID).filter_by(data_from_experiment = exID).all()


class Measured_person(Base, db.Model):
    """Table with information about measured person"""
    __tablename__ = 'measured_person'

    personHash = db.Column(db.String(128))
    measured_values = db.relationship("Measured_values", backref="measured_person")
    
    def registerPerson(userID ,personID, measurementID, experimentID, formData):
        """create new record with person - save qr-code + basic data about person"""
        person = Measured_person (
            personHash = personID
        )

        values = Measured_values(
            data = json.loads(formData)
        )
        
        experiment = db.session.query(Experiments).filter_by(id = experimentID).first()
        measurement = db.session.query(Measurement).filter_by(id = measurementID).first()
        user = db.session.query(User).filter_by(id = userID).first()
        
        print("<<< measured person >>>")
        print(measurement.id)
        
        experiment.measured_values.append(values)
        measurement.measured_values.append(values)
        #values.measured_values.append(measurement)
        user.measured_values.append(values)
        person.measured_values.append(values)
        db.session.add(person)
        db.session.commit()
        
        p = db.session.query(Measured_values).first()
        print(p.measurement_id)
        
        
    def saveData(userID, personID, measurementID, experimentID, formData):
        """Save measured data from persons"""
        values = Measured_values(
            data = json.loads(formData)
        )
        
        experiment = db.session.query(Experiments).filter_by(id = experimentID).first()
        
        measurement = db.session.query(Measurement).filter_by(id = measurementID).first()
        user = db.session.query(User).filter_by(id = userID).first()
        person = db.session.query(Measured_person).filter_by(personHash = personID).first()
        experiment.measured_values.append(values)
        measurement.measured_values.append(values)
        user.measured_values.append(values)
        person.measured_values.append(values)
        db.session.commit()
        
    def getPersonByHexId(hexID):
        """return person by hex id"""
        return db.session.query(Measured_person).filter_by(personHash = hexID).first()
        
    def getPersonById(personID):
        return db.session.query(Measured_person).filter_by(id = personID).first()
    
    def getPersons(measurementID):
        """get persons by measurement ID"""
        return db.session.query(Measured_person).join(Measured_values).filter_by(measurement_id = measurementID).order_by(Measured_person.date_created.desc())
    
    def countPersons():
        return db.session.query(Measured_person).count()

class Measuring_station(Base, db.Model):
    """Table for connection between User, Measurement and Experiments"""
    __tablename__ = 'measuring_station'
    
    measurement_id = db.Column(db.Integer, db.ForeignKey('measurement.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    experiments_id = db.Column(db.Integer, db.ForeignKey('experiments.id'))

    def getExperimentsId(measureID, userID):
        return db.session.query(Measuring_station).filter((Measuring_station.measurement_id == measureID) & (Measuring_station.user_id == userID)).all()
        #return db.session.query(Measuring_station).filter(and_(Measuring_station.user_id == userID), Measuring_station.measurement_id == measureID).all()
        #return db.session.query(Measuring_station).all()



def to_json(inst, cls):
    """Jsonify the sql alchemy query result"""
    convert = dict()
    # add your coversions for things like datetime's 
    # and what-not that aren't serializable.
    d = dict()
    for c in cls.__table__.columns:
        v = getattr(inst, c.name)
        if c.type in convert.keys() and v is not None:
            try:
                d[c.name] = convert[c.type](v)
            except:
                d[c.name] = "Error:  Failed to covert using ", str(convert[c.type])
        elif v is None:
            d[c.name] = str()
        else:
            d[c.name] = v
    return json.dumps(d)
