from flask_wtf import Form
from wtforms import StringField, SubmitField, BooleanField, IntegerField  # Import Form elements such as TextField and BooleanField (optional)
from wtforms.validators import DataRequired, length  # Import Form validators
from wtforms.fields.html5 import DateTimeLocalField, DateTimeField
from datetime import datetime


class NewMeasurementForm(Form):
    """Form for new measurement"""
    institution = StringField('Name', [DataRequired(), length(max=128)])
    # begin = TextField('Begin', format="%Y-%m-%dT%H:%M:%S", default=datetime.today, validators=[validators.DataRequired()])
    begin = DateTimeLocalField('Begin', format="%Y-%m-%dT%H:%M:%S", default=datetime.today)
    end = DateTimeLocalField('End', format="%Y-%m-%dT%H:%M:%S", default=datetime.today)
    town = StringField('Town', [length(max=128)])
    street = StringField('Street', [length(max=128)])
    number = StringField('Number')
    add = SubmitField('Submit')


class EditMeasurementForm(Form):
    """Form for measurement edit"""
    measure_id = StringField('', [DataRequired(), length(max=128)])
    institution_e = StringField('Name', [DataRequired()])
    begin_e = DateTimeField('Begin', default=datetime.today)
    end_e = DateTimeField('End', default=datetime.today)
    town_e = StringField('Town', [length(max=128)])
    street_e = StringField('Street', [length(max=128)])
    number_e = StringField('Number')
    edit = SubmitField('Submit')


class NewMeasuringStation(Form):
    """Form for adding users executing experiments of a single measurement"""
    measurement_id = StringField('', [DataRequired(), length(max=128)])
    experimentName = StringField('Experiment Name', [DataRequired()])
    #experiment_id = StringField('', [DataRequired()])
    experimentExecutor = StringField('Experimentator', [DataRequired()])
    #executor_id = StringField('', [DataRequired()])
    #add = SubmitField('Vytvořit měření')

class MeasuredData(Form):
    """Form for data from javascript"""
    dataField = StringField('data')


class CodeGenerator(Form):
    """Form for generating codes for persons"""

    codeNumbers = IntegerField('Number of codes')
    generate = SubmitField('Generate »')
