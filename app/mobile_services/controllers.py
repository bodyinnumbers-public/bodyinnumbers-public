from flask import Flask, Blueprint, request, jsonify, json, render_template, redirect, jsonify
from flask_restful import reqparse, abort, Api, Resource, url_for
from datetime import datetime, date, timedelta
from sqlalchemy import func
import hmac
from hashlib import sha256
from base64 import b64encode
from app.auth.models import User, Role
from app.experiments.models import Experiments
from app.measurement.models import Measurement, Measuring_station, Measured_person, Measured_values
from flask_bcrypt import Bcrypt, check_password_hash, generate_password_hash
from app import app, bcrypt, db
from app.fitness.food.models import FoodTracking
from os import urandom, path, remove
import binascii
import requests 

mobile_services = Blueprint('mobile_services', __name__, template_folder='templates', url_prefix='/app')


def genHex():
    """generator of 64 characters hex strings"""
    number = binascii.hexlify(urandom(32)).decode("utf-8")
    code = str(number)
    #print(len(code))
    return code


def computeToken(secret):
    """function for computing token"""
    salt = 'čE*loX$%m' + date.today().strftime('%m/%d/%Y') + 'r@Al#GÝ8.O'
    hashed = hmac.new(bytearray(secret, "UTF8"), bytearray(salt, "UTF8"), sha256)
    token = hashed.hexdigest()
    return token


def generateRandomString():
    """function for generating 10 char strings"""
    randomBytes = urandom(7)
    randomStr =  b64encode(randomBytes).decode('utf-8')
    randomStr = randomStr[:10]
    return str(randomStr)

def authorize(username, token):
    """authorize client"""
    user = User.getUser(username)
    if user and token == user.token and datetime.now() < user.tokenExpiration:
        return user.id
    return None
    

api = Api(mobile_services)



class LoginMobileClient(Resource):
    """Login"""
    parser = reqparse.RequestParser()
    parser.add_argument('client_username', type=str, required=True)
    parser.add_argument('client_passwd', type=str, required=True)
    
    def post(self):
        print("<<< prihlaseni >>>")
        data = self.parser.parse_args()
        client_username = data.get('client_username', '')
        client_passwd = data.get('client_passwd', '')
        user = User.getUser(client_username)
        if user:
            if bcrypt.check_password_hash(user.password, client_passwd):
                
                token = genHex()
                expiration = datetime.now() + timedelta(minutes=60*50) #50 hodin
                User.saveToken(user.id, token, expiration)
                data = {}
                data["token"] = token
                return data
        return "authentication failed"


class SendMeasurementList(Resource):
    """List of all measurements"""
    parser = reqparse.RequestParser()
    parser.add_argument('client_username', type=str, required=True)
    parser.add_argument('token', type=str, required=True)
    
    def post(self):
        data = self.parser.parse_args()
        client_username = data.get('client_username', '')
        token = data.get('token', '')
        userID = authorize(client_username, token)
        
        if userID:
            measureToday = Measurement.allMeasurementToday()
            data = []
            send = {}
            for m in measureToday:
                oneMeasurement = {}
                oneMeasurement["id"] = m.id
                oneMeasurement["institution"] = m.institution
                oneMeasurement["town"] = m.town
                oneMeasurement["street"] = m.street
                oneMeasurement["number"] = m.number
                data.append(oneMeasurement)
            send["list"] = data
            jsondata = json.dumps(send, ensure_ascii=False)
            return send
        else:
            return "authorization failed"

class SendFormScheme(Resource):
    """Send form scheme to mobile clinet"""
    parser = reqparse.RequestParser()
    parser.add_argument('client_username', type=str, required=True)
    parser.add_argument('token', type=str, required=True)
    parser.add_argument('measurementID', type=int, required=True)
    
    def post(self):
        data = self.parser.parse_args()
        client_username = data.get('client_username', '')
        token = data.get('token', '')
        measurementID = data.get('measurementID', '')
        userID = authorize(client_username, token)
        
        if userID:
            experimentIDs = Measuring_station.getExperimentsId(int(measurementID), int(userID))
            data = []
            for ex in experimentIDs:
                exp = Experiments.getExperiment(ex.experiments_id)
                scheme = json.dumps(exp.experimentScheme, ensure_ascii=False)
                d = {}
                d["username"] = client_username
                d["experimentID"] = exp.id
                d["experimentName"] = exp.experimentName
                d["scheme"] = scheme# exp.experimentScheme
                #jsondata = json.dumps(d, ensure_ascii=False)
                data.append(d)
            #jsondata = json.dumps(data, ensure_ascii=False)
            jsondata = data
            return jsondata
        else:
            return "Authorization failed"

class RecieveExperimentData(Resource):
    """Recieve forms with data from mobile client"""
    parser = reqparse.RequestParser()
    parser.add_argument('client_username', type=str, required=True)
    parser.add_argument('token', type=str, required=True)
    
    parser.add_argument('personID', type=str, required=True)
    parser.add_argument('measurementID', type=int, required=True)
    parser.add_argument('experimentID', type=int, required=True)
    #parser.add_argument('data', type=list, location='json', required=False) #fungovalo driv??
    parser.add_argument('data', type=str, required=True)
    
    def post(self):
        data = self.parser.parse_args()
        client_username = data.get('client_username', '')
        token = data.get('token', '')
        personID = data.get('personID', '')
        measurementID = data.get('measurementID', '')
        experimentID = data.get('experimentID', '')
        recievedData = data.get('data', '')
        
        userID = authorize(client_username, token)
        recievedData = json.loads(recievedData)
        if userID:
            exp = Experiments.getExperiment(experimentID)
            data = json.dumps(recievedData, ensure_ascii=False)
            if exp.experimentName == 'Registrace' or exp.experimentName == 'registrace' or exp.experimentName == 'Registration' or exp.experimentName == 'registrace':
                if Measured_person.getPersonByHexId(personID) is None:
                    Measured_person.registerPerson(userID, personID, measurementID, experimentID, data)
                else:
                    return "Registration failed. Use another ID!"
            else:
                if not (Measured_person.getPersonByHexId(personID) is None):
                    Measured_person.saveData(userID, personID, measurementID, experimentID, data)
                else:
                    return "ID not found"
            
            return "Save success"
        return "Authorization failed"
        

class SendMeasuredData(Resource):
    """send data about person"""
    parser = reqparse.RequestParser()
    parser.add_argument('client_username', type=str, required=True)
    parser.add_argument('token', type=str, required=True)
    parser.add_argument('personID', type=str, required=True)
    
    def post(self):
        data = self.parser.parse_args()
        client_username = data.get('client_username', '')
        token = data.get('token', '')
        personID = data.get('personID', '')
        
        userID = authorize(client_username, token)
        if userID:
            
            person = Measured_person.getPersonByHexId(personID)
            if not (person is None):
                data = Measured_values.getValues(person.id)
                experimentNames = []
                experimentsData = []
                dataOut = []
                output = {}
                allNames = []
                allUnits = []
                allValues = []
                for i, d in enumerate(data):
                    names = []
                    values = []
                    units = []
                    ex = Experiments.getExperiment(d.data_from_experiment)
                    #print(json.dumps(ex.experimentScheme))
                    scheme = ex.experimentScheme
                    jsondata = d.data
                    #print(ex.experimentName)
                    f = json.dumps(d.data)
                    experimentNames.append(ex.experimentName)
                    for index, v in enumerate(jsondata):
                        if scheme[int(v["id"])]["formType"] == "radio" and v["values"][0]:
                            values.append(scheme[int(v["id"])]["formLabels"][int(v["values"][0])])
                        else:
                            values.append(v["values"][0])
                        units.append(scheme[int(v["id"])]["units"])
                        names.append(scheme[int(v["id"])]["name"])
                    allValues.append(values)
                    allNames.append(names)
                    allUnits.append(units)
                    
                output["experiments"] = experimentNames
                output["names"] = allNames
                output["units"] = allUnits
                output["values"] = allValues
            
                return output
            else:
                return "ID not found"
            
        return "Authorization failed"
        
class LogoutMobileClient(Resource):
    """Logout client"""
    parser = reqparse.RequestParser()
    parser.add_argument('client_username', type=str, required=True)
    parser.add_argument('token', type=str, required=True)
    
    def post(self):
        data = self.parser.parse_args()
        client_username = data.get('client_username', '')
        token = data.get('token', '')
    
        userID = authorize(client_username, token)
        if userID:
            user = User.getUser(client_username)
            token = genHex()
            expiration = datetime.now()
            User.saveToken(user.id, token, expiration)
            return "ok"
        else:
            return "Logout failed"
        
api.add_resource(LoginMobileClient, '/mobile-services/login')
api.add_resource(LogoutMobileClient, '/mobile-services/logout')
api.add_resource(SendMeasurementList, '/mobile-services/measurement-list')
api.add_resource(SendFormScheme, '/mobile-services/scheme')
api.add_resource(RecieveExperimentData, '/mobile-services/receive-data')
api.add_resource(SendMeasuredData, '/mobile-services/measured-data')
