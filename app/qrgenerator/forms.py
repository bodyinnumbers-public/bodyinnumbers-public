from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, SelectField, TextAreaField, HiddenField, IntegerField, BooleanField, FileField  # Import Form elements such as StringField (optional)
from wtforms.validators import DataRequired, length  # Import Form validators


class PersonQRForm(FlaskForm):
    """Form for generating person QR codes"""
    countQR = IntegerField('Number of unique codes:')
    initialID = IntegerField('Initial ID number:', default=1)
    eventTag = StringField('Event tag:')
    date = StringField('Date of the event:')
    #templateCheckbox = BooleanField('Checkbox jestli vygenerovat šablonu')
    #template = FileField('Template file') #HiddenField('Scheme')
    generate = SubmitField('Generate »')

class StationQRForm(FlaskForm):
    """Form for generating station QR codes"""
    countQR = IntegerField('celkový počet qr kódů')
    pageCountQR = IntegerField('počet qr kódů na stránku')
    generate = SubmitField('Generate')
