from flask import Blueprint, request, render_template, redirect, jsonify, url_for, make_response
from app import app, db
from app.measurement.models import Measurement, Measured_person, Measuring_station
from app.auth.models import User
from app.experiments.models import Experiments
from app.measurement.forms import NewMeasurementForm, EditMeasurementForm, NewMeasuringStation
from flask_login import login_required, current_user

from os import urandom, path, remove
import binascii
from flask_weasyprint import HTML, render_pdf
import pyqrcode
import json
import pdfkit
import base64
import datetime

from app.qrgenerator.forms import PersonQRForm, StationQRForm

qrgenerator = Blueprint('qrgenerator', __name__, template_folder='templates', url_prefix='/app')


def genHex():
    """generator of 16 characters hex strings"""
    number = binascii.hexlify(urandom(8)).decode("utf-8")
    code = str(number)
    #print(len(code))
    return code
    


def qrcode(code, number):
    """QR code generator"""
    path = app.config['TMP_FOLDER'] + number + ".png"#path = app.config['TMP_FOLDER'] + code + ".png"
    qr = pyqrcode.create("http://bodyinnumbers.kiv.zcu.cz:8080/"+code)
    qr.png(path, scale=5)


@qrgenerator.route('/qr-codes.pdf')
@login_required
def generatePDF():
    """PDF generator"""
    count = 24
    columns = 3
    codes = []
    for i in range(0, count):
        c = genHex()
        codes.append(c)
        qrcode(c, str(i+1))
        
    
    html = render_template("qr_codes.html", columns=columns, codes=codes)
    pdf = render_pdf(HTML(string=html))
    
    
    for i, c in enumerate(codes):
        print("_")
        #remove(app.config['TMP_FOLDER'] + c + ".png")
        remove(app.config['TMP_FOLDER'] + str(i+1) + ".png")
        
    #return html
    return pdf

@qrgenerator.route('/qr-person', methods=['GET', 'POST'])
@login_required
def person_setup_QR():
    """Setup for generating person QR codes"""

    personForm = PersonQRForm()
    if personForm.data and personForm.validate_on_submit():
        #ještě udělat složky, aby mohlo stejnou činnost dělat víc lidí najednou,jinak, to bude blbnout :D
        return redirect(url_for('qrgenerator.generatePagePDF', countQR=personForm.countQR.data, initialID=personForm.initialID.data, eventTag=personForm.eventTag.data, date=personForm.date.data))
        #return redirect('app/qr-page')
    return render_template("qrperson.html", form=personForm)


@qrgenerator.route('/qr-station', methods=['GET', 'POST'])
@login_required
def station_setup_QR():
    """Setup for generating station QR codes"""
    
    stationForm = StationQRForm(request.form)
    if stationForm.data and stationForm.validate_on_submit():
        redirect('app/qr-codes.pdf')
    return render_template("qrstation.html", form=stationForm)




@qrgenerator.route('/qr-page/<int:countQR>/<int:initialID>/<string:eventTag>/<string:date>', methods=['GET', 'POST'])
@login_required
def generatePagePDF(countQR, initialID, eventTag, date):
    """PDF generator"""
    
    count = countQR
    codes = []
    img_qrs = []
    for i in range(0, count):
        c = genHex()
        codes.append(c)
        qrcode(c, str(i+1))
        with open(app.config['TMP_FOLDER'] + str(i+1) + ".png", "rb") as image_file:
            encoded = base64.b64encode(image_file.read())
            img_qrs.append(str(encoded, "utf-8"))
        
        
    
#    with open("app/static/img/body/1.png", "rb") as image_file:
#        encoded = base64.b64encode(image_file.read())
#        img_qr = str(encoded, "utf-8")
    
    with open("app/static/img/body/mozek-low.png", "rb") as image_file:
        encoded = base64.b64encode(image_file.read())
        img_brain = str(encoded, "utf-8")
        
    with open("app/static/img/body/srdce.png", "rb") as image_file:
        encoded = base64.b64encode(image_file.read())
        img_heart = str(encoded, "utf-8")
        
    with open("app/static/img/body/plice.png", "rb") as image_file:
        encoded = base64.b64encode(image_file.read())
        img_lungs = str(encoded, "utf-8")
        
    with open("app/static/img/body/people.png", "rb") as image_file:
        encoded = base64.b64encode(image_file.read())
        img_people = str(encoded, "utf-8")
    
    with open("app/static/img/body/body-low.png", "rb") as image_file:
        encoded = base64.b64encode(image_file.read())
        img_body = str(encoded, "utf-8")
        
    with open("app/static/img/body/kiv.png", "rb") as image_file:
        encoded = base64.b64encode(image_file.read())
        img_kiv = str(encoded, "utf-8")
    
    with open("app/static/img/body/mi.png", "rb") as image_file:
        encoded = base64.b64encode(image_file.read())
        img_mi = str(encoded, "utf-8")
        
    with open("app/static/img/body/ntis.gif", "rb") as image_file:
        encoded = base64.b64encode(image_file.read())
        img_ntis = str(encoded, "utf-8")
        
    with open("app/static/img/body/fav.jpg", "rb") as image_file:
        encoded = base64.b64encode(image_file.read())
        img_fav = str(encoded, "utf-8")
        
    with open("app/static/img/body/eu.jpg", "rb") as image_file:
        encoded = base64.b64encode(image_file.read())
        img_eu = str(encoded, "utf-8")
    
    html = render_template("body_cz.html", img_body=img_body, img_kiv=img_kiv, img_mi=img_mi, img_ntis=img_ntis, img_fav=img_fav, img_eu=img_eu,img_brain=img_brain, img_heart=img_heart, img_lungs=img_lungs, img_people=img_people, eventTag=eventTag, date=date, initialID=initialID, codes= zip(codes, img_qrs))
    
    #return html
    
    options = {
    'page-size': 'A4',
    'dpi':600,
    'margin-top': '0in',
    'margin-right': '0in',
    'margin-bottom': '0in',
    'margin-left': '0in',
    'encoding': "UTF-8"
}
    pdf = pdfkit.from_string(html, False, options=options)

    
    for i, c in enumerate(codes):
        print("_")
        remove(app.config['TMP_FOLDER'] + str(i+1) + ".png")
    
    response = make_response(pdf)
    response.headers['Content-Type'] = 'application/pdf'
    response.headers['Content-Disposition'] = 'attachment; filename=qr_pages.pdf'
    return response

@qrgenerator.route('/conditions', methods=['GET', 'POST'])
@login_required
def generateConditionPDF():
    """PDF generator"""
    
    
    count = 50
    codes = []
    img_qrs = []
    for i in range(0, count):
        c = genHex()
        codes.append(c)
        qrcode(c, str(i+1))
        with open(app.config['TMP_FOLDER'] + str(i+1) + ".png", "rb") as image_file:
            encoded = base64.b64encode(image_file.read())
            img_qrs.append(str(encoded, "utf-8"))
    
    date = datetime.datetime.now()
    eventTag = "zkusebni"


    with open("app/static/img/body/kiv.png", "rb") as image_file:
        encoded = base64.b64encode(image_file.read())
        img_kiv = str(encoded, "utf-8")
    
    with open("app/static/img/body/mi.png", "rb") as image_file:
        encoded = base64.b64encode(image_file.read())
        img_mi = str(encoded, "utf-8")
        
    with open("app/static/img/body/ntis.gif", "rb") as image_file:
        encoded = base64.b64encode(image_file.read())
        img_ntis = str(encoded, "utf-8")
        
    with open("app/static/img/body/fav.jpg", "rb") as image_file:
        encoded = base64.b64encode(image_file.read())
        img_fav = str(encoded, "utf-8")
        
    with open("app/static/img/body/eu.jpg", "rb") as image_file:
        encoded = base64.b64encode(image_file.read())
        img_eu = str(encoded, "utf-8")
    
    html = render_template("conditions.html", img_kiv=img_kiv, img_mi=img_mi, img_ntis=img_ntis, img_fav=img_fav, img_eu=img_eu, eventTag=eventTag, date=date, codes= zip(codes, img_qrs))
    
    options = {
    'page-size': 'A4',
    'dpi':600,
    'margin-top': '0in',
    'margin-right': '0in',
    'margin-bottom': '0in',
    'margin-left': '0in',
    'encoding': "UTF-8"
}
    pdf = pdfkit.from_string(html, False, options=options)

    
    for i, c in enumerate(codes):
        print("_")
        remove(app.config['TMP_FOLDER'] + str(i+1) + ".png")
    
    response = make_response(pdf)
    response.headers['Content-Type'] = 'application/pdf'
    response.headers['Content-Disposition'] = 'attachment; filename=qr_pages.pdf'
    return response
