import os
# Statement for enabling the development environment
DEBUG = True

# Define the application directory
BASE_DIR = os.path.abspath(os.path.dirname(__file__))  

# Define folder for temporary data like qr codes
TMP_FOLDER = 'app/tmp/'

# Define the database
SQLALCHEMY_DATABASE_URI = 'database_uri'
SQLALCHEMY_TRACK_MODIFICATIONS = False
DATABASE_CONNECT_OPTIONS = {}

# Application threads. A common general assumption is
# using 2 per available processor cores - to handle
# incoming requests using one and performing background
# operations using the other.
THREADS_PER_PAGE = 2

# Enable protection agains *Cross-site Request Forgery (CSRF)*
CSRF_ENABLED = True

# Use a secure, unique and absolutely secret key for
# signing the data. 
CSRF_SESSION_KEY = "secret_key"

# Secret key for signing cookies
SECRET_KEY = "secret_key"

JSON_AS_ASCII = False

# Session (vypršení) in minutes
RESTAPI_ = 2*60 # 2 hours

#Storage - folder for all uploads
STORAGE_FOLDER = "uploads"
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'csv', 'xls'])
